

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class PacketReader {

	private DataInputStream in;
	private Client client;

	public PacketReader(Client client) {
		this.client = client;
		this.in = this.client.in;
	}
	
	public void readPacket(int packetId) throws IOException{
		if(packetId == 0){
			String teamName = in.readUTF();
			client.teamName = teamName;
		}
		else if(packetId == 1){
			boolean automaticStartTeams = in.readBoolean();
			ArrayList<String> playerTeams = new ArrayList<String>();
			for(Player player : Player.getPlayers()){
				playerTeams.add(player.teamName);
			}
			Game.startNewGame(playerTeams, automaticStartTeams);
			Game.startGame();
		}
		else if(packetId == 2){
			int idx = in.readByte();
			int amount = in.readInt();
			if(idx >= 0){
				Gladiator gladiator = Game.recruitableGladiators.get(idx);
				Game.putRecruitOffer(gladiator, amount);
			} else {
				Game.skipRecruitOffer();
			}
		}
		else if(packetId == 3){
			int idx = in.readByte();
			String itemName = in.readUTF();
			Item item = Item.findItem(itemName);
			if(idx >= 0){
				Gladiator gladiator = client.team.gladiators.get(idx);
				Game.buyItem(gladiator, item);
			}
		}
		else if(packetId == 4){
			int idx = in.readByte();
			String spellName = in.readUTF();
			Spell spell = Spell.findSpell(spellName);
			if(idx >= 0){
				Gladiator gladiator = client.team.gladiators.get(idx);
				Game.buySpell(gladiator, spell);
			}
		}
		else if(packetId == 5){
			int idx = in.readByte();
			int slot = in.readByte();
			if(idx >= 0){
				Gladiator gladiator = client.team.gladiators.get(idx);
				Game.sellItem(gladiator, slot);
			}
		}
		else if(packetId == 6){
			int idx = in.readByte();
			String s = in.readUTF();
			if(idx >= 0){
				Gladiator gladiator = client.team.gladiators.get(idx);
				Game.trainSkill(gladiator, s);
			}
		}
		else if(packetId == 7){
			client.team.skipRecruit = true;
			Team team = Game.teamRecruitTurnOrder.get(Game.currentRecruitTeamIdx-1);
			if(client.team == team){
				Game.skipRecruitOffer();
			}
		}
		else if(packetId == 8){
			client.team.readyToBattle = true;
			for(int i = 0; i < client.team.battleGladiators.length; i++){
				int idx = in.readByte();
				if(idx >= 0){
					client.team.battleGladiators[i] = client.team.gladiators.get(idx);
				} else {
					client.team.battleGladiators[i] = null;
				}
			}
			if(!Game.teamsReadyToBattle()){
				client.packetSender.sendShowWaitDialogPacket();
			} else {
				Game.createBattleGroups();
				Game.currentBattleIdx = 0;
				Game.setupMaps();
			}
		}
		else if(packetId == 9){
			client.team.currentBattle.startNewBattleTurn();
		}
		else if(packetId == 10){
			int movementType = in.readByte();
			if(client.team == client.team.currentBattle.getCurrentTeamTurn()){
				Battle.playBattleTurn(client.team.currentBattle, movementType);
			}
		}
		else if(packetId == 11){
			Game.sendStartOfNewMonthData(client);
		}
	}
	
}
