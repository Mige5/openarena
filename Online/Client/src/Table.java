import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Table {

	public JTable jTable;
	public JScrollPane jScrollpane;
	public DefaultTableModel tableModel;
	public String table_name;
	public int[] window_name;
	public int[] sub_window_name;
	
	public Table(int[] window_name, int[] sub_window_name, String table_name, JTable jTable, JScrollPane jScrollPane, DefaultTableModel tableModel){
		this.window_name = window_name;
		this.sub_window_name = sub_window_name;
		this.table_name = table_name;
		this.jTable = jTable;
		this.jScrollpane = jScrollPane;
		this.tableModel = tableModel;
	}
	
}
