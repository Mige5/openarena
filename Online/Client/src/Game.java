import java.util.ArrayList;

public class Game {

	public static Player ownPlayer = null;
	public static Team ownTeam = null;
	
	public static boolean isOnlineGame = true;
	
	public static int MONTH = 0;
	public static int YEAR = 900;
	
	public static GamePanel gamePanel;
	
	public static String[] mods = new String[]{"OpenArena", "Areena 5"};
	
	public static int MOD_OPEN_ARENA = 0;
	public static int MOD_AREENA_5 = 1;
	
	public static int currentMod = MOD_OPEN_ARENA;
	
	public static int MONTH_TYPE_DIVISION_BATTLE = 0;
	public static int MONTH_TYPE_CUP_BATTLE = 1;
	public static int MONTH_TYPE_WINTER_BREAK = 2;
	
	public static int[] monthType = new int[]{
			MONTH_TYPE_DIVISION_BATTLE, //Tammi
			MONTH_TYPE_DIVISION_BATTLE, //Helmi
			MONTH_TYPE_CUP_BATTLE, //Maalis
			MONTH_TYPE_DIVISION_BATTLE, //Huhti
			MONTH_TYPE_CUP_BATTLE, //Touko
			MONTH_TYPE_DIVISION_BATTLE, //Kes�
			MONTH_TYPE_CUP_BATTLE, //Hein�
			MONTH_TYPE_DIVISION_BATTLE, //Elo
			MONTH_TYPE_CUP_BATTLE, //Syys
			MONTH_TYPE_DIVISION_BATTLE, //Loka
			MONTH_TYPE_WINTER_BREAK, //Marras
			MONTH_TYPE_WINTER_BREAK, //Joulu
	};
	
	public static Battle currentBattle = null;
	public static Spell currentShootSpell = null;
	public static int currentBattleIdx = -1;
	public static int BATTLE_MAP = 0;
	public static Race RACE_OF_MONTH;
	
	public static Gladiator currentGladiator;
	public static Team currentTeam;
	public static Team showGladiatorsForTeam;
	
	public static ArrayList<Gladiator> recruitableGladiators = new ArrayList<Gladiator>();
	
	public static ArrayList<Team> teams = new ArrayList<Team>();
	public static ArrayList<Team> teamRecruitTurnOrder = new ArrayList<Team>();
	public static int currentRecruitRound = 1;
	public static int currentRecruitTeamIdx = 0;
	
	public static ArrayList<Battle> battleList = new ArrayList<Battle>();
	public static ArrayList<Battle> cupBattles_round1 = new ArrayList<Battle>();
	public static ArrayList<Battle> cupBattles_round2 = new ArrayList<Battle>();
	public static ArrayList<Battle> cupBattles_round3 = new ArrayList<Battle>();
	public static ArrayList<Battle> cupBattles_round4 = new ArrayList<Battle>();
	
	public static boolean autoRound = false;
	
	public static void createOwnPlayer(){
		Team team = new Team(ownPlayer.teamName);
		Player player = new Player(team);
		team.controllingPlayer = player;
		player.controlledTeam = team;
		Game.ownTeam = team;
		Game.currentTeam = team;
		startGame();
	}
	
	public static void meleeAtk(Gladiator attacker, Gladiator defender){
		
	}
	
	public static void rangedAtk(Gladiator attacker, Gladiator defender){

	}
	
	public static void spellCast(Gladiator caster, Gladiator target, Spell spell){

	}
	
	public static boolean battleTeamHasGladiator(Gladiator gladiator){
		for(int i = 0; i < Game.currentTeam.battleGladiators.length; i++){
			Gladiator gladiator_ = Game.currentTeam.battleGladiators[i];
			if(gladiator_ == gladiator)
				return true;
		}
		return false;
	}
	
	public static Team getNextDivisionBattleOpponentForTeam(Team team){
		return getDivisionBattleOpponentForTeam(team, MONTH);
	}
	
	public static void putGladiatorToBattleSlot(Gladiator gladiator, int slot){
		Game.currentTeam.battleGladiators[slot] = gladiator;
	}
	
	public static int battleTeamGetGladiatorSlot(Gladiator gladiator){
		for(int i = 0; i < Game.currentTeam.battleGladiators.length; i++){
			Gladiator gladiator_ = Game.currentTeam.battleGladiators[i];
			if(gladiator_ == gladiator)
				return i;
		}
		return -1;
	}
	
	public static Team getDivisionBattleOpponentForTeam(Team team, int month){
		Division division = findTeamsDivision(team);
	    int teamIndex = Util.getTeamIndex(division, team);
	    Team opponent = null;
		if(month == 0 || month == 5){
    		if(teamIndex == 0){
    			opponent = division.teams.get(3);
    		} else if(teamIndex == 1){
    			opponent = division.teams.get(2);
    		} else if(teamIndex == 2){
    			opponent = division.teams.get(1);
    		} else if(teamIndex == 3){
    			opponent = division.teams.get(0);
    		}
    	} else if(month == 1 || month == 7){
    		if(teamIndex == 0){
    			opponent = division.teams.get(2);
    		} else if(teamIndex == 1){
    			opponent = division.teams.get(3);
    		} else if(teamIndex == 2){
    			opponent = division.teams.get(0);
    		} else if(teamIndex == 3){
    			opponent = division.teams.get(1);
    		}
    	} else if(month == 3 || month == 9){
    		if(teamIndex == 0){
    			opponent = division.teams.get(1);
    		} else if(teamIndex == 1){
    			opponent = division.teams.get(0);
    		} else if(teamIndex == 2){
    			opponent = division.teams.get(3);
    		} else if(teamIndex == 3){
    			opponent = division.teams.get(2);
    		}
    	}
		return opponent;
	}
	
	public static Team findTeamForName(String name){
		for(Division division : Division.divisions){
			for(Team team_ : division.teams){
				if(name.equals(team_.name)){
					return team_;
				}
			}
		}
		return null;
	}
	
	public static Division findTeamsDivision(Team team){
		for(Division division : Division.divisions){
			for(Team team_ : division.teams){
				if(team == team_){
					return division;
				}
			}
		}
		return null;
	}
	
	public static Team getOpponentForCUPBattleForTeam(Team team){
		return getOpponentForCUPBattleForTeam(team, MONTH);
	}
	
	public static Team getOpponentForCUPBattleForTeam(Team team, int month){
		ArrayList<Battle> cupBattles = new ArrayList<Battle>();
		if(month == 8){
			cupBattles = cupBattles_round4;
		}
		else if(month == 6){
			cupBattles = cupBattles_round3;
		}
		else if(month == 4){
			cupBattles = cupBattles_round2;
		}
		else if(month == 2){
			cupBattles = cupBattles_round1;
		}
		for(Battle battle : cupBattles){
			if(battle.topTeam == team){
				return battle.bottomTeam;
			}
			else if(battle.bottomTeam == team){
				return battle.topTeam;
			}
		}
		return null;
	}
	
	public static boolean successfullFriendlySpellCast(Gladiator caster, Spell spell){
		/*
		int atkVal = caster.getAtkSkillValueForSpell(spell);
		int blockVal = 0;
		int hitChance = Util.hitChance(atkVal, blockVal);
		boolean hitSuccess = Util.successfulHit(hitChance);
		return hitSuccess;
		*/
		return false;
	}
	
	public static Team getNextOpponentForTeam(Team team){
		if(Game.monthType[Game.MONTH] == Game.MONTH_TYPE_DIVISION_BATTLE){
			return getNextDivisionBattleOpponentForTeam(team);
		}
		else if(Game.monthType[Game.MONTH] == Game.MONTH_TYPE_CUP_BATTLE){
			return getOpponentForCUPBattleForTeam(team);
		}
		return null;
	}
	
	public static int getSalaryForGladiators(Team team){
		int totalSalary = 0;
		for(Gladiator gladiator : team.gladiators){
			if(team.battleTeamContainsGladiator(gladiator)){
				totalSalary += gladiator.salary;
			} else {
				totalSalary += gladiator.salary/2;
			}
		}
		return totalSalary;
	}
	
	public static void putRecruitOffer(Gladiator gladiator, int amount){
		int idx = Game.recruitableGladiators.indexOf(gladiator);
		gamePanel.mainFrame.client.packetSender.sendOfferRecruitPacket(idx, amount);
		/*
		if(Game.currentTeam.getGladiatorRaces().contains(gladiator.race))
			return;
		if(gladiator.offerTeam != null){
			//gladiator.offerTeam.money += gladiator.offerPrice;
			gladiator.offerTeam.modifyMoney(gladiator.offerPrice);
		}
		gladiator.offerPrice = amount;
		gladiator.offerTeam = Game.currentTeam;
		//Game.currentTeam.money -= amount;
		Game.currentTeam.modifyMoney(-amount);
		Game.nextRecruitTurn();
		*/
	}
	
	public static void trainSkill(String s){
		gamePanel.mainFrame.client.packetSender.sendTrainPacket(s);
		/*
		int cost = getTrainCost(s);
		if(s.equals(TRAIN_STR_TXT)){
			Game.currentGladiator.str += 1;
		}
		else if(s.equals(TRAIN_SPEED_TXT)){
			Game.currentGladiator.speed += 1;
		}
		else if(s.equals(TRAIN_HP_TXT)){
			Game.currentGladiator.hp += 1;
			Game.currentGladiator.maxHp += 1;
		}
		else if(s.equals(TRAIN_MANA_TXT)){
			Game.currentGladiator.mana += 1;
			Game.currentGladiator.maxMana += 1;
		}
		if(Game.currentTeam.money < cost){
			return;
		}
		//Game.currentTeam.money -= cost;
		Game.currentTeam.modifyMoney(-cost);
		*/
	}
	
	public static void startGame(){
		Spell.initSpells();
		Item.initItems();
		gamePanel.addSpellsToTable();
		/*
		Spell.initSpells();
		Item.initItems();
		Game.setupRecruitOrder();
		Game.nextRecruitTurn();
		createRecruitableGladiators();
		//gamePanel.addSpellsToTable();
		Item.setNewDiscountedItems();
		RACE_OF_MONTH = Race.races.get(Util.random_(Race.races.size()));
		*/
	}
	
	public static void setupRecruitOrder(){//TODO: confirm this is how it should work
		for(int i = 0; i < Division.divisions.size(); i++){
			Division division = Division.divisions.get(i);
			for(Team team : division.teams){
				Game.teamRecruitTurnOrder.add(team);
			}
		}
	}
	
	public static boolean progressToNextMonth = false;
	
	public static void progressToNextMonth(){
		/*
		progressToNextMonth = false;
		Game.currentBattle = null;
		boolean progressYear = false;
		if(Game.MONTH < Constants.MONTHS.length-1){
			Game.MONTH++;
		}else{
			Game.MONTH = 0;
			Game.YEAR++;
			progressYear = true;
		}
		Game.currentRecruitTeamIdx = 0;
		Game.currentRecruitRound = 1;
		if(progressYear){
			cupBattles_round1.clear();
			cupBattles_round2.clear();
			cupBattles_round3.clear();
			cupBattles_round4.clear();
			moveTeamsToNewDivisions();
			createCupBattleGroups(1);
			Game.teamRecruitTurnOrder.clear();
			Game.setupRecruitOrder();
			Game.nextRecruitTurn();
		} else {
			Game.nextRecruitTurn();
		}
		RACE_OF_MONTH = Race.races.get(Util.random_(Race.races.size()));
		Item.setNewDiscountedItems();
		Game.recruitableGladiators.clear();
		createRecruitableGladiators();
		for(Division division : Division.divisions){
			for(Team team : division.teams){
				team.addedToBattle = false;
				//team.money -= getSalaryForGladiators(team);
				team.modifyMoney(-getSalaryForGladiators(team));
				team.currentMonthMoneyBalance = 0;
				team.currentMonthIncome = 0;
				if(progressYear){
					team.wins = 0;
					team.battleResult = new int[12];
				}
				ArrayList<Gladiator> gladiatorsToRetire = new ArrayList<Gladiator>();
				for(Gladiator gladiator : team.gladiators){
					gladiator.hp = gladiator.maxHp;
					gladiator.mana = gladiator.maxMana;
					gladiator.needsReload = false;
					gladiator.ammo = Constants.AMMO_COUNT;
					gladiator.receivedDmg = -1;
					if(gladiator.hurt > 0){
						gladiator.hurt -= 1;
					}
					if(progressYear){
						gladiator.takeOuts_1 = 0;
						gladiator.age += 1;
						if(gladiator.age > Constants.GLADIATOR_MAX_AGE){
							gladiatorsToRetire.add(gladiator);
						}
					}
				}
				for(Gladiator gladiator : gladiatorsToRetire){//TODO: popup
					if(gladiator.items[Constants.MELEE_WEAPON_SLOT] != null){
						sellItem(gladiator, Constants.MELEE_WEAPON_SLOT);
					}
					if(gladiator.items[Constants.RANGED_WEAPON_SLOT] != null){
						sellItem(gladiator, Constants.RANGED_WEAPON_SLOT);
					}
					if(gladiator.items[Constants.ARMOUR_SLOT] != null){
						sellItem(gladiator, Constants.ARMOUR_SLOT);
					}
					team.gladiators.remove(gladiator);
				}
			}
		}
		*/
	}
	
	public static void createBattleGroups(){
		/*
		battleList.clear();
		if(monthType[MONTH] != MONTH_TYPE_CUP_BATTLE){
			for(Team team : Game.teams){
				if(team.addedToBattle)
					continue;
				createBattleGroup(team, Game.getNextOpponentForTeam(team));
			}
		} else {
			if(cupBattles_round4.size() > 0){
				battleList.addAll(cupBattles_round4);
			}
			else if(cupBattles_round3.size() > 0){
				battleList.addAll(cupBattles_round3);
			}
			else if(cupBattles_round2.size() > 0){
				battleList.addAll(cupBattles_round2);
			}
			else if(cupBattles_round1.size() > 0){
				battleList.addAll(cupBattles_round1);
			}
		}
		*/
	}
	
	public static void startNewGame(){
		Game.createDivisions();
	}
	
	public static void createDivisions(){
		new Division(Constants.START_MONEY_DIV_1);
		new Division(Constants.START_MONEY_DIV_2);
		new Division(Constants.START_MONEY_DIV_3);
		new Division(Constants.START_MONEY_DIV_4);
	}
	
	public static void setupMap(Battle battle){
		/*
		int bottom_offX = 2;
		int bottom_offY = 8;
		int top_offX = 2;
		int top_offY = 1;
		Team bottomTeam = battle.bottomTeam;
		Team topTeam = battle.topTeam;
		if(bottomTeam.controllingPlayer == null){
			Game.createBattleTeamForAI(bottomTeam);
		}
		if(topTeam.controllingPlayer == null){
			Game.createBattleTeamForAI(topTeam);
		}
		ArrayList<Gladiator> bottomTeamGladiators = new ArrayList<Gladiator>();
		ArrayList<Gladiator> topTeamGladiators = new ArrayList<Gladiator>();
		Game.currentBattle = battle;
		for(int i = 0; i < Constants.MAX_GLADIATORS_IN_BATTLE; i++){
			if(bottomTeam.battleGladiators[i] == null)
    			continue;
			int rows = 2;
    		int divider = Constants.MAX_GLADIATORS_IN_BATTLE / rows;
    		int x = i%divider;
    		int y = i/divider;
    		Game.currentBattle.map.mapTiles[x + bottom_offX][y + bottom_offY].gladiatorOnTile = bottomTeam.battleGladiators[i];
    		bottomTeam.battleGladiators[i].pos = new Position(x + bottom_offX, y + bottom_offY);
    		bottomTeamGladiators.add(bottomTeam.battleGladiators[i]);
    		bottomTeam.battleGladiators[i].matches++;
		}
		for(int i = 0; i < Constants.MAX_GLADIATORS_IN_BATTLE; i++){
			if(topTeam.battleGladiators[i] == null)
    			continue;
			int rows = 2;
    		int divider = Constants.MAX_GLADIATORS_IN_BATTLE / rows;
    		int x = i%divider;
    		int y = -(i/divider);
    		Game.currentBattle.map.mapTiles[x + top_offX][y + top_offY].gladiatorOnTile = topTeam.battleGladiators[i];
    		topTeam.battleGladiators[i].pos = new Position(x + top_offX, y + top_offY);
    		topTeamGladiators.add(topTeam.battleGladiators[i]);
    		topTeam.battleGladiators[i].matches++;
		}
		for(int i = 0; i < Constants.MAX_GLADIATORS_IN_BATTLE; i++){
			if(i < bottomTeamGladiators.size())
				Game.currentBattle.gladiatorBattleTurnOrder.add(bottomTeamGladiators.get(i));
			if(i < topTeamGladiators.size())
				Game.currentBattle.gladiatorBattleTurnOrder.add(topTeamGladiators.get(i));
		}
		if(Game.currentBattle.isAIonlyBattle){
			Game.currentBattle.startNewBattleTurn();
		} else {
			if(topTeam.controllingPlayer == null && bottomTeam.controllingPlayer != null){
				Game.currentTeam = bottomTeam;
				if(bottomTeamGladiators.size() > 0)
					Game.currentGladiator = bottomTeamGladiators.get(0);
			}
			else if(topTeam.controllingPlayer != null && bottomTeam.controllingPlayer == null){
				Game.currentTeam = topTeam;
				if(topTeamGladiators.size() > 0)
					Game.currentGladiator = topTeamGladiators.get(0);
			}
			int baseIncome = battle.getBaseIncome();//TODO: figure out how this should work
			//bottomTeam.money += baseIncome;
			//topTeam.money += baseIncome;
			bottomTeam.modifyMoney(baseIncome);
			topTeam.modifyMoney(baseIncome);
			bottomTeam.currentMonthIncome = baseIncome;
			topTeam.currentMonthIncome = baseIncome;
		}
		*/
	}
	
	public static void nextRecruitTurn(){
		/*
		if(Game.currentRecruitTeamIdx >= Game.teamRecruitTurnOrder.size()){
			Game.currentRecruitTeamIdx = 0;
			Game.currentRecruitRound++;
			if(Game.currentRecruitRound > Constants.RECRUIT_TURNS){
				Game.completeRecruits();
			}
		}
		Team team = Game.teamRecruitTurnOrder.get(Game.currentRecruitTeamIdx);
		Game.currentRecruitTeamIdx++;
		if(team.controllingPlayer == null){
			handleAIrecruitTurn();
		} else {
			Game.currentTeam = team;
		}
		*/
	}
	
	public static void buyItem(Item item){
		gamePanel.mainFrame.client.packetSender.sendBuyItemPacket(item);
		/*
		int itemSlot = -1;
		if(item.type == Item.TYPE_MELEE_WEAPON){
			itemSlot = Constants.MELEE_WEAPON_SLOT;
		}
		else if(item.type == Item.TYPE_RANGED_WEAPON){
			itemSlot = Constants.RANGED_WEAPON_SLOT;
		}
		else if(item.type == Item.TYPE_ARMOUR){
			itemSlot = Constants.ARMOUR_SLOT;
		}
		Item ownedItem = null;
		int ownedItemSellPrice = 0;
		if(Game.currentGladiator.items[itemSlot] != null){
			ownedItem = Game.currentGladiator.items[itemSlot];
			ownedItemSellPrice = ownedItem.getSellPrice();
		}
		int buyPrice = item.getCurrentPrice()-ownedItemSellPrice;
		if(Game.currentTeam.money < buyPrice){
			return;
		}
		Game.currentGladiator.items[itemSlot] = item;
		//Game.currentTeam.money -= buyPrice;
		Game.currentTeam.modifyMoney(-buyPrice);
		*/
	}
	
	public static void buySpell(Spell spell){
		gamePanel.mainFrame.client.packetSender.sendBuySpellPacket(spell);
		/*
		if(Game.currentGladiator.getKnownSpells().contains(spell)){
			return;
		}
		if(Game.currentTeam.money < spell.price){
			return;
		}
		Game.currentGladiator.knownSpells[Game.currentGladiator.getNextAvailableSpellSlot()] = spell;
		//Game.currentTeam.money -= spell.price;
		Game.currentTeam.modifyMoney(-spell.price);
		*/
	}
	
	public static void createAutoBattleTeam(){
		
		for(Gladiator gladiator : Game.currentTeam.gladiators){
			if(getBattleTeamSize() >= Constants.MAX_GLADIATORS_IN_BATTLE)
				return;
			if(gladiator.hurt > 0)
				continue;
			if(battleTeamHasGladiator(gladiator))
				continue;
			Game.currentTeam.battleGladiators[battleTeamGetNextEmptySlot()] = gladiator;
		}
		
	}
	
	public static int getBattleTeamSize(){
		int count = 0;
		for(int i = 0; i < Game.currentTeam.battleGladiators.length; i++){
			Gladiator gladiator = Game.currentTeam.battleGladiators[i];
			if(gladiator != null)
				count++;
		}
		return count;
	}
	
	public static int battleTeamGetNextEmptySlot(){
		int idx = -1;
		for(int i = 0; i < Game.currentTeam.battleGladiators.length; i++){
			Gladiator gladiator = Game.currentTeam.battleGladiators[i];
			if(gladiator == null)
				return i;
		}
		return idx;
	}
	
	public static void sellItem(int slot){
		gamePanel.mainFrame.client.packetSender.sendSellItemPacket(slot);
		/*
		Item item = Game.currentGladiator.items[slot];
		if(item == null)
			return;
		//Game.currentTeam.money += item.getSellPrice();
		Game.currentTeam.modifyMoney(item.getSellPrice());
		Game.currentGladiator.items[slot] = null;
		*/
	}
	
	public static void sellItem(Gladiator gladiator, int slot){
		/*
		Item item = gladiator.items[slot];
		if(item == null)
			return;
		gladiator.owner.modifyMoney(item.getSellPrice());
		gladiator.items[slot] = null;
		*/
	}
	
	public static int getTrainCost(String s){
		return 100;//TODO: figure out how this should work
	}
	
	public static String getAbilityName(String s){
		if(s.equals(GamePanel.TRAIN_STR_TXT)){
			return "Voima";
		}
		else if(s.equals(GamePanel.TRAIN_SPEED_TXT)){
			return "Nopeus";
		}
		else if(s.equals(GamePanel.TRAIN_HP_TXT)){
			return "Kesto";
		}
		else if(s.equals(GamePanel.TRAIN_MANA_TXT)){
			return "Mana";
		}
		return null;
	}
	
	public static int getCurrentRoundNumber(){
		if(MONTH == 0)
			return 1;
		if(MONTH == 1)
			return 2;
		if(MONTH == 3)
			return 3;
		if(MONTH == 5)
			return 4;
		if(MONTH == 7)
			return 5;
		if(MONTH == 9)
			return 6;
		return -1;
	}
	
}
