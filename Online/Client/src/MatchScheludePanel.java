import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JPanel;

public class MatchScheludePanel extends JPanel {

	private static final long serialVersionUID = 1L;

	//COLORS
	public static Color BACKGROUND_COLOR = new Color(224, 224, 224);
	
	//FONTS
	public static Font PANEL_FONT = new Font("courier new", Font.PLAIN, 12);
	public static Font BUTTON_FONT = new Font("Segoe UI", Font.BOLD, 12);
	
	//UI
	public static String MONTH_TITLE_TXT = "KUUKAUSI";
	public static String MATCH_TYPE_TITLE_TXT = "OTTELUTYYPPI";
	public static String OPPONENT_TITLE_TXT = "VASTUSTAJA";
	public static String RESULT_TITLE_TXT = "TULOS";
	
	public static int GAME_WINDOW_MATCH_SCHELUDE_VIEW = 14;
	
	public MatchScheludePanel(Jframe frame){
		this.mainFrame = frame;
		this.setLayout(null);
		this.add(button(GAME_WINDOW_MATCH_SCHELUDE_VIEW, "close match schelude view", "OK", BUTTON_FONT, 8, 432, 777, 105));
	}
	
	public void paintComponent (Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
	    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    g2d.setColor(BACKGROUND_COLOR);
	    g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
	    g2d.setColor(Color.BLACK);
	    g2d.setFont(PANEL_FONT);

	    g2d.drawString(MONTH_TITLE_TXT, 18, 30);
	    g2d.drawString(MATCH_TYPE_TITLE_TXT, 198, 30);
	    g2d.drawString(OPPONENT_TITLE_TXT, 354, 30);
	    g2d.drawString(RESULT_TITLE_TXT, 515, 30);
	    
	    int offY = 17;
	    
	    String curMonth = "-------->";
	    
	    for(int month = 0; month < Constants.MONTHS.length; month++){
	    	g2d.drawString(Constants.MONTHS[month], 18, 64+(offY*month));
	    	String opponent = "";
	    	if(Game.monthType[month] == Game.MONTH_TYPE_DIVISION_BATTLE){
	    		g2d.drawString("Sarjaottelu", 198, 64+(offY*month));
	    		opponent = Game.getDivisionBattleOpponentForTeam(team2show, month).name;
	    		g2d.drawString(opponent, 354, 64+(offY*month));
	    		String result = "Edess� p�in";
	    		if(team2show.battleResult[month] == Battle.WIN){
	    			result = "Voitto";
	    		} else if(team2show.battleResult[month] == Battle.LOSE){
	    			result = "Tappio";
	    		}
	    		g2d.drawString(result, 515, 64+(offY*month));
	    	}
	    	else if(Game.monthType[month] == Game.MONTH_TYPE_CUP_BATTLE){
	    		g2d.drawString("CUP-ottelu", 198, 64+(offY*month));
	    		Team opTeam = Game.getOpponentForCUPBattleForTeam(team2show, month);
	    		if(opTeam != null){
		    		g2d.drawString(opTeam.name, 354, 64+(offY*month));
	    		}
	    		String result = "Edess� p�in";
	    		int x = 515;
	    		if(team2show.battleResult[month] == Battle.WIN){
	    			result = "Voitto";
	    		} else if(team2show.battleResult[month] == Battle.LOSE){
	    			result = "Tappio";
	    		} else if(team2show.droppedFromCUP()){
	    			result = "Ollaan tiputtu CUP:ista!";
	    			x = 354;
	    		}
	    		g2d.drawString(result, x, 64+(offY*month));
	    	}
	    	else if(Game.monthType[month] == Game.MONTH_TYPE_WINTER_BREAK){
	    		g2d.drawString("TALVITAUKO", 198, 64+(offY*month));
	    	}
	    	if(month == Game.MONTH){
	    		g2d.drawString(curMonth, 100, 64+(offY*month));
	    	}
	    }
	}
	
	public void setTeam2show(Team team){
		team2show = team;
	}
	
	Team team2show;
	
	public void refresh(){
		this.revalidate();
		this.repaint();
	}
	
	public ArrayList<Button> buttons = new ArrayList<Button>();
	
	public JButton button(int windowName, String s, Font f, int x, int y, int width, int height){
		return button(windowName, s, s, f, x, y, width, height);
	}
	
	public JButton button(int windowName, String buttonName, String s, Font f, int x, int y, int width, int height){
		JButton jButton = new JButton(s);
		jButton.setForeground(Color.BLACK);
		jButton.setFont(f);
		jButton.setBounds(x, y, width, height);
		jButton.setMargin(new Insets(0, 0, 0, 0));
		jButton.setVisible(true);
		jButton.setActionCommand(buttonName);
		jButton.addActionListener(this.mainFrame);
		for(Button button : buttons){
			if(button.button_name.equals(buttonName)){
				System.out.println("[ERROR]: Button with same name already exists! "+buttonName);
			}
		}
		buttons.add(new Button(new int[]{windowName}, buttonName, jButton));
		return jButton;
	}
	
	public JButton button(int[] windowName, String s, Font f, int x, int y, int width, int height){
		return button(windowName, s, s, f, x, y, width, height);
	}
	
	public JButton button(int[] windowName, String buttonName, String s, Font f, int x, int y, int width, int height){
		JButton jButton = new JButton(s);
		jButton.setForeground(Color.BLACK);
		jButton.setFont(f);
		jButton.setBounds(x, y, width, height);
		jButton.setMargin(new Insets(0, 0, 0, 0));
		jButton.setVisible(true);
		jButton.setActionCommand(buttonName);
		jButton.addActionListener(this.mainFrame);
		for(Button button : buttons){
			if(button.button_name.equals(buttonName)){
				System.out.println("[ERROR]: Button with same name already exists! "+buttonName);
			}
		}
		buttons.add(new Button(windowName, buttonName, jButton));
		return jButton;
	}
	
	public JButton findButton(String s){
		for(Button button : buttons){
			if(button.button_name.toLowerCase().equals(s.toLowerCase())){
				return button.jButton;
			}
		}
		return null;
	}
	
	Jframe mainFrame;
	
}
