import java.util.ArrayList;

public class Item {

	public static ArrayList<Item> allItems = new ArrayList<Item>();
	
	public static ArrayList<Item> spears = new ArrayList<Item>();
	public static ArrayList<Item> swords = new ArrayList<Item>();
	public static ArrayList<Item> axes = new ArrayList<Item>();
	public static ArrayList<Item> maces = new ArrayList<Item>();
	public static ArrayList<Item> bows = new ArrayList<Item>();
	public static ArrayList<Item> crossbows = new ArrayList<Item>();
	public static ArrayList<Item> armours = new ArrayList<Item>();
	
	public static int TYPE_MELEE_WEAPON = 0;
	public static int TYPE_RANGED_WEAPON = 1;
	public static int TYPE_ARMOUR = 2;
	
	public static int SUBTYPE_MELEE_WEAPON_SPEAR = 0;
	public static int SUBTYPE_MELEE_WEAPON_SWORD = 1;
	public static int SUBTYPE_MELEE_WEAPON_AXE = 2;
	public static int SUBTYPE_MELEE_WEAPON_MACE = 3;
	
	public static int SUBTYPE_RANGED_WEAPON_BOW = 4;
	public static int SUBTYPE_RANGED_WEAPON_CROSSBOW = 5;
	
	String name;
	int price;
	boolean discounted = false;
	int hb_val;//hit bonus, only on melee weapons
	int pb_val;//block bonus, only on melee weapons
	int pp_val;//armor value
	String dmg;
	int type;
	int subtype;
	
	public Item(String name, int subtype, int price, String dmg, int hb_val, int pb_val){//MELEE WEAPONS
		this.name = name;
		this.type = TYPE_MELEE_WEAPON;
		this.subtype = subtype;
		this.price = price;
		this.dmg = dmg;
		this.hb_val = hb_val;
		this.pb_val = pb_val;
		if(subtype == SUBTYPE_MELEE_WEAPON_SPEAR){
			spears.add(this);
		} else if(subtype == SUBTYPE_MELEE_WEAPON_SWORD){
			swords.add(this);
		} else if(subtype == SUBTYPE_MELEE_WEAPON_AXE){
			axes.add(this);
		} else {
			maces.add(this);
		}
		allItems.add(this);
	}
	
	public Item(String name, int subtype, int price, String dmg){//RANGED WEAPONS
		this.name = name;
		this.type = TYPE_RANGED_WEAPON;
		this.subtype = subtype;
		this.price = price;
		this.dmg = dmg;
		if(subtype == SUBTYPE_RANGED_WEAPON_BOW){
			bows.add(this);
		} else {
			crossbows.add(this);
		}
		allItems.add(this);
	}
	
	public Item(String name, int price, int pp_val){//ARMOUR
		this.name = name;
		this.type = TYPE_ARMOUR;
		this.price = price;
		this.pp_val = pp_val;
		armours.add(this);
		allItems.add(this);
	}
	
	public String getSubTypeName(){
		if(subtype == SUBTYPE_MELEE_WEAPON_SPEAR){
			return "Keih�s";
		}
		if(subtype == SUBTYPE_MELEE_WEAPON_SWORD){
			return "Miekka";
		}
		if(subtype == SUBTYPE_MELEE_WEAPON_AXE){
			return "Kirves";
		}
		if(subtype == SUBTYPE_MELEE_WEAPON_MACE){
			return "Nuija";
		}
		if(subtype == SUBTYPE_RANGED_WEAPON_BOW){
			return "Jousi";
		}
		if(subtype == SUBTYPE_RANGED_WEAPON_CROSSBOW){
			return "Varsijousi";
		}
		return null;
	}
	
	public int getNormalPrice(){
		return this.price;
	}
	
	public int getDiscountedPrice(){
		Double d_price = (double) this.price;
		double discountPrice = Math.round(d_price * Constants.DISCOUNT_PRICE);
		int i_price = (int) discountPrice;
		return i_price;
	}
	
	public int getSellPrice(){
		Double d_price = (double) this.price;
		double sellPrice = Math.round(d_price * Constants.SELL_PRICE);
		int i_price = (int) sellPrice;
		return i_price;
	}
	
	public int getCurrentPrice(){
		if(this.discounted){
			return getDiscountedPrice();
		} else {
			return getNormalPrice();
		}
	}
	
	public static ArrayList<Item> discountedItems = new ArrayList<Item>();
	
	public static void setNewDiscountedItems(){
		for(Item item : discountedItems){
			item.discounted = false;
		}
		discountedItems.clear();
		addDiscountedItem(spears.get(Util.random_(spears.size())));
		addDiscountedItem(swords.get(Util.random_(swords.size())));
		addDiscountedItem(axes.get(Util.random_(axes.size())));
		addDiscountedItem(maces.get(Util.random_(maces.size())));
		addDiscountedItem(bows.get(Util.random_(bows.size())));
		addDiscountedItem(crossbows.get(Util.random_(crossbows.size())));
		addDiscountedItem(armours.get(Util.random_(armours.size())));

	}
	
	public static void addDiscountedItem(Item item){
		item.discounted = true;
		discountedItems.add(item);
	}
	
	public static ArrayList<Item> getDiscountedItems(){
		return discountedItems;
	}
	
	public static Item findItem(String itemName){
		for(Item item : allItems){
			if(item.name.equals(itemName)){
				return item;
			}
		}
		return null;
	}
	
	public static void initItems(){
		new Item("Ter�v� keppi", SUBTYPE_MELEE_WEAPON_SPEAR, 40, "1d4+1", 0, 0);
		new Item("Kivikeih�s", SUBTYPE_MELEE_WEAPON_SPEAR, 102, "1d6+1", 0, 5);
		new Item("Lyhyt keih�s", SUBTYPE_MELEE_WEAPON_SPEAR, 205, "1d8+1", 0, 10);
		new Item("Pitk� keih�s", SUBTYPE_MELEE_WEAPON_SPEAR, 328, "1d10+1", 5, 10);
		new Item("Suurkeih�s", SUBTYPE_MELEE_WEAPON_SPEAR, 683, "2d6+2", 5, 15);
		new Item("Jalkav�en peitsi", SUBTYPE_MELEE_WEAPON_SPEAR, 822, "3d6", 20, 0);
		new Item("Mystinen keih�s", SUBTYPE_MELEE_WEAPON_SPEAR, 1195, "1d8+8", 15, 10);
		new Item("Tulikeih�s", SUBTYPE_MELEE_WEAPON_SPEAR, 1504, "3d6+4", 25, 0);
		new Item("Siunattu keih�s", SUBTYPE_MELEE_WEAPON_SPEAR, 2433, "3d8+5", 5, 25);
		new Item("Mahtikeih�s", SUBTYPE_MELEE_WEAPON_SPEAR, 3534, "2d10+10", 10, 30);
		
		new Item("Tikari", SUBTYPE_MELEE_WEAPON_SWORD, 71, "1d4+1", 5, 0);
		new Item("Lyhyt Miekka", SUBTYPE_MELEE_WEAPON_SWORD, 160, "1d6+1", 5, 5);
		new Item("Sapeli", SUBTYPE_MELEE_WEAPON_SWORD, 230, "1d6+2", 10, 0);
		new Item("Leve� Miekka", SUBTYPE_MELEE_WEAPON_SWORD, 360, "1d8+2", 10, 5);
		new Item("Bastardimiekka", SUBTYPE_MELEE_WEAPON_SWORD, 376, "1d10+2", 5, 5);
		new Item("Suurmiekka", SUBTYPE_MELEE_WEAPON_SWORD, 683, "2d6+2", 15, 0);
		new Item("Musta Miekka", SUBTYPE_MELEE_WEAPON_SWORD, 1195, "1d8+8", 20, 0);
		new Item("Siunattu miekka", SUBTYPE_MELEE_WEAPON_SWORD, 1672, "2d8+4", 10, 20);
		new Item("Tulimiekka", SUBTYPE_MELEE_WEAPON_SWORD, 2270, "3d8+3", 20, 10);
		new Item("Mahtimiekka", SUBTYPE_MELEE_WEAPON_SWORD, 3946, "2d10+10", 20, 20);
		
		new Item("K�sikirves", SUBTYPE_MELEE_WEAPON_AXE, 51, "1d4+1", 0, 0);
		new Item("Kirves", SUBTYPE_MELEE_WEAPON_AXE, 129, "1d6+1", 5, 0);
		new Item("Taistelukirves", SUBTYPE_MELEE_WEAPON_AXE, 409, "1d8+2", 10, 5);
		new Item("Suurkirves", SUBTYPE_MELEE_WEAPON_AXE, 598, "2d6", 15, 5);
		new Item("Piilukirves", SUBTYPE_MELEE_WEAPON_AXE, 871, "2d6+2", 20, 0);
		new Item("Hilpari", SUBTYPE_MELEE_WEAPON_AXE, 1026, "3d6", 15, 5);
		new Item("Lumottu kirves", SUBTYPE_MELEE_WEAPON_AXE, 1638, "1d8+8", 20, 10);
		new Item("Mestarikirves", SUBTYPE_MELEE_WEAPON_AXE, 2073, "3d6+4", 15, 15);
		new Item("Siunattu kirves", SUBTYPE_MELEE_WEAPON_AXE, 2475, "2d6+8", 10, 25);
		new Item("Mahtikirves", SUBTYPE_MELEE_WEAPON_AXE, 4382, "2d10+10", 25, 15);
		
		new Item("Puunuija", SUBTYPE_MELEE_WEAPON_MACE, 25, "1d4", 0, 0);
		new Item("Pamppu", SUBTYPE_MELEE_WEAPON_MACE, 94, "1d6", 5, 0);
		new Item("Nuija", SUBTYPE_MELEE_WEAPON_MACE, 205, "1d8", 5, 5);
		new Item("Raskas nuija", SUBTYPE_MELEE_WEAPON_MACE, 360, "1d10", 10, 5);
		new Item("Moukari", SUBTYPE_MELEE_WEAPON_MACE, 577, "2d6", 15, 0);
		new Item("Sotamoukari", SUBTYPE_MELEE_WEAPON_MACE, 1137, "3d6", 15, 5);
		new Item("Lumottu nuija", SUBTYPE_MELEE_WEAPON_MACE, 1849, "1d8+8", 20, 10);
		new Item("Muinainen nuija", SUBTYPE_MELEE_WEAPON_MACE, 1886, "2d6+6", 25, 0);
		new Item("Siunattu nuija", SUBTYPE_MELEE_WEAPON_MACE, 3635, "3d6+8", 10, 25);
		new Item("Mahtimoukari", SUBTYPE_MELEE_WEAPON_MACE, 4898, "2d10+10", 30, 10);
		
		new Item("Lyhytjousi", SUBTYPE_RANGED_WEAPON_BOW, 172, "1d4+1");
		new Item("�rkkijousi", SUBTYPE_RANGED_WEAPON_BOW, 306, "1d6+1");
		new Item("Pitk�jousi", SUBTYPE_RANGED_WEAPON_BOW, 478, "1d8+1");
		new Item("Sarvijousi", SUBTYPE_RANGED_WEAPON_BOW, 688, "1d10+1");
		new Item("Haltiajousi", SUBTYPE_RANGED_WEAPON_BOW, 937, "1d12+1");
		new Item("Mestarijousi", SUBTYPE_RANGED_WEAPON_BOW, 1550, "2d6+2");
		new Item("Lumottu jousi", SUBTYPE_RANGED_WEAPON_BOW, 1913, "1d10+5");
		new Item("Pyh� haltiajousi", SUBTYPE_RANGED_WEAPON_BOW, 2755, "3d6+2");
		new Item("Siunattu jousi", SUBTYPE_RANGED_WEAPON_BOW, 3233, "1d12+7");
		new Item("Mahtijousi", SUBTYPE_RANGED_WEAPON_BOW, 4898, "2d10+5");
		
		new Item("Kevyt varsijousi", SUBTYPE_RANGED_WEAPON_CROSSBOW, 107, "1d4+2");
		new Item("Kevyt k��pi�varsijousi", SUBTYPE_RANGED_WEAPON_CROSSBOW, 172, "2d3+1");
		new Item("Varsijousi", SUBTYPE_RANGED_WEAPON_CROSSBOW, 234, "1d8+2");
		new Item("K��pi�varsijousi", SUBTYPE_RANGED_WEAPON_CROSSBOW, 552, "3d4+2");
		new Item("Raskas varsijousi", SUBTYPE_RANGED_WEAPON_CROSSBOW, 808, "2d6+4");
		new Item("Pyh� k��pi�varsijousi", SUBTYPE_RANGED_WEAPON_CROSSBOW, 971, "4d4+2");
		new Item("Lumottu varsijousi", SUBTYPE_RANGED_WEAPON_CROSSBOW, 1550, "2d6+8");
		new Item("Muinainen varsijousi", SUBTYPE_RANGED_WEAPON_CROSSBOW, 1727, "3d6+6");
		new Item("Siunattu varsijousi", SUBTYPE_RANGED_WEAPON_CROSSBOW, 2211, "2d8+9");
		new Item("Mahtivarsijousi", SUBTYPE_RANGED_WEAPON_CROSSBOW, 2989, "2d10+10");
		
		new Item("Nahkanuttu", 32, 1);
		new Item("Besaittuhaarniska", 128, 2);
		new Item("Silmukkahaarniska", 288, 3);
		new Item("Rengashaarniska", 512, 4);
		new Item("Suomuhaarniska", 800, 5);
		new Item("Levyhaarniska", 1152, 6);
		new Item("Mystinen haarniska", 1568, 7);
		new Item("Mithrilhaarniska", 2048, 8);
		new Item("Siunattu haarniska", 3200, 10);
		new Item("Mahtihaarniska", 4608, 12);
	}
	
}
