import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class NewGamePanel extends JPanel{

	private static final long serialVersionUID = 1L;
	public static Color BG_COLOR = new Color(238, 238, 238);
	public static Color BOX_COLOR = new Color(160, 160, 160);
	
	public static Font SMALL_BUTTON_FONT = new Font("comic sans ms", Font.PLAIN, 11);
	
	public static String START_GAME = "Aloita peli";
	public static String CANCEL = "Peruuta";
	public static String PLAYER_TEAMS = "Pelaajien joukkueet";
	public static String AUTOMATIC_START_TEAMS = "Pelaajille tehd��n automaattisesti aloitusjoukkueet";
	public static String SPECIAL_GLADIATORS = "Peliss� on ylivertaisia ja ylipalkattuja sankareita";
	public static String GAME_DIFFICULTY = "Pelin vaikeusaste";
	public static String P1_CHECKBOX = "P1 checkbox";
	public static String P2_CHECKBOX = "P2 checkbox";
	public static String P3_CHECKBOX = "P3 checkbox";
	public static String P4_CHECKBOX = "P4 checkbox";
	public static String P1_TEXTFIELD = "P1 textfield";
	public static String P2_TEXTFIELD = "P2 textfield";
	public static String P3_TEXTFIELD = "P3 textfield";
	public static String P4_TEXTFIELD = "P4 textfield";
	
	public static int GAME_WINDOW_NEW_GAME = 12;
	
	public void paintComponent (Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
	    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    
	    g2d.setColor(BOX_COLOR);
	    g2d.drawRect(8, 175, 319, 104);
	    g2d.setColor(BG_COLOR);
	    g2d.fillRect(15, 155, 104, 50);

	}
	
	public NewGamePanel(Jframe frame){
		this.mainFrame = frame;
		this.setLayout(null);
		this.setPreferredSize(new Dimension(341, 409));
		addText("<html>Mystisess� Riimukallion kaupungissa on j�lleen alkamassa<br><p style='margin-top:-2'>GLADIAATTORIEN koitos. Kuusitoista joukkuetta ottaa<br><p style='margin-top:-2'>toisistaan mittaa sarjassa, jossa ei armoa tunneta eik�<br><p style='margin-top:-2'>s��lipisteit� jaeta.<br><p style='margin-top:-2'><br><p style='margin-top:-2'>Kaikkien joukkueiden tavoitteena on voittaa arvostettu<br><p style='margin-top:-2'>SARJAMESTARUUS.<br><p style='margin-top:-2'><br><p style='margin-top:-2'>Muutaman joukkueen managerin paikka on t�ytt�m�tt�...</p></html>",
				SMALL_BUTTON_FONT, 8, 7);
		addText(PLAYER_TEAMS, SMALL_BUTTON_FONT, 17, 167);
		addText(GAME_DIFFICULTY, SMALL_BUTTON_FONT, 8, 343);
		
		addCheckbox(P1_CHECKBOX, "", SMALL_BUTTON_FONT, true, 12, 182);
		addCheckbox(P2_CHECKBOX, "", SMALL_BUTTON_FONT, false, 12, 206);
		addCheckbox(P3_CHECKBOX, "", SMALL_BUTTON_FONT, false, 12, 230);
		addCheckbox(P4_CHECKBOX, "", SMALL_BUTTON_FONT, false, 12, 254);
		
		String p1_teamName = "Gladiaattorit Oy";
		if(Game.isOnlineGame)
			p1_teamName = "";
		
		addTextField(P1_TEXTFIELD, p1_teamName, SMALL_BUTTON_FONT, 40, 184, 281, 19);
		addTextField(P2_TEXTFIELD, "", SMALL_BUTTON_FONT, 40, 208, 281, 19);
		addTextField(P3_TEXTFIELD, "", SMALL_BUTTON_FONT, 40, 232, 281, 19);
		addTextField(P4_TEXTFIELD, "", SMALL_BUTTON_FONT, 40, 256, 281, 19);
		
		addCheckbox(AUTOMATIC_START_TEAMS, SMALL_BUTTON_FONT, true, 5, 291);
		addCheckbox(SPECIAL_GLADIATORS, SMALL_BUTTON_FONT, true, 5, 315);
		
		addCombobox(GAME_DIFFICULTY, Constants.GAME_DIFFICULTY, SMALL_BUTTON_FONT, 1, 112, 344, 217, 23);
		
		addButton(START_GAME, SMALL_BUTTON_FONT, 8, 376, 233, 25);
		addButton(CANCEL, SMALL_BUTTON_FONT, 248, 376, 81, 25);
		
		updateButtons();
	}
	
	public void updateButtons(){
		if(!Game.isOnlineGame){
			if(!findCheckBox(P1_CHECKBOX).isSelected() && !findCheckBox(P2_CHECKBOX).isSelected()
					&& !findCheckBox(P3_CHECKBOX).isSelected() && !findCheckBox(P4_CHECKBOX).isSelected()){
				findButton(START_GAME).setEnabled(false);
			} else {
				findButton(START_GAME).setEnabled(true);
			}
			findTextField(P1_TEXTFIELD).setEnabled(findCheckBox(P1_CHECKBOX).isSelected());
			findTextField(P2_TEXTFIELD).setEnabled(findCheckBox(P2_CHECKBOX).isSelected());
			findTextField(P3_TEXTFIELD).setEnabled(findCheckBox(P3_CHECKBOX).isSelected());
			findTextField(P4_TEXTFIELD).setEnabled(findCheckBox(P4_CHECKBOX).isSelected());
		} else {
			findTextField(P1_TEXTFIELD).setEnabled(false);
			findTextField(P2_TEXTFIELD).setEnabled(false);
			findTextField(P3_TEXTFIELD).setEnabled(false);
			findTextField(P4_TEXTFIELD).setEnabled(false);
			findCheckBox(P1_CHECKBOX).setEnabled(false);
			findCheckBox(P2_CHECKBOX).setEnabled(false);
			findCheckBox(P3_CHECKBOX).setEnabled(false);
			findCheckBox(P4_CHECKBOX).setEnabled(false);
			if(Game.ownPlayer != null){
				if(Game.ownPlayer == Player.getPlayers().get(0)){
					findCheckBox(AUTOMATIC_START_TEAMS).setEnabled(true);
					findButton(START_GAME).setEnabled(true);
				} else {
					findCheckBox(AUTOMATIC_START_TEAMS).setEnabled(false);
					findButton(START_GAME).setEnabled(false);
				}
			}
			for(int i = 0; i < Constants.MAX_PLAYERS; i++){
				Player player = null;
				if(i < Player.getPlayers().size())
					player = Player.getPlayers().get(i);
				if(player != null){
					if(i == 0){
						findCheckBox(P1_CHECKBOX).setSelected(true);
						findTextField(P1_TEXTFIELD).setText(player.teamName);
						/*
						findCheckBox(AUTOMATIC_START_TEAMS).setEnabled(true);
						findButton(START_GAME).setEnabled(true);
						*/
					}
					else if(i == 1){
						findCheckBox(P2_CHECKBOX).setSelected(true);
						findTextField(P2_TEXTFIELD).setText(player.teamName);
					}
					else if(i == 2){
						findCheckBox(P3_CHECKBOX).setSelected(true);
						findTextField(P3_TEXTFIELD).setText(player.teamName);
					}
					else if(i == 3){
						findCheckBox(P4_CHECKBOX).setSelected(true);
						findTextField(P4_TEXTFIELD).setText(player.teamName);
					}
				} else {
					if(i == 0){
						findCheckBox(P1_CHECKBOX).setSelected(false);
						findTextField(P1_TEXTFIELD).setText("");
					}
					else if(i == 1){
						findCheckBox(P2_CHECKBOX).setSelected(false);
						findTextField(P2_TEXTFIELD).setText("");
					}
					else if(i == 2){
						findCheckBox(P3_CHECKBOX).setSelected(false);
						findTextField(P3_TEXTFIELD).setText("");
					}
					else if(i == 3){
						findCheckBox(P4_CHECKBOX).setSelected(false);
						findTextField(P4_TEXTFIELD).setText("");
					}
				}
			}
		}
		//TODO: add support for stuff below
		findCheckBox(SPECIAL_GLADIATORS).setSelected(false);
		findCheckBox(SPECIAL_GLADIATORS).setEnabled(false);
		findComboBox(GAME_DIFFICULTY).setEnabled(false);
	}
	
	public void startNewGame(){
		ArrayList<String> playerTeams = new ArrayList<String>();
		if(findCheckBox(P1_CHECKBOX).isSelected()){
			playerTeams.add(findTextField(P1_TEXTFIELD).getText());
		}
		if(findCheckBox(P2_CHECKBOX).isSelected()){
			playerTeams.add(findTextField(P2_TEXTFIELD).getText());
		}
		if(findCheckBox(P3_CHECKBOX).isSelected()){
			playerTeams.add(findTextField(P3_TEXTFIELD).getText());
		}
		if(findCheckBox(P4_CHECKBOX).isSelected()){
			playerTeams.add(findTextField(P4_TEXTFIELD).getText());
		}
		//Game.startNewGame(playerTeams, findCheckBox(AUTOMATIC_START_TEAMS).isSelected());
	}
	
	public void addCombobox(String comboBox_name, String[] options, Font f, int defaultSelection, int x, int y, int width, int height){
		JComboBox<String> jComboBox = new JComboBox<>(options);
        jComboBox.setBounds(x, y, width, height);
        jComboBox.setFont(f);
        jComboBox.setSelectedIndex(defaultSelection);
		this.add(jComboBox);
		for(ComboBox comboBox : comboBoxes){
			if(comboBox.comboBox_name.equals(comboBox_name)){
				System.out.println("[ERROR]: ComboBox with same name already exists! "+comboBox_name);
			}
		}
		comboBoxes.add(new ComboBox(comboBox_name, jComboBox));
	}
	
	public void addCheckbox(String text, Font f, boolean checked, int x, int y){
		addCheckbox(text, text, f, checked, x, y);
	}
	
	public void addCheckbox(String checkBox_name, String text, Font f, boolean checked, int x, int y){
		JCheckBox c = new JCheckBox(text);
		c.setFont(f);
		c.setSelected(checked);
		c.setActionCommand(checkBox_name);
		c.addActionListener(this.mainFrame);
		this.add(c);
		Dimension size = c.getPreferredSize();
		c.setBounds(x, y, size.width, size.height);
		for(CheckBox checkBox : checkBoxes){
			if(checkBox.checkBox_name.equals(checkBox_name)){
				System.out.println("[ERROR]: CheckBox with same name already exists! "+checkBox_name);
			}
		}
		checkBoxes.add(new CheckBox(checkBox_name, c));
	}
	
	public void addTextField(String textField_name, String text, Font f, int x, int y, int width, int height){
		JTextField textField = new JTextField(text);
		textField.setFont(f);
		this.add(textField);
		textField.setBounds(x, y, width, height);
		for(TextField textField_ : textFields){
			if(textField_.textField_name.equals(textField_name)){
				System.out.println("[ERROR]: TextField with same name already exists! "+textField_name);
			}
		}
		textFields.add(new TextField(textField_name, textField));
	}
	
	public void addText(String text, Font f, int x, int y){
		JLabel label = new JLabel(text);
		label.setFont(f);
		this.add(label);
		Dimension size = label.getPreferredSize();
		label.setBounds(x, y, size.width, size.height);
	}
	
	public void addButton(String text, Font f, int x, int y, int width, int height){
		addButton(text, text, f, x, y, width, height);
	}
	
	public void addButton(String buttonName, String text, Font f, int x, int y, int width, int height){
		JButton button = new JButton(text);
		button.setFont(f);
		this.add(button);
		button.setActionCommand(buttonName);
		button.setBounds(x, y, width, height);
		button.addActionListener(this.mainFrame);
		for(Button button_ : buttons){
			if(button_.button_name.equals(buttonName)){
				System.out.println("[ERROR]: Button with same name already exists! "+buttonName);
			}
		}
		buttons.add(new Button(new int[]{GAME_WINDOW_NEW_GAME}, buttonName, button));
	}
	
	public void refresh(){
		this.revalidate();
		this.repaint();
	}
	
	public ArrayList<CheckBox> checkBoxes = new ArrayList<CheckBox>();
	
	public JCheckBox findCheckBox(String s){
		for(CheckBox checkBox : checkBoxes){
			if(checkBox.checkBox_name.toLowerCase().equals(s.toLowerCase())){
				return checkBox.jCheckBox;
			}
		}
		return null;
	}
	
	public ArrayList<TextField> textFields = new ArrayList<TextField>();
	
	public JTextField findTextField(String s){
		for(TextField textField : textFields){
			if(textField.textField_name.toLowerCase().equals(s.toLowerCase())){
				return textField.jTextField;
			}
		}
		return null;
	}
	
	public ArrayList<Button> buttons = new ArrayList<Button>();
	
	public JButton findButton(String s){
		for(Button button : buttons){
			if(button.button_name.toLowerCase().equals(s.toLowerCase())){
				return button.jButton;
			}
		}
		return null;
	}
	
	public ArrayList<ComboBox> comboBoxes = new ArrayList<ComboBox>();
	
	public JComboBox findComboBox(String s){
		for(ComboBox comboBox : comboBoxes){
			if(comboBox.comboBox_name.toLowerCase().equals(s.toLowerCase())){
				return comboBox.jComboBox;
			}
		}
		return null;
	}
	
	Jframe mainFrame;
	
}
