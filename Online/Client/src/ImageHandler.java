import java.awt.*;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.awt.image.*;

public class ImageHandler {
	
	public static String RESOURCE_FOLDER = "./res/"+Game.mods[Game.currentMod]+"/";
	
	public static Toolkit toolkit = Toolkit.getDefaultToolkit();
	
	public static Image[] hitmarkImages = new Image[5];
	public static Image[] gladiatorImages = new Image[Race.races.size()];
	public static Image titleScreenImage;
	public static Image[] mapImages = new Image[1];
	public static Image[] clipmapImages = new Image[1];
	public static Image[] battleDirArrowImages = new Image[8];
	public static Image battleSetupArrowImage;
	public static Image shootCursorImage;
	
	public static void loadImages(){
		titleScreenImage = toolkit.getImage(RESOURCE_FOLDER+"titlescreen background.png");
		for(int i = 0; i < gladiatorImages.length; i++){
			Image image = toolkit.getImage(RESOURCE_FOLDER+"gladiators/"+i+".png");
			image = ImageHandler.makeColorTransparent(image, new Color(0xff00ff));
			gladiatorImages[i] = image;
		}
		for(int i = 0; i < 1; i++){
			Image image = toolkit.getImage(RESOURCE_FOLDER+"maps/"+i+" image.png");
			image = ImageHandler.makeColorTransparent(image, new Color(0xff00ff));
			mapImages[i] = image;
		}
		for(int i = 0; i < 1; i++){
			Image image = toolkit.getImage(RESOURCE_FOLDER+"maps/"+i+" clip.png");
			image = ImageHandler.makeColorTransparent(image, new Color(0xff00ff));
			clipmapImages[i] = image;
		}
		for(int i = 0; i < 8; i++){
			Image image = toolkit.getImage(RESOURCE_FOLDER+"battle/"+i+".png");
			image = ImageHandler.makeColorTransparent(image, new Color(0xff00ff));
			battleDirArrowImages[i] = image;
		}
		for(int i = 0; i < 5; i++){
			Image image = toolkit.getImage(RESOURCE_FOLDER+"hit/"+i+".png");
			image = ImageHandler.makeColorTransparent(image, new Color(0xff00ff));
			hitmarkImages[i] = image;
		}
		Image image = toolkit.getImage(RESOURCE_FOLDER+"battle setup arrow.png");
		image = ImageHandler.makeColorTransparent(image, new Color(0xff00ff));
		battleSetupArrowImage = image;
		image = toolkit.getImage(RESOURCE_FOLDER+"shoot.png");
		image = ImageHandler.makeColorTransparent(image, new Color(0xff00ff));
		shootCursorImage = image;
	}

	public static Image makeColorTransparent(Image im, final Color color) {
		ImageFilter filter = new RGBImageFilter() {
      // the color we are looking for... Alpha bits are set to opaque
      public int markerRGB = color.getRGB() | 0xFF000000;

      public final int filterRGB(int x, int y, int rgb) {
        if ( ( rgb | 0xFF000000 ) == markerRGB ) {
          // Mark the alpha bits as zero - transparent
          return 0x00FFFFFF & rgb;
          }
        else {
          // nothing to do
          return rgb;
          }
        }
      }; 

    ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
    return Toolkit.getDefaultToolkit().createImage(ip);
    }
	
	// This method returns a buffered image with the contents of an image
	public static BufferedImage toBufferedImage(Image image) {
    if (image instanceof BufferedImage) {
        return (BufferedImage)image;
    }

    // This code ensures that all the pixels in the image are loaded
    image = new ImageIcon(image).getImage();

    // Determine if the image has transparent pixels; for this method's
    // implementation, see Determining If an Image Has Transparent Pixels
   // boolean hasAlpha = hasAlpha(image);

    // Create a buffered image with a format that's compatible with the screen
    BufferedImage bimage = null;
    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    try {
        // Determine the type of transparency of the new buffered image
        int transparency = Transparency.OPAQUE;
        /*if (hasAlpha) {
            transparency = Transparency.BITMASK;
        }*/

        // Create the buffered image
        GraphicsDevice gs = ge.getDefaultScreenDevice();
        GraphicsConfiguration gc = gs.getDefaultConfiguration();
        bimage = gc.createCompatibleImage(
            image.getWidth(null), image.getHeight(null), transparency);
    } catch (HeadlessException e) {
        // The system does not have a screen
    }

    if (bimage == null) {
        // Create a buffered image using the default color model
        int type = BufferedImage.TYPE_INT_RGB;
        /*if (hasAlpha) {
            type = BufferedImage.TYPE_INT_ARGB;
        }*/
        bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), type);
    }

    // Copy image to buffered image
    Graphics g = bimage.createGraphics();

    // Paint the image onto the buffered image
    g.drawImage(image, 0, 0, null);
    g.dispose();

    return bimage;
	}

	// This method returns an Image object from a buffered image
	public static Image toImage(BufferedImage bufferedImage) {
		return Toolkit.getDefaultToolkit().createImage(bufferedImage.getSource());
	}
	
}