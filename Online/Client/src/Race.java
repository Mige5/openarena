import java.awt.Image;
import java.util.ArrayList;

public class Race {

	public static ArrayList<Race> races = new ArrayList<Race>();
	
	String name;
	String str;
	String speed;
	String mana;
	String health;
	
	public Race(String name, String str, String speed, String mana, String health){
		this.name = name;
		this.str = str;
		this.speed = speed;
		this.mana = mana;
		this.health = health;
		races.add(this);
	}
	
	public int getIdx(){
		for(int i = 0; i < races.size(); i++){
			Race race = races.get(i);
			if(race == this)
				return i;
		}
		return -1;
	}
	
	public static void initRaces(){
		if(Game.currentMod == Game.MOD_AREENA_5){
			new Race("Amatsoni", "3d6+3", "3d6+3", "2d6", "2d6+12");//0*
			new Race("Barbaari", "3d6+4", "3d6+2", "2d6", "2d6+12");//1*
			new Race("Druidi", "2d6", "2d6", "4d6+6", "2d6+12");//2*
			new Race("Feenix", "2d6", "3d6", "3d6+9", "2d6+9");//3* (should be 1px right)
			new Race("Gnolli", "4d6+8", "2d6", "1d6", "3d6+10");//4*
			new Race("Goblin", "2d6", "4d6+6", "2d6+4", "2d6+8");//5*
			new Race("Goblinvelho", "1d6", "3d6+6", "4d6+6", "2d6+6");//6*
			new Race("Haltia", "2d6", "3d6+6", "3d6+2", "2d6+10");//7*
			new Race("Hobitti", "1d6+2", "4d6+4", "2d6+6", "3d6+6");//8*
			new Race("Imp", "2d6", "3d6+6", "3d6+6", "2d6+6");//9*
			new Race("J�nis", "2d6", "4d6+6", "2d6", "2d6+12");//10*
			new Race("J��mies", "3d6", "1d6", "3d6+6", "3d6+12");//11*
			new Race("Kaamio", "2d6+6", "2d6", "2d6", "4d6+12");//12*
			new Race("Karhumies", "4d6", "1d6+4", "2d6", "3d6+14");//13*
			new Race("Kivihirvi�", "5d6+6", "1d6", "1d6", "4d6+12");//14* (should be 1px up)
			new Race("K��pi�", "3d6+2", "2d6", "2d6", "3d6+16");//15*
			new Race("K��pi�velho", "3d6", "1d6+6", "3d6+4", "3d6+8");//16*
			new Race("Liskomies", "3d6", "2d6", "2d6+8", "3d6+10");//17*
			
			new Race("Minotauri", "4d6+10", "1d6", "2d6", "3d6+8");//18*
			new Race("Munkki", "2d6+6", "3d6", "2d6+6", "3d6+6");//19*
			new Race("Muumio", "2d6+6", "1d6", "2d6+6", "5d6+6");//20*
			new Race("Mystikko", "2d6", "2d6+2", "4d6+4", "2d6+12");//21*
			new Race("Ninja", "2d6+6", "4d6", "2d6", "2d6+12");//22*
			new Race("Noita", "2d6", "2d6", "4d6+6", "2d6+12");//23*
			new Race("Otus", "5d6+6", "1d6", "1d6", "3d6+12");//24*
			new Race("Parantajatar", "2d6", "3d6", "3d6+6", "2d6+12");//25*
			new Race("Peikko", "4d6+6", "2d6", "1d6", "3d6+12");//26*
			new Race("Ritari", "3d6+2", "3d6+1", "2d6+3", "2d6+12");//27*
			new Race("Rosvo", "2d6+6", "3d6+4", "2d6+2", "3d6+6");//28*
			new Race("Samooja", "2d6+4", "2d6+4", "2d6+4", "3d6+6");//29*
			new Race("Sumo", "3d6+8", "2d6", "2d6", "3d6+10");//30*
			new Race("Tonttu", "1d6", "6d6+8", "2d6+2", "1d6+8");//31*
			new Race("Velho", "2d6", "2d6", "4d6+6", "2d6+12");//32* (should be 1px up)
			new Race("Velhotar", "1d6", "3d6+6", "3d6+6", "3d6+6");//33*
			new Race("Zombie", "4d6", "2d6", "1d6", "3d6+18");//34*
			new Race("�rkki", "2d6+4", "3d6+4", "3d6", "2d6+10");//35*
		} else if (Game.currentMod == Game.MOD_OPEN_ARENA){
			new Race("Demoni", "3d6+3", "3d6+3", "2d6", "2d6+12");//0*
			new Race("Kentauri", "3d6+4", "3d6+2", "2d6", "2d6+12");//1*
			new Race("Driadi", "2d6", "2d6", "4d6+6", "2d6+12");//2*
			new Race("Enkeli", "2d6", "3d6", "3d6+9", "2d6+9");//3*
			new Race("Gnolli", "4d6+8", "2d6", "1d6", "3d6+10");//4*
			new Race("Goblin", "2d6", "4d6+6", "2d6+4", "2d6+8");//5*
			new Race("Pime�ritari", "1d6", "3d6+6", "4d6+6", "2d6+6");//6*
			new Race("Nagavelho", "2d6", "3d6+6", "3d6+2", "2d6+10");//7*
			new Race("Hobitti", "1d6+2", "4d6+4", "2d6+6", "3d6+6");//8*
			new Race("Imp", "2d6", "3d6+6", "3d6+6", "2d6+6");//9*
			new Race("Haamu", "2d6", "4d6+6", "2d6", "2d6+12");//10*
			new Race("Haltiapappi", "3d6", "1d6", "3d6+6", "3d6+12");//11*
			new Race("Haltiaritari", "2d6+6", "2d6", "2d6", "4d6+12");//12*
			new Race("Harpyija", "4d6", "1d6+4", "2d6", "3d6+14");//13*
			new Race("Kivihirvi�", "5d6+6", "1d6", "1d6", "4d6+12");//14*
			new Race("K��pi�", "3d6+2", "2d6", "2d6", "3d6+16");//15*
			new Race("Zombiej�tti", "3d6", "1d6+6", "3d6+4", "3d6+8");//16*
			new Race("Liskomies", "3d6", "2d6", "2d6+8", "3d6+10");//17*
			
			new Race("Minotauri", "4d6+10", "1d6", "2d6", "3d6+8");//18*
			new Race("Kyklooppi", "2d6+6", "3d6", "2d6+6", "3d6+6");//19*
			new Race("Muumio", "2d6+6", "1d6", "2d6+6", "5d6+6");//20*
			new Race("Merenneito", "2d6", "2d6+2", "4d6+4", "2d6+12");//21*
			new Race("Naga", "2d6+6", "4d6", "2d6", "2d6+12");//22*
			new Race("Noita", "2d6", "2d6", "4d6+6", "2d6+12");//23*
			new Race("Fauni", "5d6+6", "1d6", "1d6", "3d6+12");//24*
			new Race("Pyh�kentauri", "2d6", "3d6", "3d6+6", "2d6+12");//25*
			new Race("Peikko", "4d6+6", "2d6", "1d6", "3d6+12");//26*
			new Race("Ritari", "3d6+2", "3d6+1", "2d6+3", "2d6+12");//27*
			new Race("Pyh�ritari", "2d6+6", "3d6+4", "2d6+2", "3d6+6");//28*
			new Race("J�tti", "2d6+4", "2d6+4", "2d6+4", "3d6+6");//29*
			new Race("Luuranko", "3d6+8", "2d6", "2d6", "3d6+10");//30*
			new Race("Tonttu", "1d6", "6d6+8", "2d6+2", "1d6+8");//31*
			new Race("Velho", "2d6", "2d6", "4d6+6", "2d6+12");//32*
			new Race("Vampyyri", "1d6", "3d6+6", "3d6+6", "3d6+6");//33*
			new Race("�rkkivelho", "4d6", "2d6", "1d6", "3d6+18");//34*
			new Race("�rkki", "2d6+4", "3d6+4", "3d6", "2d6+10");//35*
		}
	}
	
}
