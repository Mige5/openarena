
public class Map {

	public MapTile[][] mapTiles = new MapTile[Constants.MAP_WIDTH][Constants.MAP_HEIGHT];
	
	public Map(){
		mapTiles = new MapTile[Constants.MAP_WIDTH][Constants.MAP_HEIGHT];
		for(int x = 0; x < Constants.MAP_WIDTH; x++){
			for(int y = 0; y < Constants.MAP_HEIGHT; y++){
				mapTiles[x][y] = new MapTile();
			}
		}
	}
	
}
