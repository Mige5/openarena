import javax.swing.JButton;

public class Button {

	public JButton jButton;
	public String button_name;
	public int[] window_name;
	
	public Button(int[] window_name, String button_name, JButton jButton){
		this.window_name = window_name;
		this.button_name = button_name;
		this.jButton = jButton;
	}
	
	
}
