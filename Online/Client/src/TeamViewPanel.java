import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

public class TeamViewPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	//COLORS
	public static Color BACKGROUND_COLOR = new Color(0, 0, 128);
	public static Color GLADIATOR_BACKGROUND_COLOR = new Color(0, 255, 0);
	public static Color KO_GLADIATOR_BACKGROUND_COLOR = new Color(192, 192, 192);
	
	//FONTS
	public static Font PANEL_FONT = new Font("comic sans ms", Font.PLAIN, 11);
	public static Font HURT_FONT = new Font("arial", Font.PLAIN, 9);
	
	//UI
	public static String MATCHES_TXT = "Ottelut";
	public static String ARMOR_TXT = "Panssari";
	public static String OTHER_ABILITY_TXT_2 = "Loitsunvast";
	public static int GAME_WINDOW_TEAM_VIEW = 10;
	
	public TeamViewPanel(Jframe frame){
		this.mainFrame = frame;
		this.setLayout(null);
		this.add(button(GAME_WINDOW_TEAM_VIEW, "close team view", "Ok", PANEL_FONT, 8, 512, 777, 25));
	}
	
	public void setTeam2show(Team team){
		team2show = team;
	}
	
	public void paintComponent (Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
	    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    g2d.setColor(BACKGROUND_COLOR);
	    g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
	    int offY = 56;
	    for(int i = 0; i < team2show.gladiators.size(); i++){
	    	Gladiator gladiator = team2show.gladiators.get(i);
	    	if(gladiator.hurt == 0)
	    		g2d.setColor(GLADIATOR_BACKGROUND_COLOR);
	    	else
	    		g2d.setColor(KO_GLADIATOR_BACKGROUND_COLOR);
	    	g2d.fillRect(0, 8+(i*offY), 791, 53);
	    	g2d.setFont(PANEL_FONT);
	    	g2d.setColor(Color.BLACK);
	    	g2d.drawLine(200, 24+(i*offY), 200, 24+(i*offY)+31);
	    	g2d.drawLine(360, 24+(i*offY), 360, 24+(i*offY)+31);
	    	g2d.drawLine(592, 24+(i*offY), 592, 24+(i*offY)+31);
	    	g2d.drawLine(704, 24+(i*offY), 704, 24+(i*offY)+31);
	    	g2d.drawString(Util.underlinedString(gladiator.name, PANEL_FONT), 0, 20+(i*offY));
	    	g2d.drawString(GamePanel.AGE_TXT, 40, 36+(i*offY));
	    	g2d.drawString(GamePanel.SALARY_TXT, 40, 52+(i*offY));
	    	g2d.drawString(GamePanel.TAKEOUTS_TXT, 112, 36+(i*offY));
	    	g2d.drawString(MATCHES_TXT, 112, 52+(i*offY));
	    	g2d.drawString(GamePanel.HP_TXT, 216, 36+(i*offY));
	    	g2d.drawString(GamePanel.MANA_TXT, 216, 52+(i*offY));
	    	g2d.drawString(GamePanel.STR_TXT, 288, 36+(i*offY));
	    	g2d.drawString(GamePanel.SPEED_TXT, 288, 52+(i*offY));
	    	g2d.drawString(GamePanel.OTHER_ABILITY_TXT_1, 608, 36+(i*offY));
	    	g2d.drawString(OTHER_ABILITY_TXT_2, 608, 52+(i*offY));
	    	g2d.drawString(ARMOR_TXT, 720, 36+(i*offY));
	    	g2d.drawString(GamePanel.MORAL_TXT_1, 720, 52+(i*offY));
	    	
	    	g2d.drawString(""+gladiator.age, 80, 36+(i*offY));
	    	g2d.drawString(""+gladiator.salary, 80, 52+(i*offY));
	    	g2d.drawString(gladiator.takeOuts_1+"/"+gladiator.takeOuts_2, 151, 36+(i*offY));
	    	g2d.drawString(""+gladiator.matches, 151, 52+(i*offY));
	    	g2d.drawString(gladiator.hp+"/"+gladiator.maxHp, 248, 36+(i*offY));
	    	g2d.drawString(gladiator.mana+"/"+gladiator.maxMana, 248, 52+(i*offY));
	    	g2d.drawString(""+gladiator.str, 328, 36+(i*offY));
	    	g2d.drawString(""+gladiator.speed, 328, 52+(i*offY));
	    	
	    	String melee_wep_name = "Nyrkki";
	    	String melee_dmg_val = gladiator.getMeleeDmgValue();
	    	if(gladiator.getMeleeWeapon() != null){
	    		melee_wep_name = gladiator.getMeleeWeapon().name;
	    	}
	    	String ranged_wep_name = "-";
	    	int ranged_atkVal = 0;
	    	String ranged_dmg_val = "";
	    	if(gladiator.getRangedWeapon() != null){
	    		ranged_wep_name = gladiator.getRangedWeapon().name;
	    		ranged_atkVal = gladiator.getAtkSkillValueForWeapon(gladiator.getRangedWeapon());
	    		ranged_dmg_val = gladiator.getRangedDmgValue();
	    	}
	    	int melee_atkVal = gladiator.getAtkSkillValueForWeapon(gladiator.getMeleeWeapon());
	    	int melee_blockVal = gladiator.getDefSkillValueForWeapon(gladiator.getMeleeWeapon());
	    	
	    	g2d.drawString(melee_wep_name, 376, 36+(i*offY));
	    	g2d.drawString(ranged_wep_name, 376, 52+(i*offY));
	    	
	    	g2d.drawString(""+melee_atkVal, 488, 36+(i*offY));
	    	if(ranged_atkVal > 0)
	    		g2d.drawString(""+ranged_atkVal, 488, 52+(i*offY));
	    	g2d.drawString(""+melee_blockVal, 512, 36+(i*offY));
	    	
	    	g2d.drawString(melee_dmg_val, 536, 36+(i*offY));
	    	if(!ranged_dmg_val.equals(""))
	    		g2d.drawString(ranged_dmg_val, 536, 52+(i*offY));
	    	
	    	g2d.drawString(""+gladiator.ability1, 672, 36+(i*offY));
	    	g2d.drawString(""+gladiator.ability2, 672, 52+(i*offY));
	    	g2d.drawString(""+gladiator.getArmorValue(), 777, 36+(i*offY));
	    	g2d.drawString(""+gladiator.moral, 777, 52+(i*offY));
	    	
	    	g2d.drawImage(ImageHandler.gladiatorImages[gladiator.race.getIdx()], 8, 24+(i*offY), this);
	    	
	    	if(gladiator.hurt > 0){
	    		g2d.setFont(HURT_FONT);
	    		g2d.setColor(Color.RED);
	    		g2d.drawString(""+gladiator.hurt, 9, 32+(i*offY));
	    	}
	    }
	}
	
	public void refresh(){
		this.revalidate();
		this.repaint();
	}
	
	public ArrayList<Button> buttons = new ArrayList<Button>();
	
	public JButton button(int windowName, String s, Font f, int x, int y, int width, int height){
		return button(windowName, s, s, f, x, y, width, height);
	}
	
	public JButton button(int windowName, String buttonName, String s, Font f, int x, int y, int width, int height){
		JButton jButton = new JButton(s);
		jButton.setForeground(Color.BLACK);
		jButton.setFont(f);
		jButton.setBounds(x, y, width, height);
		jButton.setMargin(new Insets(0, 0, 0, 0));
		jButton.setVisible(true);
		jButton.setActionCommand(buttonName);
		jButton.addActionListener(this.mainFrame);
		for(Button button : buttons){
			if(button.button_name.equals(buttonName)){
				System.out.println("[ERROR]: Button with same name already exists! "+buttonName);
			}
		}
		buttons.add(new Button(new int[]{windowName}, buttonName, jButton));
		return jButton;
	}
	
	public JButton button(int[] windowName, String s, Font f, int x, int y, int width, int height){
		return button(windowName, s, s, f, x, y, width, height);
	}
	
	public JButton button(int[] windowName, String buttonName, String s, Font f, int x, int y, int width, int height){
		JButton jButton = new JButton(s);
		jButton.setForeground(Color.BLACK);
		jButton.setFont(f);
		jButton.setBounds(x, y, width, height);
		jButton.setMargin(new Insets(0, 0, 0, 0));
		jButton.setVisible(true);
		jButton.setActionCommand(buttonName);
		jButton.addActionListener(this.mainFrame);
		for(Button button : buttons){
			if(button.button_name.equals(buttonName)){
				System.out.println("[ERROR]: Button with same name already exists! "+buttonName);
			}
		}
		buttons.add(new Button(windowName, buttonName, jButton));
		return jButton;
	}
	
	public JButton findButton(String s){
		for(Button button : buttons){
			if(button.button_name.toLowerCase().equals(s.toLowerCase())){
				return button.jButton;
			}
		}
		return null;
	}
	
	Team team2show;
	
	Jframe mainFrame;
	
}
