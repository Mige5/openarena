import java.util.ArrayList;
import java.util.Vector;

public class Spell {

	public static ArrayList<Spell> spells = new ArrayList<Spell>();
	
	public static String[] TYPES = new String[]{"Hy�kk�ys", "Parannus", "Suojaus", "H�m�ys", "Voimakkuus", "Pys�ytys", "Poista taikuus", "Ime kesto", "Ime mana", "Siunaus", "Kirous"};
	
	public static int TYPE_ATK = 0;
	public static int TYPE_HEAL = 1;
	public static int TYPE_PROTECT = 2;
	public static int TYPE_MYSTIFICATION = 3;
	public static int TYPE_STR = 4;
	public static int TYPE_STOP = 5;
	public static int TYPE_REMOVE_SPELL = 6;
	public static int TYPE_DRAIN_HEALTH = 7;
	public static int TYPE_DRAIN_MANA = 8;
	public static int TYPE_BLESSING = 9;
	public static int TYPE_CURSE = 10;
	
	String name;
	int type;
	int price;
	int manaCost;
	int duration;
	String strength;
	
	public Spell(String name, int type, int price, int manaCost, String strength){
		this.name = name;
		this.type = type;
		this.price = price;
		this.manaCost = manaCost;
		this.strength = strength;
		this.duration = -1;
		spells.add(this);
	}
	
	public Spell(String name, int type, int price, int manaCost, String strength, int duration){
		this.name = name;
		this.type = type;
		this.price = price;
		this.manaCost = manaCost;
		this.strength = strength;
		this.duration = duration;
		spells.add(this);
	}
	
	public boolean isFriendlySpell(){
		if(this.type == TYPE_HEAL || this.type == TYPE_PROTECT || this.type == TYPE_STR || this.type == TYPE_REMOVE_SPELL || this.type == TYPE_BLESSING){
			return true;
		}
		return false;
	}
	
	public static void initSpells(){
		new Spell("Haava", TYPE_ATK, 100, 1, "1d3");
		new Spell("Vesivasama", TYPE_ATK, 200, 2, "2d3");
		new Spell("Valovasama", TYPE_ATK, 300, 3, "3d3");
		new Spell("J��t�v� Polte", TYPE_ATK, 400, 4, "4d3");
		new Spell("Turjake", TYPE_ATK, 500, 5, "5d3");
		new Spell("Synkk� Tuomio", TYPE_ATK, 600, 6, "6d3");
		new Spell("Auttava K�si", TYPE_HEAL, 100, 1, "5");
		new Spell("Henkinen parannus", TYPE_HEAL, 200, 2, "10");
		new Spell("Mielenrauha", TYPE_HEAL, 300, 3, "15");
		new Spell("Parannusvakuutus", TYPE_HEAL, 400, 4, "20");
		new Spell("Tosi Terveys", TYPE_HEAL, 500, 5, "25");
		new Spell("Pappisparannus", TYPE_HEAL, 600, 6, "30");
		new Spell("Sateensuoja", TYPE_PROTECT, 100, 1, "1", 10);
		new Spell("Suojataika", TYPE_PROTECT, 200, 2, "2", 10);
		new Spell("T�ydellinen Suoja", TYPE_PROTECT, 300, 3, "3", 10);
		new Spell("Tosi Panssari", TYPE_PROTECT, 400, 4, "4", 10);
		new Spell("Lent�v� Sika", TYPE_MYSTIFICATION, 200, 2, "", 1);
		new Spell("Nikotuskohtaus", TYPE_MYSTIFICATION, 400, 4, "", 2);
		new Spell("Vitamiinipilleri", TYPE_STR, 100, 1, "5", 10);
		new Spell("Voiman Tunto", TYPE_STR, 200, 2, "10", 10);
		new Spell("Punttikuuri", TYPE_STR, 300, 3, "15", 10);
		new Spell("Tosi Lihas", TYPE_STR, 400, 4, "20", 10);
		new Spell("Jalkakramppi", TYPE_STOP, 100, 1, "", 2);
		new Spell("Terval�t�kk�", TYPE_STOP, 200, 2, "", 4);
		new Spell("Jalkakahle", TYPE_STOP, 300, 3, "", 6);
		new Spell("Loitsutuho", TYPE_REMOVE_SPELL, 200, 2, "");
		new Spell("Kevyt Vampyrismi", TYPE_DRAIN_HEALTH, 100, 1, "1d2");
		new Spell("Liev� Vampyrismi", TYPE_DRAIN_HEALTH, 200, 2, "2d2");
		new Spell("Ik�v� Vampyrismi", TYPE_DRAIN_HEALTH, 300, 3, "3d2");
		new Spell("Tyly Vampyrismi", TYPE_DRAIN_HEALTH, 400, 4, "4d2");
		new Spell("Karu Vampyrismi", TYPE_DRAIN_HEALTH, 500, 5, "5d2");
		new Spell("Tosi Vampyrismi", TYPE_DRAIN_HEALTH, 600, 6, "6d2");
		new Spell("Kevyt Manaimu", TYPE_DRAIN_MANA, 100, 1, "5");
		new Spell("Liev� Manaimu", TYPE_DRAIN_MANA, 200, 2, "10");
		new Spell("Ik�v� Manaimu", TYPE_DRAIN_MANA, 300, 3, "15");
		new Spell("Tyly Manaimu", TYPE_DRAIN_MANA, 400, 4, "20");
		new Spell("Karu Manaimu", TYPE_DRAIN_MANA, 500, 5, "25");
		new Spell("Tosi Manaimu", TYPE_DRAIN_MANA, 600, 6, "30");
		new Spell("Onnentoivotus", TYPE_BLESSING, 100, 1, "5", 10);
		new Spell("J�niksenk�p�l�", TYPE_BLESSING, 200, 2, "10", 10);
		new Spell("Neliapila", TYPE_BLESSING, 300, 3, "15", 10);
		new Spell("Tosi Siunaus", TYPE_BLESSING, 400, 4, "20", 10);
		new Spell("Ep�onnentoivotus", TYPE_CURSE, 100, 1, "5", 10);
		new Spell("Paha Silm�", TYPE_CURSE, 200, 2, "10", 10);
		new Spell("Paha Kurki", TYPE_CURSE, 300, 3, "15", 10);
		new Spell("Tosi Kirous", TYPE_CURSE, 400, 4, "20", 10);
	}
	
	public static Spell findSpell(String spellName){
		for(Spell spell : spells){
			if(spell.name.equals(spellName)){
				return spell;
			}
		}
		return null;
	}
	
}
