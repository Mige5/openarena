import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JPanel;

public class CupStandingsPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	//FONTS
	public static Font PANEL_FONT = new Font("comic sans ms", Font.BOLD, 11);
	public static Font BUTTON_FONT = new Font("comic sans ms", Font.BOLD, 11);
	public static Font WINNER_FONT = new Font("comic sans ms", Font.BOLD, 21);
	
	//UI
	public static String ROUND_1_TXT = "1. kierros";
	public static String ROUND_2_TXT = "2. kierros";
	public static String ROUND_3_TXT = "V�lier�t";
	public static String ROUND_4_TXT = "Finaali";
	public static String CHAMPION_TXT = "CUP-mestari";
	
	public static int GAME_WINDOW_CUP_VIEW = 13;
	
	public CupStandingsPanel(Jframe frame){
		this.mainFrame = frame;
		this.setLayout(null);
		this.add(button(GAME_WINDOW_CUP_VIEW, "close cup view", "OK", BUTTON_FONT, 48, 480, 689, 57));
	}
	
	public void paintComponent (Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
	    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    g2d.setColor(Color.WHITE);
	    g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
	    g2d.setColor(Color.BLACK);
	    g2d.setFont(PANEL_FONT);
	    int yOff = 48;
	    int yOff2 = 0;
	    int yOff3 = 0;
	    for(int i = 0; i < 8; i++){
	    	Battle battle = Game.cupBattles_round1.get(i);
	    	if(i%2 == 0 && i > 0){
	    		yOff2 += 16;
	    	}
	    	if(i%4 == 0 && i > 0){
	    		yOff3 += 8;
	    	}
	    	g2d.drawLine(160, 48+(yOff*i)+yOff2+yOff3, 167, 48+(yOff*i)+yOff2+yOff3);
	    	g2d.drawLine(160, 64+(yOff*i)+yOff2+yOff3, 167, 64+(yOff*i)+yOff2+yOff3);
	    	g2d.drawLine(168, 48+(yOff*i)+yOff2+yOff3, 168, 63+(yOff*i)+yOff2+yOff3);
	    	g2d.drawLine(169, 56+(yOff*i)+yOff2+yOff3, 175, 56+(yOff*i)+yOff2+yOff3);
	    	g2d.drawString(battle.topTeam.name, 48, 53+(yOff*i)+yOff2+yOff3);
	    	g2d.drawString(battle.bottomTeam.name, 48, 69+(yOff*i)+yOff2+yOff3);
	    }
	    yOff = 112;
	    yOff2 = 0;
	    for(int i = 0; i < 4; i++){
	    	Battle battle = null;
	    	if(i < Game.cupBattles_round2.size())
	    		battle = Game.cupBattles_round2.get(i);
	    	if(i%2 == 0 && i > 0){
	    		yOff2 += 8;
	    	}
	    	g2d.drawLine(288, 56+(yOff*i)+yOff2, 295, 56+(yOff*i)+yOff2);
	    	g2d.drawLine(288, 104+(yOff*i)+yOff2, 295, 104+(yOff*i)+yOff2);
	    	g2d.drawLine(296, 56+(yOff*i)+yOff2, 296, 103+(yOff*i)+yOff2);
	    	g2d.drawLine(297, 80+(yOff*i)+yOff2, 303, 80+(yOff*i)+yOff2);
	    	if(battle != null){
	    		g2d.drawString(battle.topTeam.name, 177, 53+(yOff*i)+yOff2+yOff3);
		    	g2d.drawString(battle.bottomTeam.name, 177, 101+(yOff*i)+yOff2+yOff3);
	    	}
	    }
	    yOff = 232;
	    for(int i = 0; i < 2; i++){
	    	Battle battle = null;
	    	if(i < Game.cupBattles_round3.size())
	    		battle = Game.cupBattles_round3.get(i);
	    	g2d.drawLine(416, 80+(yOff*i), 423, 80+(yOff*i));
	    	g2d.drawLine(416, 192+(yOff*i), 423, 192+(yOff*i));
	    	g2d.drawLine(424, 80+(yOff*i), 424, 191+(yOff*i));
	    	g2d.drawLine(425, 136+(yOff*i), 431, 136+(yOff*i));
	    	if(battle != null){
	    		g2d.drawString(battle.topTeam.name, 305, 69+(yOff*i)+yOff2+yOff3);
		    	g2d.drawString(battle.bottomTeam.name, 305, 181+(yOff*i)+yOff2+yOff3);
	    	}
	    }
	    Battle battle = null;
	    if(Game.cupBattles_round4.size() > 0)
	    	battle = Game.cupBattles_round4.get(0);
	    g2d.drawLine(544, 136, 551, 136);
    	g2d.drawLine(544, 360, 551, 360);
    	g2d.drawLine(552, 136, 552, 359);
    	g2d.drawLine(553, 248, 559, 248);
    	if(battle != null){
    		g2d.drawString(battle.topTeam.name, 433, 141);
	    	g2d.drawString(battle.bottomTeam.name, 433, 373);
	    	if(battle.winningTeam != null){
	    		g2d.setFont(WINNER_FONT);
	    		g2d.drawString(battle.winningTeam.name, 568, 254);
	    	}
    	}
    	g2d.setFont(PANEL_FONT);
	    g2d.drawString(ROUND_1_TXT, 51, 21);
	    g2d.drawString(ROUND_2_TXT, 176, 21);
	    g2d.drawString(ROUND_3_TXT, 304, 21);
	    g2d.drawString(ROUND_4_TXT, 431, 21);
	    g2d.drawString(CHAMPION_TXT, 568, 21);
	}
	
	public void refresh(){
		this.revalidate();
		this.repaint();
	}
	
	public ArrayList<Button> buttons = new ArrayList<Button>();
	
	public JButton button(int windowName, String s, Font f, int x, int y, int width, int height){
		return button(windowName, s, s, f, x, y, width, height);
	}
	
	public JButton button(int windowName, String buttonName, String s, Font f, int x, int y, int width, int height){
		JButton jButton = new JButton(s);
		jButton.setForeground(Color.BLACK);
		jButton.setFont(f);
		jButton.setBounds(x, y, width, height);
		jButton.setMargin(new Insets(0, 0, 0, 0));
		jButton.setVisible(true);
		jButton.setActionCommand(buttonName);
		jButton.addActionListener(this.mainFrame);
		for(Button button : buttons){
			if(button.button_name.equals(buttonName)){
				System.out.println("[ERROR]: Button with same name already exists! "+buttonName);
			}
		}
		buttons.add(new Button(new int[]{windowName}, buttonName, jButton));
		return jButton;
	}
	
	public JButton button(int[] windowName, String s, Font f, int x, int y, int width, int height){
		return button(windowName, s, s, f, x, y, width, height);
	}
	
	public JButton button(int[] windowName, String buttonName, String s, Font f, int x, int y, int width, int height){
		JButton jButton = new JButton(s);
		jButton.setForeground(Color.BLACK);
		jButton.setFont(f);
		jButton.setBounds(x, y, width, height);
		jButton.setMargin(new Insets(0, 0, 0, 0));
		jButton.setVisible(true);
		jButton.setActionCommand(buttonName);
		jButton.addActionListener(this.mainFrame);
		for(Button button : buttons){
			if(button.button_name.equals(buttonName)){
				System.out.println("[ERROR]: Button with same name already exists! "+buttonName);
			}
		}
		buttons.add(new Button(windowName, buttonName, jButton));
		return jButton;
	}
	
	public JButton findButton(String s){
		for(Button button : buttons){
			if(button.button_name.toLowerCase().equals(s.toLowerCase())){
				return button.jButton;
			}
		}
		return null;
	}
	
	Jframe mainFrame;
	
}
