import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

public class Game {

	public static int MONTH = 0;
	public static int YEAR = 900;
	
	public static GamePanel gamePanel;
	
	public static String[] mods = new String[]{"OpenArena", "Areena 5"};
	
	public static int MOD_OPEN_ARENA = 0;
	public static int MOD_AREENA_5 = 1;
	
	public static int currentMod = MOD_OPEN_ARENA;
	
	public static int MONTH_TYPE_DIVISION_BATTLE = 0;
	public static int MONTH_TYPE_CUP_BATTLE = 1;
	public static int MONTH_TYPE_WINTER_BREAK = 2;
	
	public static int[] monthType = new int[]{
			MONTH_TYPE_DIVISION_BATTLE, //Tammi
			MONTH_TYPE_DIVISION_BATTLE, //Helmi
			MONTH_TYPE_CUP_BATTLE, //Maalis
			MONTH_TYPE_DIVISION_BATTLE, //Huhti
			MONTH_TYPE_CUP_BATTLE, //Touko
			MONTH_TYPE_DIVISION_BATTLE, //Kes�
			MONTH_TYPE_CUP_BATTLE, //Hein�
			MONTH_TYPE_DIVISION_BATTLE, //Elo
			MONTH_TYPE_CUP_BATTLE, //Syys
			MONTH_TYPE_DIVISION_BATTLE, //Loka
			MONTH_TYPE_WINTER_BREAK, //Marras
			MONTH_TYPE_WINTER_BREAK, //Joulu
	};
	
	public static void startNewGame(ArrayList<String> playerTeams, boolean automaticStartTeams){
		Game.createDivisions();
		Game.createAITeams();
		Game.createOtherTeams(playerTeams);
		Game.createStartingGladiators(automaticStartTeams);
		Game.setTeamStartValues();
		createCupBattleGroups(1);
	}
	
	public static void createDivisions(){
		new Division(Constants.START_MONEY_DIV_1);
		new Division(Constants.START_MONEY_DIV_2);
		new Division(Constants.START_MONEY_DIV_3);
		new Division(Constants.START_MONEY_DIV_4);
	}
	
	public static void createAITeams(){
		Division.divisions.get(0).teams.add(new Team("Kaaosvoima"));
		Division.divisions.get(0).teams.add(new Team("Auringon soturit"));
		Division.divisions.get(0).teams.add(new Team("Kuun Sotilaat"));
		Division.divisions.get(0).teams.add(new Team("Pime�n Ruhtinaat"));
		
		Division.divisions.get(1).teams.add(new Team("Synk�t Valtiaat"));
		Division.divisions.get(1).teams.add(new Team("Sormusritarit"));
		Division.divisions.get(1).teams.add(new Team("Olorin Lordit"));
		Division.divisions.get(1).teams.add(new Team("Valon Vartijat"));
		
		Division.divisions.get(2).teams.add(new Team("Karhunkaatajat"));
		Division.divisions.get(2).teams.add(new Team("Verikoirat"));
		Division.divisions.get(2).teams.add(new Team("Harmaat Sudet"));
		Division.divisions.get(2).teams.add(new Team("Hainhampaat"));
	}
	
	public static void createOtherTeams(ArrayList<String> playerTeamNames){
		ArrayList<Team> playerTeams = new ArrayList<Team>();
		for(String teamName : playerTeamNames){
			Team team = new Team(teamName);
			Player player = new Player(team);
			team.controllingPlayer = player;
			playerTeams.add(team);
		}
		if(playerTeamNames.size() > 3){
			Division.divisions.get(3).teams.add(playerTeams.get(3));//P4
		} else {
			Division.divisions.get(3).teams.add(new Team("Tomunpurijat"));//P4
		}
		if(playerTeamNames.size() > 2){
			Division.divisions.get(3).teams.add(playerTeams.get(2));//P3
		} else {
			Division.divisions.get(3).teams.add(new Team("Varomattomat"));//P3
		}
		if(playerTeamNames.size() > 1){
			Division.divisions.get(3).teams.add(playerTeams.get(1));//P2
		} else {
			Division.divisions.get(3).teams.add(new Team("Malttamattomat"));//P2
		}
		Division.divisions.get(3).teams.add(playerTeams.get(0));//P1
	}
	
	public static void createStartingGladiators(boolean automaticStartTeams){
		for(int i = 0; i < Division.divisions.size(); i++){
			Division division = Division.divisions.get(i);
			for(Team team : division.teams){
				if(team.controllingPlayer == null){
					Game.addStartingGladiatorsToTeam(team);
				} else {
					if(automaticStartTeams){
						Game.addStartingGladiatorsToTeam(team);
					}
				}
				team.division = i;
				teams.add(team);
			}
		}
	}
	
	public static void addStartingGladiatorsToTeam(Team team){
		for(int i = 0; i < Util.random_(Constants.MAX_START_GLADIATORS)+1; i++){
			Gladiator gladiator = new Gladiator("Starter "+i);
			gladiator.owner = team;
			if(team.getGladiatorRaces().contains(gladiator.race))
				continue;
			team.gladiators.add(gladiator);
		}
	}
	
	public static void setTeamStartValues(){
		for(int i = 0; i < Division.divisions.size(); i++){
			Division division = Division.divisions.get(i);
			for(Team team : division.teams){
				team.money = division.startMoney;
				team.rating = Constants.START_RATING;
				team.arenaPoints = Constants.START_ARENA_POINTS;
			}
		}
	}
	
	public static Team getNextOpponentForTeam(Team team){
		if(Game.monthType[Game.MONTH] == Game.MONTH_TYPE_DIVISION_BATTLE){
			return getNextDivisionBattleOpponentForTeam(team);
		}
		else if(Game.monthType[Game.MONTH] == Game.MONTH_TYPE_CUP_BATTLE){
			return getOpponentForCUPBattleForTeam(team);
		}
		return null;
	}
		
	public static Team getNextDivisionBattleOpponentForTeam(Team team){
		return getDivisionBattleOpponentForTeam(team, MONTH);
	}
	
	public static Team getDivisionBattleOpponentForTeam(Team team, int month){
		Division division = findTeamsDivision(team);
	    int teamIndex = Util.getTeamIndex(division, team);
	    Team opponent = null;
		if(month == 0 || month == 5){
    		if(teamIndex == 0){
    			opponent = division.teams.get(3);
    		} else if(teamIndex == 1){
    			opponent = division.teams.get(2);
    		} else if(teamIndex == 2){
    			opponent = division.teams.get(1);
    		} else if(teamIndex == 3){
    			opponent = division.teams.get(0);
    		}
    	} else if(month == 1 || month == 7){
    		if(teamIndex == 0){
    			opponent = division.teams.get(2);
    		} else if(teamIndex == 1){
    			opponent = division.teams.get(3);
    		} else if(teamIndex == 2){
    			opponent = division.teams.get(0);
    		} else if(teamIndex == 3){
    			opponent = division.teams.get(1);
    		}
    	} else if(month == 3 || month == 9){
    		if(teamIndex == 0){
    			opponent = division.teams.get(1);
    		} else if(teamIndex == 1){
    			opponent = division.teams.get(0);
    		} else if(teamIndex == 2){
    			opponent = division.teams.get(3);
    		} else if(teamIndex == 3){
    			opponent = division.teams.get(2);
    		}
    	}
		return opponent;
	}
	
	public static Division findTeamsDivision(Team team){
		for(Division division : Division.divisions){
			for(Team team_ : division.teams){
				if(team == team_){
					return division;
				}
			}
		}
		return null;
	}
	
	public static int BOTTOM = 0;
	public static int TOP = 1;
	
	public static void sellItem(int slot){
		Item item = Game.currentGladiator.items[slot];
		if(item == null)
			return;
		//Game.currentTeam.money += item.getSellPrice();
		Game.currentTeam.modifyMoney(item.getSellPrice());
		Game.currentGladiator.items[slot] = null;
	}
	
	public static void sellItem(Gladiator gladiator, int slot){
		Item item = gladiator.items[slot];
		if(item == null)
			return;
		gladiator.owner.modifyMoney(item.getSellPrice());
		gladiator.items[slot] = null;
	}
	
	public static int getDivisionBattlePositionForTeam(Team team){
		Division division = findTeamsDivision(team);
	    int teamIndex = Util.getTeamIndex(division, team);
		if(MONTH == 0 || MONTH  == 5){
    		if(teamIndex == 0){
    			if(MONTH == 0)
    				return TOP;
    			else
    				return BOTTOM;
    		} else if(teamIndex == 1){
    			if(MONTH == 0)
    				return TOP;
    			else
    				return BOTTOM;
    		} else if(teamIndex == 2){
    			if(MONTH == 0)
    				return BOTTOM;
    			else
    				return TOP;
    		} else if(teamIndex == 3){
    			if(MONTH == 0)
    				return BOTTOM;
    			else
    				return TOP;
    		}
    	} else if(MONTH  == 1 || MONTH  == 7){
    		if(teamIndex == 0){
    			if(MONTH == 1)
    				return TOP;
    			else
    				return BOTTOM;
    		} else if(teamIndex == 1){
    			if(MONTH == 1)
    				return TOP;
    			else
    				return BOTTOM;
    		} else if(teamIndex == 2){
    			if(MONTH == 1)
    				return BOTTOM;
    			else
    				return TOP;
    		} else if(teamIndex == 3){
    			if(MONTH == 1)
    				return BOTTOM;
    			else
    				return TOP;
    		}
    	} else if(MONTH  == 3 || MONTH  == 9){
    		if(teamIndex == 0){
    			if(MONTH == 3)
    				return TOP;
    			else
    				return BOTTOM;
    		} else if(teamIndex == 1){
    			if(MONTH == 3)
    				return BOTTOM;
    			else
    				return TOP;
    		} else if(teamIndex == 2){
    			if(MONTH == 3)
    				return TOP;
    			else
    				return BOTTOM;
    		} else if(teamIndex == 3){
    			if(MONTH == 3)
    				return BOTTOM;
    			else
    				return TOP;
    		}
    	}
		return -1;
	}

	public static int currentRecruitRound = 1;
	public static int currentRecruitTeamIdx = 0;
	
	public static int getSalaryForGladiators(Team team){
		int totalSalary = 0;
		for(Gladiator gladiator : team.gladiators){
			if(team.battleTeamContainsGladiator(gladiator)){
				totalSalary += gladiator.salary;
			} else {
				totalSalary += gladiator.salary/2;
			}
		}
		return totalSalary;
	}
	
	public static void createAutoBattleTeam(){
		for(Gladiator gladiator : Game.currentTeam.gladiators){
			if(getBattleTeamSize() >= Constants.MAX_GLADIATORS_IN_BATTLE)
				return;
			if(gladiator.hurt > 0)
				continue;
			if(battleTeamHasGladiator(gladiator))
				continue;
			Game.currentTeam.battleGladiators[battleTeamGetNextEmptySlot()] = gladiator;
		}
	}
	
	public static int battleTeamGetNextEmptySlot(){
		int idx = -1;
		for(int i = 0; i < Game.currentTeam.battleGladiators.length; i++){
			Gladiator gladiator = Game.currentTeam.battleGladiators[i];
			if(gladiator == null)
				return i;
		}
		return idx;
	}
	
	public static int battleTeamGetGladiatorSlot(Gladiator gladiator){
		for(int i = 0; i < Game.currentTeam.battleGladiators.length; i++){
			Gladiator gladiator_ = Game.currentTeam.battleGladiators[i];
			if(gladiator_ == gladiator)
				return i;
		}
		return -1;
	}
	
	public static void putGladiatorToBattleSlot(Gladiator gladiator, int slot){
		Game.currentTeam.battleGladiators[slot] = gladiator;
	}
	
	public static int getBattleTeamSize(){
		int count = 0;
		for(int i = 0; i < Game.currentTeam.battleGladiators.length; i++){
			Gladiator gladiator = Game.currentTeam.battleGladiators[i];
			if(gladiator != null)
				count++;
		}
		return count;
	}
	
	public static boolean battleTeamHasGladiator(Gladiator gladiator){
		for(int i = 0; i < Game.currentTeam.battleGladiators.length; i++){
			Gladiator gladiator_ = Game.currentTeam.battleGladiators[i];
			if(gladiator_ == gladiator)
				return true;
		}
		return false;
	}

	public static void handleKO(Gladiator attacker, Gladiator defender){
		attacker.takeOuts_1++;
		attacker.takeOuts_2++;
		int i = Math.abs(defender.hp);
		defender.hurt = (i/5)+1;//TODO: figure out how this should really work
	}
	
	public static boolean autoRound = false;

	public static void meleeAtk(Gladiator attacker, Gladiator defender){
		Item weapon_atk = attacker.getMeleeWeapon();
		Item weapon_def = defender.getMeleeWeapon();
		int atkVal = attacker.getAtkSkillValueForWeapon(weapon_atk) + (weapon_atk != null ? weapon_atk.hb_val : 0);
		int blockVal = defender.getDefSkillValueForWeapon(weapon_def) + (weapon_def != null ? weapon_def.pb_val : 0);
		int hitChance = Util.hitChance(atkVal, blockVal);
		boolean hitSuccess = Util.successfulHit(hitChance);
		int hitDMG = 0;
		int dmgDone = 0;
		int armorValue = defender.getArmorValue();
		if(hitSuccess){
			dmgDone = Util.roll(attacker.getMeleeDmgValue());
			double dmg_d = (double) dmgDone;
			if(Util.criticalHit())
				dmg_d *= Constants.CRITICAL_HIT_DMG_MULTIPLIER;
			dmgDone = (int) dmg_d;
			if(Game.RACE_OF_MONTH == attacker.race){
				dmgDone *= 2;
			}
			hitDMG = dmgDone;
			dmgDone -= armorValue;
			if(dmgDone > 0){
				defender.hp -= dmgDone;
			}
			if(defender.hp > 0){
				defender.receivedDmg = dmgDone;
			}
		} else {
			defender.receivedDmg = 0;
		}
		String atkWeaponName = "Nyrkki";
		if(weapon_atk != null){
			atkWeaponName = weapon_atk.name;
		}
		String defWeaponName = "Nyrkill��n";
		if(weapon_def != null){
			defWeaponName = "aseellaan "+weapon_def.name;
		}
		Item armor = defender.getArmour();
		String armorName = "Vaatteet";
		if(armor != null){
			armorName = armor.name;
		}
		if(hitSuccess){
			String damagedLine = "haavoittunut.";//haavoittunut = oma	//"vakavasti haavoittunut."	//"liev�sti haavoittunut."
			if(defender.hp <= 0)
				damagedLine = "tajuton.";
			String actualDMGLine = armorName+" suojaa "+armorValue+" pisteen verran, joten "+defender.name+" k�rsii "+dmgDone+" pistett� vahinkoa ja on siten";
			if(dmgDone <= 0){
				actualDMGLine = armorName+" suojaa "+armorValue+" pisteen verran, joten vahinkoa ei p��se l�pi.";
				damagedLine = "";
			}
			GamePanel.setMessage(attacker.name+" hy�kk�� aseenaan "+atkWeaponName+" ("+atkVal+").",
				defender.name+" torjuu "+defWeaponName+" ("+blockVal+").",
				"=> Osumismahdollisuus on "+hitChance+"%.",
				attacker.name+" osuu tehden vahinkoa "+hitDMG+" pistett�.",
				actualDMGLine,
				damagedLine
				);
		} else {
			GamePanel.setMessage(attacker.name+" hy�kk�� aseenaan "+atkWeaponName+" ("+atkVal+").",
					defender.name+" torjuu "+defWeaponName+" ("+blockVal+").",
					"=> Osumismahdollisuus on "+hitChance+"%.",
					attacker.name+" ei osu."
					);
		}
		if(defender.hp <= 0)
			Game.handleKO(attacker, defender);
		if(!Game.currentBattle.isAIonlyBattle && !autoRound){
			Game.currentBattle.showBattleButtons = false;
			gamePanel.updateButtons();
		}
	}
	
	public static void rangedAtk(Gladiator attacker, Gladiator defender){
		Item weapon_atk = attacker.getRangedWeapon();
		int atkVal = attacker.getAtkSkillValueForWeapon(weapon_atk);
		int blockVal = defender.ability1;
		int hitChance = Util.hitChance(atkVal, blockVal);
		boolean hitSuccess = Util.successfulHit(hitChance);
		int hitDMG = 0;
		int dmgDone = 0;
		int armorValue = defender.getArmorValue();
		if(weapon_atk.subtype == Item.SUBTYPE_RANGED_WEAPON_CROSSBOW)
			attacker.needsReload = true;
		attacker.ammo -= 1;
		if(hitSuccess){
			dmgDone = Util.roll(attacker.getRangedDmgValue());
			double dmg_d = (double) dmgDone;
			if(Util.criticalHit())
				dmg_d *= Constants.CRITICAL_HIT_DMG_MULTIPLIER;
			dmgDone = (int) dmg_d;
			if(Game.RACE_OF_MONTH == attacker.race){
				dmgDone *= 2;
			}
			hitDMG = dmgDone;
			dmgDone -= armorValue;
			if(dmgDone > 0){
				defender.hp -= dmgDone;
			}
			if(defender.hp > 0){
				defender.receivedDmg = dmgDone;
			}
		} else {
			defender.receivedDmg = 0;
		}
		String atkWeaponName = null;
		if(weapon_atk != null){
			atkWeaponName = weapon_atk.name;
		}
		Item armor = defender.getArmour();
		String armorName = "Vaatteet";
		if(armor != null){
			armorName = armor.name;
		}
		if(hitSuccess){
			String damagedLine = "haavoittunut.";//haavoittunut = oma	//"vakavasti haavoittunut."	//"liev�sti haavoittunut."
			if(defender.hp <= 0)
				damagedLine = "tajuton.";
			String actualDMGLine = armorName+" suojaa "+armorValue+" pisteen verran, joten "+defender.name+" k�rsii "+dmgDone+" pistett� vahinkoa ja on siten";
			if(dmgDone <= 0){
				actualDMGLine = armorName+" suojaa "+armorValue+" pisteen verran, joten vahinkoa ei p��se l�pi.";
				damagedLine = "";
			}
			GamePanel.setMessage(attacker.name+" hy�kk�� aseenaan "+atkWeaponName+" ("+atkVal+").",
				defender.name+" v�ist�� ("+blockVal+").",
				"=> Osumismahdollisuus on "+hitChance+"%.",
				attacker.name+" osuu tehden vahinkoa "+hitDMG+" pistett�.",
				actualDMGLine,
				damagedLine
				);
		} else {
			GamePanel.setMessage(attacker.name+" hy�kk�� aseenaan "+atkWeaponName+" ("+atkVal+").",
					defender.name+" v�ist�� ("+blockVal+").",
					"=> Osumismahdollisuus on "+hitChance+"%.",
					attacker.name+" ei osu."
					);
		}
		if(defender.hp <= 0)
			handleKO(attacker, defender);
		Game.currentBattle.currentBattleGladiatorIdx++;
	}

	public static boolean successfullFriendlySpellCast(Gladiator caster, Spell spell){
		int atkVal = caster.getAtkSkillValueForSpell(spell);
		int blockVal = 0;
		int hitChance = Util.hitChance(atkVal, blockVal);
		boolean hitSuccess = Util.successfulHit(hitChance);
		return hitSuccess;
	}

	public static void spellCast(Gladiator caster, Gladiator target, Spell spell){
		int atkVal = caster.getAtkSkillValueForSpell(spell);
		int blockVal = 0;
		int hitChance = 0;
		boolean hitSuccess = true;
		if(!spell.isFriendlySpell()) {
			blockVal = target.ability2;
			hitChance = Util.hitChance(atkVal, blockVal);
			hitSuccess = Util.successfulHit(hitChance);
		}
		caster.mana -= spell.manaCost;
		if(spell.type == Spell.TYPE_ATK){
			int hitDMG = 0;
			int dmgDone = 0;
			if(hitSuccess){
				dmgDone = Util.roll(spell.strength);
				double dmg_d = (double) dmgDone;
				if(Util.criticalHit())
					dmg_d *= Constants.CRITICAL_HIT_DMG_MULTIPLIER;
				dmgDone = (int) dmg_d;
				if(Game.RACE_OF_MONTH == caster.race){
					dmgDone *= 2;
				}
				hitDMG = dmgDone;
				if(dmgDone > 0){
					target.hp -= dmgDone;
				}
				if(target.hp > 0){
					target.receivedDmg = dmgDone;
				}
			} else {
				target.receivedDmg = 0;
			}
			String spellName = spell.name;
			if(hitSuccess){
				String damagedLine = "haavoittunut.";//haavoittunut = oma	//"vakavasti haavoittunut."	//"liev�sti haavoittunut."
				if(target.hp <= 0)
					damagedLine = "tajuton.";
				String actualDMGLine = target.name+" k�rsii "+dmgDone+" pistett� vahinkoa ja on siten "+damagedLine;
				if(dmgDone <= 0){
					
				}
				GamePanel.setMessage(caster.name+" heitt�� loitsun "+spellName+" ("+atkVal+").",
					target.name+" vastustaa loitsua ("+blockVal+").",
					"=> Loitsun onnistumismahdollisuus on "+hitChance+"%.",
					"Loitsu onnistuu ja tekee "+hitDMG+" pistett� vahinkoa.",
					actualDMGLine
					);
			} else {
				GamePanel.setMessage(caster.name+" heitt�� loitsun "+spellName+" ("+atkVal+").",
						target.name+" vastustaa loitsua ("+blockVal+").",
						"=> Loitsun onnistumismahdollisuus on "+hitChance+"%.",
						"Loitsu ep�onnistuu."
						);
			}
			Game.currentBattle.currentBattleGladiatorIdx++;
		}
		if(target.hp <= 0)
			handleKO(caster, target);
	}
	
	public static ArrayList<Battle> battleList = new ArrayList<Battle>();
	public static ArrayList<Battle> cupBattles_round1 = new ArrayList<Battle>();
	public static ArrayList<Battle> cupBattles_round2 = new ArrayList<Battle>();
	public static ArrayList<Battle> cupBattles_round3 = new ArrayList<Battle>();
	public static ArrayList<Battle> cupBattles_round4 = new ArrayList<Battle>();
	
	public static void createBattleGroups(){
		battleList.clear();
		if(monthType[MONTH] != MONTH_TYPE_CUP_BATTLE){
			for(Team team : Game.teams){
				if(team.addedToBattle)
					continue;
				createBattleGroup(team, Game.getNextOpponentForTeam(team));
			}
		} else {
			if(cupBattles_round4.size() > 0){
				battleList.addAll(cupBattles_round4);
			}
			else if(cupBattles_round3.size() > 0){
				battleList.addAll(cupBattles_round3);
			}
			else if(cupBattles_round2.size() > 0){
				battleList.addAll(cupBattles_round2);
			}
			else if(cupBattles_round1.size() > 0){
				battleList.addAll(cupBattles_round1);
			}
		}
	}
	
	public static void createCupBattleGroups(int round){
		ArrayList<Team> cupTeams = new ArrayList<Team>();
		if(round == 1){
			cupTeams.addAll(Game.teams);
			Collections.shuffle(cupTeams);
		}
		else if(round == 2){
			for(Battle battle : cupBattles_round1){
				cupTeams.add(battle.winningTeam);
			}
		}
		else if(round == 3){
			for(Battle battle : cupBattles_round2){
				cupTeams.add(battle.winningTeam);
			}
		}
		else if(round == 4){
			for(Battle battle : cupBattles_round3){
				cupTeams.add(battle.winningTeam);
			}
		}
		ArrayList<Team> topTeams = new ArrayList<Team>();
		ArrayList<Team> bottomTeams = new ArrayList<Team>();
		boolean topTeam_ = true;
		for(Team team : cupTeams){
			if(topTeam_){
				topTeams.add(team);
			} else {
				bottomTeams.add(team);
			}
			topTeam_ = !topTeam_;
		}
		for(int i = 0; i < topTeams.size(); i++){
			Team topTeam = topTeams.get(i);
			Team bottomTeam = bottomTeams.get(i);
			if(round == 1){
				cupBattles_round1.add(new Battle(topTeam, bottomTeam));
			}
			else if(round == 2){
				cupBattles_round2.add(new Battle(topTeam, bottomTeam));
			}
			else if(round == 3){
				cupBattles_round3.add(new Battle(topTeam, bottomTeam));
			}
			else if(round == 4){
				cupBattles_round4.add(new Battle(topTeam, bottomTeam));
			}
		}
	}
	
	public static Team getOpponentForCUPBattleForTeam(Team team){
		return getOpponentForCUPBattleForTeam(team, MONTH);
	}
	
	public static Team getOpponentForCUPBattleForTeam(Team team, int month){
		ArrayList<Battle> cupBattles = new ArrayList<Battle>();
		if(month == 8){
			cupBattles = cupBattles_round4;
		}
		else if(month == 6){
			cupBattles = cupBattles_round3;
		}
		else if(month == 4){
			cupBattles = cupBattles_round2;
		}
		else if(month == 2){
			cupBattles = cupBattles_round1;
		}
		for(Battle battle : cupBattles){
			if(battle.topTeam == team){
				return battle.bottomTeam;
			}
			else if(battle.bottomTeam == team){
				return battle.topTeam;
			}
		}
		return null;
	}
	
	public static void createBattleGroup(Team team1, Team team2){
		Team bottomTeam = null;
		Team topTeam = null;
		int startPos = Game.getDivisionBattlePositionForTeam(team1);
		if(startPos == Game.BOTTOM){
			bottomTeam = team1;
			topTeam = team2;
		} else {
			bottomTeam = team2;
			topTeam = team1;
		}
		battleList.add(new Battle(topTeam, bottomTeam));
		topTeam.addedToBattle = true;
		bottomTeam.addedToBattle = true;
	}
	
	public static void setupMap(Battle battle){
		int bottom_offX = 2;
		int bottom_offY = 8;
		int top_offX = 2;
		int top_offY = 1;
		Team bottomTeam = battle.bottomTeam;
		Team topTeam = battle.topTeam;
		if(bottomTeam.controllingPlayer == null){
			Game.createBattleTeamForAI(bottomTeam);
		}
		if(topTeam.controllingPlayer == null){
			Game.createBattleTeamForAI(topTeam);
		}
		ArrayList<Gladiator> bottomTeamGladiators = new ArrayList<Gladiator>();
		ArrayList<Gladiator> topTeamGladiators = new ArrayList<Gladiator>();
		Game.currentBattle = battle;
		for(int i = 0; i < Constants.MAX_GLADIATORS_IN_BATTLE; i++){
			if(bottomTeam.battleGladiators[i] == null)
    			continue;
			int rows = 2;
    		int divider = Constants.MAX_GLADIATORS_IN_BATTLE / rows;
    		int x = i%divider;
    		int y = i/divider;
    		Game.currentBattle.map.mapTiles[x + bottom_offX][y + bottom_offY].gladiatorOnTile = bottomTeam.battleGladiators[i];
    		bottomTeam.battleGladiators[i].pos = new Position(x + bottom_offX, y + bottom_offY);
    		bottomTeamGladiators.add(bottomTeam.battleGladiators[i]);
    		bottomTeam.battleGladiators[i].matches++;
		}
		for(int i = 0; i < Constants.MAX_GLADIATORS_IN_BATTLE; i++){
			if(topTeam.battleGladiators[i] == null)
    			continue;
			int rows = 2;
    		int divider = Constants.MAX_GLADIATORS_IN_BATTLE / rows;
    		int x = i%divider;
    		int y = -(i/divider);
    		Game.currentBattle.map.mapTiles[x + top_offX][y + top_offY].gladiatorOnTile = topTeam.battleGladiators[i];
    		topTeam.battleGladiators[i].pos = new Position(x + top_offX, y + top_offY);
    		topTeamGladiators.add(topTeam.battleGladiators[i]);
    		topTeam.battleGladiators[i].matches++;
		}
		for(int i = 0; i < Constants.MAX_GLADIATORS_IN_BATTLE; i++){
			if(i < bottomTeamGladiators.size())
				Game.currentBattle.gladiatorBattleTurnOrder.add(bottomTeamGladiators.get(i));
			if(i < topTeamGladiators.size())
				Game.currentBattle.gladiatorBattleTurnOrder.add(topTeamGladiators.get(i));
		}
		if(Game.currentBattle.isAIonlyBattle){
			Game.currentBattle.startNewBattleTurn();
		} else {
			if(topTeam.controllingPlayer == null && bottomTeam.controllingPlayer != null){
				Game.currentTeam = bottomTeam;
				if(bottomTeamGladiators.size() > 0)
					Game.currentGladiator = bottomTeamGladiators.get(0);
			}
			else if(topTeam.controllingPlayer != null && bottomTeam.controllingPlayer == null){
				Game.currentTeam = topTeam;
				if(topTeamGladiators.size() > 0)
					Game.currentGladiator = topTeamGladiators.get(0);
			}
			int baseIncome = battle.getBaseIncome();//TODO: figure out how this should work
			//bottomTeam.money += baseIncome;
			//topTeam.money += baseIncome;
			bottomTeam.modifyMoney(baseIncome);
			topTeam.modifyMoney(baseIncome);
			bottomTeam.currentMonthIncome = baseIncome;
			topTeam.currentMonthIncome = baseIncome;
			gamePanel.updateButtons();
			gamePanel.refresh();
		}
	}
	
	public static void endBattle(Battle battle){
		boolean noWinner = false;
		ArrayList<Gladiator> aliveTopTeamGladiators = new ArrayList<Gladiator>();
		ArrayList<Gladiator> aliveBottomTeamGladiators = new ArrayList<Gladiator>();
		for(Gladiator gladiator : battle.topTeam.battleGladiators){
			if(gladiator == null)
				continue;
			if(gladiator.hp <= 0)
				continue;
			aliveTopTeamGladiators.add(gladiator);
		}
		for(Gladiator gladiator : battle.bottomTeam.battleGladiators){
			if(gladiator == null)
				continue;
			if(gladiator.hp <= 0)
				continue;
			aliveBottomTeamGladiators.add(gladiator);
		}
		if(aliveTopTeamGladiators.size() > 0 && aliveBottomTeamGladiators.size() > 0){
			noWinner = true;
		}
		Team winningTeam = null;
		Team losingTeam = null;
		if(noWinner){
			if(Util.random_(2) == 0){
				winningTeam = battle.topTeam;
				losingTeam = battle.bottomTeam;
			} else {
				winningTeam = battle.bottomTeam;
				losingTeam = battle.topTeam;
			}
		} else {
			if(aliveTopTeamGladiators.size() > 0){
				winningTeam = battle.topTeam;
				losingTeam = battle.bottomTeam;
			} else {
				winningTeam = battle.bottomTeam;
				losingTeam = battle.topTeam;
			}
		}
		if(Game.monthType[Game.MONTH] == Game.MONTH_TYPE_DIVISION_BATTLE)
			winningTeam.wins++;
		battle.winningTeam = winningTeam;
		battle.losingTeam = losingTeam;
		battle.winningTeam.battleResult[Game.MONTH] = Battle.WIN;
		battle.losingTeam.battleResult[Game.MONTH] = Battle.LOSE;
		//battle.winningTeam.money += battle.getWinBonus();
		battle.winningTeam.modifyMoney(battle.getWinBonus());
		if(battle.winningTeam.controllingPlayer != null || battle.losingTeam.controllingPlayer != null){
			gamePanel.showBattleResult();
		}
		Game.currentBattleIdx++;
		if(Game.currentBattleIdx < Game.battleList.size()){
			Game.setupMap(Game.battleList.get(Game.currentBattleIdx));
		} else {
			progressToNextMonth = true;
			if(Game.monthType[Game.MONTH] == Game.MONTH_TYPE_CUP_BATTLE){
				if(cupBattles_round3.size() > 0){
					createCupBattleGroups(4);
				}
				else if(cupBattles_round2.size() > 0){
					createCupBattleGroups(3);
				}
				else if(cupBattles_round1.size() > 0){
					createCupBattleGroups(2);
				}
			}
			gamePanel.showBattleResults();
		}
	}
	
	public static boolean progressToNextMonth = false;
	
	public static void progressToNextMonth(){
		progressToNextMonth = false;
		Game.currentBattle = null;
		boolean progressYear = false;
		if(Game.MONTH < Constants.MONTHS.length-1){
			Game.MONTH++;
		}else{
			Game.MONTH = 0;
			Game.YEAR++;
			progressYear = true;
		}
		Game.currentRecruitTeamIdx = 0;
		Game.currentRecruitRound = 1;
		if(progressYear){
			cupBattles_round1.clear();
			cupBattles_round2.clear();
			cupBattles_round3.clear();
			cupBattles_round4.clear();
			moveTeamsToNewDivisions();
			createCupBattleGroups(1);
			Game.teamRecruitTurnOrder.clear();
			Game.setupRecruitOrder();
			Game.nextRecruitTurn();
		} else {
			Game.nextRecruitTurn();
		}
		RACE_OF_MONTH = Race.races.get(Util.random_(Race.races.size()));
		Item.setNewDiscountedItems();
		Game.recruitableGladiators.clear();
		createRecruitableGladiators();
		for(Division division : Division.divisions){
			for(Team team : division.teams){
				team.addedToBattle = false;
				//team.money -= getSalaryForGladiators(team);
				team.modifyMoney(-getSalaryForGladiators(team));
				team.currentMonthMoneyBalance = 0;
				team.currentMonthIncome = 0;
				if(progressYear){
					team.wins = 0;
					team.battleResult = new int[12];
				}
				ArrayList<Gladiator> gladiatorsToRetire = new ArrayList<Gladiator>();
				for(Gladiator gladiator : team.gladiators){
					gladiator.hp = gladiator.maxHp;
					gladiator.mana = gladiator.maxMana;
					gladiator.needsReload = false;
					gladiator.ammo = Constants.AMMO_COUNT;
					gladiator.receivedDmg = -1;
					if(gladiator.hurt > 0){
						gladiator.hurt -= 1;
					}
					if(progressYear){
						gladiator.takeOuts_1 = 0;
						gladiator.age += 1;
						if(gladiator.age > Constants.GLADIATOR_MAX_AGE){
							gladiatorsToRetire.add(gladiator);
						}
					}
				}
				for(Gladiator gladiator : gladiatorsToRetire){//TODO: popup
					if(gladiator.items[Constants.MELEE_WEAPON_SLOT] != null){
						sellItem(gladiator, Constants.MELEE_WEAPON_SLOT);
					}
					if(gladiator.items[Constants.RANGED_WEAPON_SLOT] != null){
						sellItem(gladiator, Constants.RANGED_WEAPON_SLOT);
					}
					if(gladiator.items[Constants.ARMOUR_SLOT] != null){
						sellItem(gladiator, Constants.ARMOUR_SLOT);
					}
					team.gladiators.remove(gladiator);
				}
			}
		}
	}
	
	public static void moveTeamsToNewDivisions(){
		ArrayList<Team> removeFromDiv1 = new ArrayList<Team>();
		ArrayList<Team> removeFromDiv2 = new ArrayList<Team>();
		ArrayList<Team> removeFromDiv3 = new ArrayList<Team>();
		ArrayList<Team> removeFromDiv4 = new ArrayList<Team>();
		ArrayList<Team> addToDiv1 = new ArrayList<Team>();
		ArrayList<Team> addToDiv2 = new ArrayList<Team>();
		ArrayList<Team> addToDiv3 = new ArrayList<Team>();
		ArrayList<Team> addToDiv4 = new ArrayList<Team>();
		for(int i = 0; i < Division.divisions.size(); i++){
			Division division = Division.divisions.get(i);
			ArrayList<Team> teams = division.teams;
			Util.organizeTeams(teams);
			if(i == 0){
				removeFromDiv1.add(teams.get(division.teams.size()-1));
				addToDiv2.add(teams.get(division.teams.size()-1));
			}
			else if(i == 1){
				removeFromDiv2.add(teams.get(0));
				addToDiv1.add(teams.get(0));
				removeFromDiv2.add(teams.get(division.teams.size()-1));
				addToDiv3.add(teams.get(division.teams.size()-1));
			}
			else if(i == 2){
				removeFromDiv3.add(teams.get(0));
				addToDiv2.add(teams.get(0));
				removeFromDiv3.add(teams.get(division.teams.size()-1));
				addToDiv4.add(teams.get(division.teams.size()-1));
			}
			else if(i == 3){
				removeFromDiv4.add(teams.get(0));
				addToDiv3.add(teams.get(0));
			}
		}
		for(Team team : removeFromDiv1){
			Division.divisions.get(0).teams.remove(team);
		}
		for(Team team : addToDiv1){
			Division.divisions.get(0).teams.add(team);
		}
		for(Team team : removeFromDiv2){
			Division.divisions.get(1).teams.remove(team);
		}
		for(Team team : addToDiv2){
			Division.divisions.get(1).teams.add(team);
		}
		for(Team team : removeFromDiv3){
			Division.divisions.get(2).teams.remove(team);
		}
		for(Team team : addToDiv3){
			Division.divisions.get(2).teams.add(team);
		}
		for(Team team : removeFromDiv4){
			Division.divisions.get(3).teams.remove(team);
		}
		for(Team team : addToDiv4){
			Division.divisions.get(3).teams.add(team);
		}
		for(int i = 0; i < Division.divisions.size(); i++){
			Division division = Division.divisions.get(i);
			ArrayList<Team> teams = division.teams;
			for(Team team : teams){
				team.division = i;
			}
		}
	}
	
	public static void createBattleTeamForAI(Team team){
		team.battleGladiators = new Gladiator[Constants.MAX_GLADIATORS_IN_BATTLE];
		for(Gladiator gladiator : team.gladiators){
			if(team.getBattleTeamSize() >= Constants.MAX_GLADIATORS_IN_BATTLE)
				return;
			if(gladiator.hurt > 0)
				continue;
			team.battleGladiators[team.battleTeamGetNextEmptySlot()] = gladiator;
		}
	}
	
	public static void setupRecruitOrder(){//TODO: confirm this is how it should work
		for(int i = 0; i < Division.divisions.size(); i++){
			Division division = Division.divisions.get(i);
			for(Team team : division.teams){
				Game.teamRecruitTurnOrder.add(team);
			}
		}
	}
	
	public static void completeRecruits(){
		for(Gladiator gladiator : Game.recruitableGladiators){
			if(gladiator.offerTeam != null){
				gladiator.owner = gladiator.offerTeam;
				gladiator.offerTeam.gladiators.add(gladiator);
				//gladiator.offerTeam = null;
				gladiator.offerPrice = 0;
			}
		}
	}
	
	public static void nextRecruitTurn(){
		if(Game.currentRecruitTeamIdx >= Game.teamRecruitTurnOrder.size()){
			Game.currentRecruitTeamIdx = 0;
			Game.currentRecruitRound++;
			if(Game.currentRecruitRound > Constants.RECRUIT_TURNS){
				Game.completeRecruits();
			}
		}
		Team team = Game.teamRecruitTurnOrder.get(Game.currentRecruitTeamIdx);
		Game.currentRecruitTeamIdx++;
		if(team.controllingPlayer == null){
			handleAIrecruitTurn();
		} else {
			Game.currentTeam = team;
		}
	}
	
	public static void handleAIrecruitTurn(){//TODO:
		nextRecruitTurn();
	}
	
	public static void putRecruitOffer(Gladiator gladiator, int amount){
		if(Game.currentTeam.getGladiatorRaces().contains(gladiator.race))
			return;
		if(gladiator.offerTeam != null){
			//gladiator.offerTeam.money += gladiator.offerPrice;
			gladiator.offerTeam.modifyMoney(gladiator.offerPrice);
		}
		gladiator.offerPrice = amount;
		gladiator.offerTeam = Game.currentTeam;
		//Game.currentTeam.money -= amount;
		Game.currentTeam.modifyMoney(-amount);
		Game.nextRecruitTurn();
	}
	
	public static void buySpell(Spell spell){
		if(Game.currentGladiator.getKnownSpells().contains(spell)){
			return;
		}
		if(Game.currentTeam.money < spell.price){
			return;
		}
		Game.currentGladiator.knownSpells[Game.currentGladiator.getNextAvailableSpellSlot()] = spell;
		//Game.currentTeam.money -= spell.price;
		Game.currentTeam.modifyMoney(-spell.price);
	}
	
	public static void buyItem(Item item){
		int itemSlot = -1;
		if(item.type == Item.TYPE_MELEE_WEAPON){
			itemSlot = Constants.MELEE_WEAPON_SLOT;
		}
		else if(item.type == Item.TYPE_RANGED_WEAPON){
			itemSlot = Constants.RANGED_WEAPON_SLOT;
		}
		else if(item.type == Item.TYPE_ARMOUR){
			itemSlot = Constants.ARMOUR_SLOT;
		}
		Item ownedItem = null;
		int ownedItemSellPrice = 0;
		if(Game.currentGladiator.items[itemSlot] != null){
			ownedItem = Game.currentGladiator.items[itemSlot];
			ownedItemSellPrice = ownedItem.getSellPrice();
		}
		int buyPrice = item.getCurrentPrice()-ownedItemSellPrice;
		if(Game.currentTeam.money < buyPrice){
			return;
		}
		Game.currentGladiator.items[itemSlot] = item;
		//Game.currentTeam.money -= buyPrice;
		Game.currentTeam.modifyMoney(-buyPrice);
	}
	
	public static void trainSkill(String s){
		int cost = getTrainCost(s);
		if(s.equals(GamePanel.TRAIN_STR_TXT)){
			Game.currentGladiator.str += 1;
		}
		else if(s.equals(GamePanel.TRAIN_SPEED_TXT)){
			Game.currentGladiator.speed += 1;
		}
		else if(s.equals(GamePanel.TRAIN_HP_TXT)){
			Game.currentGladiator.hp += 1;
			Game.currentGladiator.maxHp += 1;
		}
		else if(s.equals(GamePanel.TRAIN_MANA_TXT)){
			Game.currentGladiator.mana += 1;
			Game.currentGladiator.maxMana += 1;
		}
		if(Game.currentTeam.money < cost){
			return;
		}
		//Game.currentTeam.money -= cost;
		Game.currentTeam.modifyMoney(-cost);
	}
	
	public static int getTrainCost(String s){
		return 100;//TODO: figure out how this should work
	}
	
	public static String getAbilityName(String s){
		if(s.equals(GamePanel.TRAIN_STR_TXT)){
			return "Voima";
		}
		else if(s.equals(GamePanel.TRAIN_SPEED_TXT)){
			return "Nopeus";
		}
		else if(s.equals(GamePanel.TRAIN_HP_TXT)){
			return "Kesto";
		}
		else if(s.equals(GamePanel.TRAIN_MANA_TXT)){
			return "Mana";
		}
		return null;
	}
	
	public static void startGame(){
		Spell.initSpells();
		Item.initItems();
		Game.setupRecruitOrder();
		Game.nextRecruitTurn();
		createRecruitableGladiators();
		gamePanel.addSpellsToTable();
		Item.setNewDiscountedItems();
		RACE_OF_MONTH = Race.races.get(Util.random_(Race.races.size()));
	}
	
	public static void createRecruitableGladiators(){
		
		for(int i = 0; i < Constants.RECRUIT_GLADIATOR_COUNT; i++){
			Gladiator gladiator = new Gladiator("Test "+i);
			Game.recruitableGladiators.add(gladiator);
		}
		
		/*
		for(int i = 0; i < Race.races.size(); i++){
			Gladiator gladiator = new Gladiator("Test "+i, i);
			Game.recruitableGladiators.add(gladiator);
		}
		*/
		gamePanel.addGladiatorsToRecruitTable();
	}
	
	public static int getCurrentRoundNumber(){
		if(MONTH == 0)
			return 1;
		if(MONTH == 1)
			return 2;
		if(MONTH == 3)
			return 3;
		if(MONTH == 5)
			return 4;
		if(MONTH == 7)
			return 5;
		if(MONTH == 9)
			return 6;
		return -1;
	}
	
	public static ArrayList<Team> teamRecruitTurnOrder = new ArrayList<Team>();
	public static ArrayList<Gladiator> recruitableGladiators = new ArrayList<Gladiator>();
	
	public static ArrayList<Team> teams = new ArrayList<Team>();
	
	public static void updateButtons(){
		gamePanel.updateButtons();
	}
	
	public static Gladiator currentGladiator;
	public static Team currentTeam;
	
	public static Team showGladiatorsForTeam;
	
	public static Battle currentBattle = null;
	
	public static Spell currentShootSpell = null;

	public static int currentBattleIdx = -1;
	
	public static int BATTLE_MAP = 0;
	public static Race RACE_OF_MONTH;
	
}
