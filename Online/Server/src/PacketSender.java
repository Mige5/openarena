

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map.Entry;

public class PacketSender {
	
	private DataOutputStream out;
	private Client client;

	public PacketSender(Client client) {
		this.client = client;
		this.out = this.client.out;
	}
	
	public void sendPlayerDataPacket() {
		int packetId = 0;
		try {
			out.writeByte(packetId);
			out.writeByte(client.clientId);
			out.writeUTF(client.teamName);
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendPlayersPacket() {
		int packetId = 1;
		try {
			out.writeByte(packetId);
			//System.out.println(Client.clients.size());
			/*
			out.writeByte(Client.clients.size());
			for(int i = 0; i < Client.clients.size(); i++){
				Client client = Client.clients.get(i);
				if(client != null){
					out.writeUTF(client.teamName);
				}
			}
			*/
			out.writeByte(Client.clients2.size());
			for (Entry<Integer, Client> mapEntry : Client.clients2.entrySet()) {
				int key = mapEntry.getKey();
				Client client_ = mapEntry.getValue();
				out.write(key);
				out.writeUTF(client_.teamName);
	        }
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendHideDialogPacket(){
		int packetId = 2;
		try {
			out.writeByte(packetId);
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}

	}
	
	
	public void sendGladiatorData(Gladiator gladiator){
		try {
			out.writeInt(gladiator.global_idx);
			out.writeUTF(gladiator.name);
			out.writeByte(gladiator.race.getIdx());
			out.writeByte(gladiator.str);
			out.writeByte(gladiator.speed);
			out.writeByte(gladiator.maxMana);
			out.writeByte(gladiator.maxHp);
			out.writeByte(gladiator.mana);
			out.writeByte(gladiator.hp);
			out.writeByte(gladiator.age);
			out.writeByte(gladiator.askPrice);
			out.writeByte(gladiator.salary);
			out.writeByte(gladiator.melee_weapon_atk_1);
			out.writeByte(gladiator.melee_weapon_atk_2);
			out.writeByte(gladiator.melee_weapon_atk_3);
			out.writeByte(gladiator.melee_weapon_atk_4);
			out.writeByte(gladiator.melee_weapon_atk_5);
			out.writeByte(gladiator.melee_weapon_def_1);
			out.writeByte(gladiator.melee_weapon_def_2);
			out.writeByte(gladiator.melee_weapon_def_3);
			out.writeByte(gladiator.melee_weapon_def_4);
			out.writeByte(gladiator.melee_weapon_def_5);
			out.writeByte(gladiator.ranged_weapon_atk_1);
			out.writeByte(gladiator.ranged_weapon_atk_2);
			out.writeByte(gladiator.ability1);
			out.writeByte(gladiator.ability2);
			for(int i = 0; i < gladiator.spellAtk.length; i++){
				out.writeByte(gladiator.spellAtk[i]);
			}
			out.writeByte(gladiator.speedBonus);
			out.writeByte(gladiator.strBonus);
			out.writeByte(gladiator.manaBonus);
			
			out.writeUTF(gladiator.owner == null ? "" : gladiator.owner.name);
			out.writeUTF(gladiator.offerTeam == null ? "" : gladiator.offerTeam.name);
			out.writeInt(gladiator.askPrice);
			out.writeInt(gladiator.offerPrice);
			
			out.writeInt(gladiator.takeOuts_1);
			out.writeInt(gladiator.takeOuts_2);
			out.writeByte(gladiator.hurt);
			out.writeInt(gladiator.matches);
			out.writeByte(gladiator.skillEffectCount);
			out.writeByte(gladiator.moral);
			out.writeByte(gladiator.condition);
			out.writeByte(gladiator.spells);
			out.writeBoolean(gladiator.specialGladiator);
			for(int i = 0; i < gladiator.items.length; i++){
				out.writeUTF(gladiator.items[i] == null ? "" : gladiator.items[i].name);
			}
			for(int i = 0; i < gladiator.knownSpells.length; i++){
				out.writeUTF(gladiator.knownSpells[i] == null ? "" : gladiator.knownSpells[i].name);
			}
			out.writeByte(gladiator.pos == null ? -1 : gladiator.pos.getX());
			out.writeByte(gladiator.pos == null ? -1 : gladiator.pos.getY());
			out.writeByte(gladiator.receivedDmg);
			out.writeByte(gladiator.ammo);
			out.writeBoolean(gladiator.needsReload);

		}catch(IOException e) {
			e.printStackTrace();
		}

	}
	
	public void sendRecruitableGladiatorsPacket() {
		int packetId = 3;
		try {
			out.writeByte(packetId);
			
			out.writeByte(Game.recruitableGladiators.size());
			for(int i = 0; i < Game.recruitableGladiators.size(); i++){
				Gladiator gladiator = Game.recruitableGladiators.get(i);
				sendGladiatorData(gladiator);
			}
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendChangeWindowPacket(int window) {
		sendChangeWindowPacket(window, 0);
	}
	
	public void sendChangeWindowPacket(int window, int subwindow) {
		int packetId = 4;
		try {
			out.writeByte(packetId);
			
			out.writeByte(window);
			out.writeByte(subwindow);
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendWindowMessagePacket() {
		int packetId = 5;
		try {
			out.writeByte(packetId);
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendCurrentMonthDataPacket() {
		int packetId = 6;
		try {
			out.writeByte(packetId);
			
			out.writeShort(Game.YEAR);
			out.writeByte(Game.MONTH);
			out.writeByte(Game.RACE_OF_MONTH.getIdx());
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendCurrentRecruitStatePacket() {
		int packetId = 7;
		try {
			out.writeByte(packetId);
			
			out.writeByte(Game.currentRecruitTeamIdx);
			out.writeByte(Game.currentRecruitRound);
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendTeamsPacket() {
		int packetId = 8;
		try {
			out.writeByte(packetId);
			out.writeByte(Division.divisions.size());
			for(int i = 0; i < Division.divisions.size(); i++){
				Division division = Division.divisions.get(i);
				out.writeByte(division.teams.size());
				for(Team team_ : division.teams){
					out.writeByte(team_.division);
					out.writeUTF(team_.name);
					out.writeBoolean(team_.controllingPlayer == null ? false : true);
				}
			}
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendTeamValuesPacket() {
		int packetId = 9;
		try {
			out.writeByte(packetId);
			
			out.writeInt(client.team.money);
			out.writeByte(client.team.arenaPoints);
			out.writeByte(client.team.rating);
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendTeamGladiatorsPacket() {
		int packetId = 10;
		try {
			out.writeByte(packetId);
			
			out.writeByte(client.team.gladiators.size());
			for(int i = 0; i < client.team.gladiators.size(); i++){
				Gladiator gladiator = client.team.gladiators.get(i);
				sendGladiatorData(gladiator);
			}
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendRefreshPacket() {
		int packetId = 11;
		try {
			out.writeByte(packetId);
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendCupBattlesPacket(int round) {
		int packetId = 12;
		ArrayList<Battle> battles = new ArrayList<Battle>();
		if(round == 1){
			battles = Game.cupBattles_round1;
		}
		else if(round == 2){
			battles = Game.cupBattles_round2;
		}
		else if(round == 3){
			battles = Game.cupBattles_round3;
		}
		else if(round == 4){
			battles = Game.cupBattles_round4;
		}
		try {
			out.writeByte(packetId);
			
			out.writeByte(round);
			out.writeByte(battles.size());
			for(int i = 0; i < battles.size(); i++){
				out.writeUTF(battles.get(i).topTeam.name);
				out.writeUTF(battles.get(i).bottomTeam.name);
			}
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendOtherTeamGladiatorsPacket(Team team) {
		int packetId = 13;
		try {
			out.writeByte(packetId);
			
			out.writeUTF(team.name);
			out.writeByte(team.gladiators.size());
			for(int i = 0; i < team.gladiators.size(); i++){
				Gladiator gladiator = team.gladiators.get(i);
				sendGladiatorData(gladiator);
			}
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendDiscountedItemsPacket() {
		int packetId = 14;
		try {
			out.writeByte(packetId);
			
			out.writeByte(Item.discountedItems.size());
			for(int i = 0; i < Item.discountedItems.size(); i++){
				Item item = Item.discountedItems.get(i);
				out.writeUTF(item.name);
			}
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendShowWaitDialogPacket() {
		int packetId = 15;
		try {
			out.writeByte(packetId);
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendCreateBattlePacket(Battle battle) {
		int packetId = 16;
		try {
			out.writeByte(packetId);
			out.writeUTF(battle.topTeam.name);
			out.writeUTF(battle.bottomTeam.name);
			for(int j = 0; j < 2; j++){
				Team team = battle.topTeam;
				if(j == 1)
					team = battle.bottomTeam;
				for(int i = 0; i < team.battleGladiators.length; i++){
					Gladiator gladiator = null;
					int idx = -1;
					if(team.battleGladiators[i] != null){
						gladiator = team.battleGladiators[i];
						idx = team.gladiators.indexOf(gladiator);
					}
					out.writeByte(idx);
					int x = -1;
					int y = -1;
					if(gladiator != null){
						x = gladiator.pos.getX();
						y = gladiator.pos.getY();
					}
					out.writeByte(x);
					out.writeByte(y);
				}
			}
			
			out.writeInt(battle.getBaseIncome());
			out.writeInt(battle.getWinBonus());
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendUpdateBattlePacket() {
		int packetId = 17;
		try {
			out.writeByte(packetId);
			Battle battle = client.currentBattle;
			out.write(battle.round);
			out.write(battle.currentBattleGladiatorIdx);
			out.writeInt(battle.getCurrentBattleGladiator().global_idx);
			for(int j = 0; j < 2; j++){
				Team team = battle.topTeam;
				if(j == 1)
					team = battle.bottomTeam;
				for(int i = 0; i < team.battleGladiators.length; i++){
					Gladiator gladiator = null;
					int idx = -1;
					if(team.battleGladiators[i] != null){
						gladiator = team.battleGladiators[i];
						idx = team.gladiators.indexOf(gladiator);
					}
					out.writeByte(idx);
					int x = -1;
					int y = -1;
					if(gladiator != null){
						x = gladiator.pos.getX();
						y = gladiator.pos.getY();
					}
					out.writeByte(x);
					out.writeByte(y);
				}
			}
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendEndBattlePacket() {
		int packetId = 18;
		try {
			out.writeByte(packetId);
			
			Battle battle = client.currentBattle;
			out.writeUTF(battle.winningTeam.name);
			out.writeUTF(battle.losingTeam.name);
			out.writeInt(100);
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendMessagePacket(String[] messages) {
		int packetId = 19;
		try {
			out.writeByte(packetId);
			
			int count = messages.length;
			out.writeByte(count);
			for(int i = 0; i < messages.length; i++){
				out.writeUTF(messages[i]);
			}
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendAllBattleResultsPacket() {
		int packetId = 20;
		try {
			out.writeByte(packetId);
			
			out.writeByte(Game.battleList.size());
			for(int i = 0; i < Game.battleList.size(); i++){
				Battle battles = Game.battleList.get(i);
				out.writeUTF(battles.topTeam.name);
				out.writeUTF(battles.bottomTeam.name);
				out.writeUTF(battles.winningTeam.name);
				out.writeUTF(battles.losingTeam.name);
			}
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
}
