
public class Constants {

	public static String GAME_NAME = "Avoin Areena";
	public static String GAME_VERSION = "v0.1";
	public static String[] GAME_DIFFICULTY = new String[]{"Helppo", "Normaali", "Vaikea", "Mahdoton"};
	public static String[] MONTHS = new String[]{"Tammikuu", "Helmikuu", "Maaliskuu", "Huhtikuu", "Toukokuu", "Kes�kuu", "Hein�kuu", "Elokuu", "Syyskuu", "Lokakuu", "Marraskuu", "Joulukuu"};
	
	//OTHER
	public static String MONEY_PREFIX = "Rahat";
	public static String MONEY_SUFFIX = "kr";
	public static int START_MONEY_DIV_1 = 10000;
	public static int START_MONEY_DIV_2 = 5000;
	public static int START_MONEY_DIV_3 = 2000;
	public static int START_MONEY_DIV_4 = 1500;
	public static int START_RATING = 70;
	public static int START_ARENA_POINTS = 100;
	public static int DIVISIONS = 4;//NOT USED!
	public static int TEAMS_IN_DIVISION = 4;//NOT USED!; USE EVEN NUMBER
	public static int RECRUIT_TURNS = 3;
	public static int RECRUIT_GLADIATOR_COUNT = 8;
	public static double DISCOUNT_PRICE = 0.9;
	public static int CRITICAL_HIT_CHANCE_PERCENTAGE = 10;
	public static double CRITICAL_HIT_DMG_MULTIPLIER = 2.0;
	public static int GLADIATOR_MIN_AGE = 20;
	public static int GLADIATOR_MAX_AGE = 35;
	public static int SKILL_MAX_VALUE = 90;
	public static int MELEE_WEAPON_SLOT = 0;
	public static int RANGED_WEAPON_SLOT = 1;
	public static int ARMOUR_SLOT = 2;
	public static int MAX_GLADIATORS_IN_TEAM = 9;
	public static int MAX_GLADIATORS_IN_BATTLE = 6;
	public static int MAX_BATTLE_ROUNDS = 40;
	
	public static int MAX_START_GLADIATORS = 4;
	
	public static int MAX_SPELLS = 4;//NOT REALLY USED CURRENTLY
	
	public static int AMMO_COUNT = 20;
	
	public static int HITMARK_HIT = 0;
	public static int HITMARK_MISS = 1;
	public static int HITMARK_KO = 2;
	public static int HITMARK_SPELL_1 = 3;
	public static int HITMARK_SPELL_2 = 4;
	
	public static int SHOOT_MODE_RANGED_WEAPON = 1;
	public static int SHOOT_MODE_SPELL = 2;
	public static int MAP_WIDTH = 7;
	public static int MAP_HEIGHT = 10;
	public static int MAP_TILE_SIZE = 34;
	public static boolean DRAW_BATTLE_GRID = false;
	public static String YEAR_SUFFIX = "AD";
	
	public static double SELL_PRICE = 0.75;
	
}
