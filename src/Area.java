
public class Area {
    
    private int bottomX, bottomY, topX, topY;
    
    public Area(int x1, int y1, int x2, int y2) {
    	this.bottomX = x1 <= x2 ? x1 : x2;
        this.bottomY = y1 <= y2 ? y1 : y2;
        this.topX = x1 >= x2 ? x1 : x2;
        this.topY = y1 >= y2 ? y1 : y2;
    }
    
    public int getMinX(){
    	return bottomX;
    }
    
    public int getMinY(){
    	return bottomY;
    }
    
    public int getMaxX(){
    	return topX;
    }
    
    public int getMaxY(){
    	return topY;
    }
    
    /*public Position getRandomPosInArea(){
		int minX = this.getMinX();
		int minY = this.getMinY();
		int maxX = this.getMaxX();
		int maxY = this.getMaxY();
		int diffX = maxX-minX;
		int diffY = maxY-minY;
		int x = minX + Misc.random_(diffX);
		int y = minY + Misc.random_(diffY);
		return new Position(x, y);
	}*/
    
    public Position[] calculateAllPositions() {
        Position[] positions = new Position[((topX-bottomX)+1)*((topY-bottomY)+1)];
        int current = 0;
        for (int x = bottomX; x <= topX; x++) {
            for (int y = bottomY; y <= topY; y++) {
                positions[current++] = new Position(x, y);
            }
        }
        return positions;
    }
    
    
    public boolean contains(Position pos) {
        int x = pos.getX(); 
        int y = pos.getY();
        return x > bottomX && x < topX && y > bottomY && y < topY;
    }
    
    public boolean containsBorderIncluded(Position pos) {
        int x = pos.getX(); 
        int y = pos.getY();
        return x >= bottomX && x <= topX && y >= bottomY && y <= topY;
    }
    
}
