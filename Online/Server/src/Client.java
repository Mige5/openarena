

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class Client implements Runnable{
	
	public static ArrayList<Client> clients = new ArrayList<>();
	public static HashMap<Integer, Client> clients2 = new HashMap<Integer, Client>();
	
	private Socket socket;
	public DataInputStream in;
	public DataOutputStream out;
	public PacketReader packetReader;
	public PacketSender packetSender;
	
	public String teamName;
	public int clientId;
	
	public Team team;
	
	public Battle currentBattle;
	
	public static int clientID = 0;
	
	public Client(Socket socket) {
		this.socket = socket;
		try {
			out = new DataOutputStream(socket.getOutputStream());
			in = new DataInputStream(socket.getInputStream());
			packetReader = new PacketReader(this);
			packetSender = new PacketSender(this);
			teamName = in.readUTF();
			clientId = clientID;
			Player player = new Player(clientId, teamName);
			this.getPacketSender().sendPlayerDataPacket();
			sendPlayersToAllClients();
			clientID++;
		}catch(IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		while(socket.isConnected()){
			try {
				int packetId = in.readByte();
				packetReader.readPacket(packetId);
			} catch (IOException e){
				closeClient(socket, in, out);
				break;
			}
		}
	}
	
	public void closeClient(Socket socket, DataInputStream in, DataOutputStream out){
		removeClient();
		try{
			if(in != null){
				in.close();
			}
			if(out != null){
				out.close();
			}
			if(socket != null){
				socket.close();
			}
		}catch (IOException e){
			e.printStackTrace();
		}
		sendPlayersToAllClients();
	}
	
	public static void sendPlayersToAllClients(){
		/*
		for(Client client : clients){
			if(client != null)
				client.getPacketSender().sendPlayersPacket();
		}
		*/
		for (Entry<Integer, Client> mapEntry : clients2.entrySet()) {
			int key = mapEntry.getKey();
			Client client_ = mapEntry.getValue();
			client_.getPacketSender().sendPlayersPacket();
        }
	}
	
	public static void sendRecruitGladiatorsAllClients(){
		for (Entry<Integer, Client> mapEntry : clients2.entrySet()) {
			int key = mapEntry.getKey();
			Client client_ = mapEntry.getValue();
			client_.getPacketSender().sendRecruitableGladiatorsPacket();
        }
	}
	
	public static void sendRecruitWindowAllClients(){
		for (Entry<Integer, Client> mapEntry : clients2.entrySet()) {
			int key = mapEntry.getKey();
			Client client_ = mapEntry.getValue();
			client_.getPacketSender().sendChangeWindowPacket(Game.GAME_WINDOW_RECRUIT);
        }
	}
	
	public static void sendWindowMessageAllClients(){
		for (Entry<Integer, Client> mapEntry : clients2.entrySet()) {
			int key = mapEntry.getKey();
			Client client_ = mapEntry.getValue();
			client_.getPacketSender().sendWindowMessagePacket();
        }
	}
	
	public static void sendMonthDataAllClients(){
		for (Entry<Integer, Client> mapEntry : clients2.entrySet()) {
			int key = mapEntry.getKey();
			Client client_ = mapEntry.getValue();
			client_.getPacketSender().sendCurrentMonthDataPacket();
        }
	}
	
	public static void sendRecruitStateAllClients(){
		for (Entry<Integer, Client> mapEntry : clients2.entrySet()) {
			int key = mapEntry.getKey();
			Client client_ = mapEntry.getValue();
			client_.getPacketSender().sendCurrentRecruitStatePacket();
        }
	}
	
	public static void sendHideDialogAllClients(){
		for (Entry<Integer, Client> mapEntry : clients2.entrySet()) {
			int key = mapEntry.getKey();
			Client client_ = mapEntry.getValue();
			client_.getPacketSender().sendHideDialogPacket();
        }
	}
	
	public static void sendTeamsAllClients(){
		for (Entry<Integer, Client> mapEntry : clients2.entrySet()) {
			int key = mapEntry.getKey();
			Client client_ = mapEntry.getValue();
			client_.getPacketSender().sendTeamsPacket();
        }
	}
	
	public static void sendTeamValuesAllClients(){
		for (Entry<Integer, Client> mapEntry : clients2.entrySet()) {
			int key = mapEntry.getKey();
			Client client_ = mapEntry.getValue();
			client_.getPacketSender().sendTeamValuesPacket();
        }
	}
	
	public static void sendTeamGladiatorsAllClients(){
		for (Entry<Integer, Client> mapEntry : clients2.entrySet()) {
			int key = mapEntry.getKey();
			Client client_ = mapEntry.getValue();
			client_.getPacketSender().sendTeamGladiatorsPacket();
        }
	}
	
	public static void sendRefreshAllClients(){
		for (Entry<Integer, Client> mapEntry : clients2.entrySet()) {
			int key = mapEntry.getKey();
			Client client_ = mapEntry.getValue();
			client_.getPacketSender().sendRefreshPacket();
        }
	}
	
	public static void sendCupBattlesAllClients(int round){
		for (Entry<Integer, Client> mapEntry : clients2.entrySet()) {
			int key = mapEntry.getKey();
			Client client_ = mapEntry.getValue();
			client_.getPacketSender().sendCupBattlesPacket(round);
        }
	}
	
	public static void sendOtherTeamGladiatorsAllClients(){
		for (Entry<Integer, Client> mapEntry : clients2.entrySet()) {
			int key = mapEntry.getKey();
			Client client_ = mapEntry.getValue();
			for(Team team : Game.teams){
				if(team == client_.team)
					continue;
				client_.getPacketSender().sendOtherTeamGladiatorsPacket(team);
			}
        }
	}
	
	public static void sendDiscountedItemsAllClients(){
		for (Entry<Integer, Client> mapEntry : clients2.entrySet()) {
			int key = mapEntry.getKey();
			Client client_ = mapEntry.getValue();
			client_.getPacketSender().sendDiscountedItemsPacket();
        }
	}
	
	public static Client findClientForTeam(String teamName_){
		for (Entry<Integer, Client> mapEntry : clients2.entrySet()) {
			int key = mapEntry.getKey();
			Client client_ = mapEntry.getValue();
			if(client_.teamName.equals(teamName_)){
				return client_;
			}
        }
		return null;
	}
	
	public void removeClient(){
		clients.remove(this);
		clients2.remove(this);
	}
	
	public PacketSender getPacketSender() {
		return packetSender;
	}

}
