import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

public class GamePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	//GAME WINDOWS
	public static int GAME_WINDOW_TITLESCREEN = 0;
	public static int GAME_WINDOW_MAIN_MENU = 1;
	public static int GAME_WINDOW_BATTLE_SETUP = 2;
	public static int GAME_WINDOW_BATTLE = 3;
	public static int GAME_WINDOW_RECRUIT = 4;
	public static int GAME_WINDOW_SCHOOL = 5;
	public static int GAME_WINDOW_MAGE = 6;
	public static int GAME_WINDOW_SHOP = 7;
	public static int GAME_WINDOW_OTHER_TEAMS = 8;
	public static int GAME_WINDOW_CHANGE_PLAYER = 9;
	
	public static int SUB_GAME_WINDOW_SHOP_MELEE = 1;
	public static int SUB_GAME_WINDOW_SHOP_RANGED = 2;
	public static int SUB_GAME_WINDOW_SHOP_ARMOUR = 3;
	public static int SUB_GAME_WINDOW_SHOP_DISCOUNT = 4;
	
	//COLORS
	public static Color MAIN_BACKGROUND_COLOR = new Color(192, 192, 192);
	public static Color TOP1_BACKGROUND_COLOR = new Color(255, 128, 128);
	public static Color TOP2_BACKGROUND_COLOR = new Color(255, 192, 192);
	public static Color BUTTON_AREA_BACKGROUND_COLOR = new Color(0, 0, 192);
	public static Color BOTTOM_TXT_BACKGROUND_COLOR = new Color(128, 128, 255);
	
	public static Color GLADIATOR_TITLE_COLOR = new Color(0, 0, 128);
	public static Color SPECIAL_GLADIATOR_TITLE_COLOR = new Color(128, 0, 0);
	
	public static Color GAME_NAME_OUTLINE_COLOR = new Color(113, 16, 16);
	
	public static Color SELECTED_GLADIATOR_COLOR = new Color(100, 100, 100);
	
	public static Color TABLE_BORDER_COLOR = new Color(100, 100, 100);
	
	public static Color BATTLE_SETUP_BACKGROUND_COLOR = new Color(0, 0, 128);
	
	public static Color BATTLE_BACKGROUND_COLOR = new Color(128, 0, 128);
	
	public static Color CLIPPED_TILE_COLOR = new Color(237, 28, 36);
	
	public static Color RECRUIT_BACKGROUND_COLOR = new Color(224, 224, 224);
	
	public static Color SCHOOL_BACKGROUND_COLOR = new Color(0, 128, 128);
	
	public static Color CHANGE_PLAYER_BACKGROUND_COLOR = new Color(0, 0, 255);
	
	public static Color RACE_OF_MONTH_COLOR = new Color(255, 0, 0);
	
	//FONTS
	public static Font MENU_FONT = new Font("Segoe UI", Font.PLAIN, 10);
	
	public static Font BIG_FONT = new Font("comic sans ms", Font.BOLD, 19);
	public static Font MAIN_FONT = new Font("comic sans ms", Font.BOLD, 12);
	
	public static Font BOTTOM_TXT_FONT = new Font("comic sans ms", Font.BOLD, 13);
	public static Font MSG_FONT = new Font("comic sans ms", Font.PLAIN, 11);
	
	public static Font GLADIATOR_NAME_FONT = new Font("comic sans ms", Font.BOLD, 16);
	
	public static Font GLADIATOR_STAT_TITLE_FONT = new Font("comic sans ms", Font.BOLD, 12);
	
	public static Font MAIN_BUTTON_FONT = new Font("comic sans ms", Font.BOLD, 13);
	public static Font SCHOOL_BUTTON_FONT = new Font("comic sans ms", Font.BOLD, 14);
	public static Font RECRUIT_BUTTON_FONT = new Font("comic sans ms", Font.PLAIN, 13);
	public static Font EQUIPMENT_BUTTON_FONT = new Font("comic sans ms", Font.BOLD, 12);
	
	public static Font GAME_NAME_FONT = new Font("comic sans ms", Font.BOLD, 55);
	public static Font GAME_VERSION_FONT = new Font("comic sans ms", Font.PLAIN, 21);
	public static Font TITLESCREEN_BUTTON_FONT = new Font("comic sans ms", Font.BOLD, 16);
	
	public static Font RECRUIT_TITLE_FONT = new Font("comic sans ms", Font.PLAIN, 21);
	public static Font RECRUIT_TABLE_TITLE_FONT = new Font("courier new", Font.PLAIN, 11);
	public static Font HURT_FONT = new Font("arial", Font.PLAIN, 9);
	
	//PLAYER
	public static String RATING_TXT = "Johtoporras on toiveikas";
	public static String ARENA_POINTS_TXT = "Areenapisteet";
	public int GAME_WINDOW_CURRENT = GAME_WINDOW_TITLESCREEN;
	public int SUB_GAME_WINDOW_CURRENT = 0;
	
	//UI
	public static String ROUND_TXT = "Kierros";
	
	public static String NEW_GAME_TXT = "Uusi peli";
	public static String LOAD_GAME_TXT = "Avaa peli";
	public static String ONLINE_GAME_TXT = "Verkkopeli";
	public static String EXIT_GAME_TXT = "Lopeta";
	
	public static String RECRUIT_TITLE_TXT = "V�RV�TT�V�T GLADIAATTORIT";
	public static String RECRUIT_TABLE_GLADIATOR_NAME_TITLE_TXT = "NIMI";
	public static String RECRUIT_TABLE_GLADIATOR_RACE_TITLE_TXT = "ROTU";
	public static String RECRUIT_TABLE_GLADIATOR_ASK_TITLE_TXT = "VAATIMUS";
	public static String RECRUIT_TABLE_GLADIATOR_OFFER_TITLE_TXT = "TARJOUS";
	public static String RECRUIT_TABLE_GLADIATOR_TEAM_TITLE_TXT = "JOUKKUE";
	
	public static String MAGE_TITLE_TXT = "MAAGI";
	public static String MAGE_TABLE_SPELL_NAME_TITLE_TXT = "LOITSUN NIMI";
	public static String MAGE_TABLE_SPELL_TYPE_TITLE_TXT = "TYYPPI";
	public static String MAGE_TABLE_SPELL_PRICE_TITLE_TXT = "HINTA";
	
	public static String OTHER_TEAMS_TITLE_TXT = "MUUT JOUKKUEET";
	public static String OTHER_TEAMS_TABLE_TEAM_NAME_TITLE_TXT = "JOUKKUE";
	public static String OTHER_TEAMS_TABLE_TEAM_DIVISION_TITLE_TXT = "DIVARI";
	public static String OTHER_TEAMS_TABLE_TEAM_TOUGHNESS_TITLE_TXT = "KOVUUS";
	
	public static String SHOP_TITLE_TXT = "KAUPPA";
	public static String MELEE_TABLE_ITEM_NAME_TITLE_TXT = "NIMI";
	public static String MELEE_TABLE_DMG_TITLE_TXT = "VAHINKO";
	public static String MELEE_TABLE_HB_TITLE_TXT = "HB";
	public static String MELEE_TABLE_PB_TITLE_TXT = "PB";
	public static String MELEE_TABLE_PRICE_TITLE_TXT = "HINTA";
	
	public static String RANGED_TABLE_ITEM_NAME_TITLE_TXT = "NIMI";
	public static String RANGED_TABLE_DMG_TITLE_TXT = "VAHINKO";
	public static String RANGED_TABLE_PRICE_TITLE_TXT = "HINTA";
	
	public static String ARMOUR_TABLE_ITEM_NAME_TITLE_TXT = "NIMI";
	public static String ARMOUR_TABLE_PP_TITLE_TXT = "PP";
	public static String ARMOUR_TABLE_PRICE_TITLE_TXT = "HINTA";
	
	public static String DISCOUNT_TABLE_ITEM_NAME_TITLE_TXT = "NIMI";
	public static String DISCOUNT_TABLE_DISCOUNT_PRICE_TITLE_TXT = "TARJOUS";
	public static String DISCOUNT_TABLE_NORMAL_PRICE_TITLE_TXT = "NORMAALIHINTA";
	
	public static String VS_TXT = "vs.";
	
	public static String OPPONENT_TXT = "Vastustajasi on";
	public static String RESTING_GLADIATORS_SALARY_TXT = "Taistelusta sivuun j��ville gladiaattoreille maksetaan vain puolet palkasta.";
	public static String BATTLE_COST_TXT = "Kuukauden palkkamenosi t�ll� taistelumuodostelmalla : ";
	
	public static String RECRUIT_TABLE = "Recruit table";
	public static String MAGE_TABLE = "Mage table";
	public static String MELEE_TABLE = "Melee table";
	public static String RANGED_TABLE = "Ranged table";
	public static String ARMOUR_TABLE = "Armour table";
	public static String DISCOUNT_TABLE = "Discount table";
	public static String OTHER_TEAMS_TABLE = "Other teams table";
	
	public static String BATTLE_FORMATION_TXT = "Tee t�h�n joukkueesi taistelumuodostelma seuraavaan sarjaotteluun.";
	public static String RACE_OF_MONTH_TXT = "Kuukauden rotu";
	public static String SKILL_EFFECTS_TXT = "Kykyihin vaikuttavat tekij�t:";
	public static String MELEE_SKILLS_TITLE_TXT = "Aseet";
	public static String MELEE_SKILLS_ATK_TXT = "Osum.";
	public static String MELEE_SKILLS_DEF_TXT = "Torj.";
	public static String RANGED_SKILLS_TITLE_TXT = "Heittoaseet";
	public static String RANGED_SKILLS_ATK_TXT = "Osum.";
	public static String OTHER_SKILLS_TITLE_TXT = "Muut kyvyt";
	public static String OTHER_SKILLS_VALUE_TXT = "Taito";
	public static String EQUIPMENT_TITLE_TXT = "Varusteet";
	public static String SPELLS_TITLE_TXT = "Osatut loitsut (loitsun heittotaito)";
	public static String RACE_TXT = "Rotu";
	public static String SALARY_TXT = "Palkka";
	public static String TAKEOUTS_TXT = "Kaadot";
	public static String HP_TXT = "Kesto";
	public static String MANA_TXT = "Mana";
	public static String AGE_TXT = "Ik�";
	public static String HURT_TXT = "Loukkaantunut";
	public static String MATCHES_TXT = "Otteluja";
	public static String STR_TXT = "Voima";
	public static String SPEED_TXT = "Nopeus";
	public static String MORAL_TXT_1 = "Moraali";
	public static String CONDITION_TXT_1 = "Kunto";
	public static String SPELLS_TXT = "Loitsut";
	public static String MORAL_TXT_2 = "Normaali";
	public static String CONDITION_TXT_2 = "Ei vammoja";
	
	public static String MELEE_WEAPON_TXT_1 = "Nyrkki";
	public static String MELEE_WEAPON_TXT_2 = "Keih�s";
	public static String MELEE_WEAPON_TXT_3 = "Miekka";
	public static String MELEE_WEAPON_TXT_4 = "Kirves";
	public static String MELEE_WEAPON_TXT_5 = "Nuija";
	
	public static String RANGED_WEAPON_TXT_1 = "Jousi";
	public static String RANGED_WEAPON_TXT_2 = "Varsijousi";
	
	public static String OTHER_ABILITY_TXT_1 = "V�ist�";
	public static String OTHER_ABILITY_TXT_2 = "Loitsunvastustus";
	
	public void paintComponent (Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
	    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    
	    if (GAME_WINDOW_CURRENT == GAME_WINDOW_TITLESCREEN) {
	    	g2d.drawImage(ImageHandler.titleScreenImage, 0, 0, this);

	        FontRenderContext frc = new FontRenderContext(null,false,false);

	        TextLayout tl = new TextLayout(Constants.GAME_NAME, GAME_NAME_FONT, frc);
	        AffineTransform textAt = new AffineTransform();

	        textAt.translate(39, 137); 
	        Shape outline = tl.getOutline(textAt);

	        g2d.setColor(Color.WHITE);
	        g2d.fill(outline);

	        g2d.setColor(GAME_NAME_OUTLINE_COLOR);
	        BasicStroke wideStroke = new BasicStroke(3);
	        g2d.setStroke(wideStroke); 
	        g2d.draw(outline);
	        
	        g2d.setFont(GAME_VERSION_FONT);
	    	g2d.drawString(Constants.GAME_VERSION, 42, 158);
	    }
	    
	    if (GAME_WINDOW_CURRENT != GAME_WINDOW_TITLESCREEN) {
	    	g2d.setColor(MAIN_BACKGROUND_COLOR);
			g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
			g2d.setColor(TOP1_BACKGROUND_COLOR);
			g2d.fillRect(0, 0, 488, 32);
			g2d.setColor(TOP2_BACKGROUND_COLOR);
			g2d.fillRect(0, 32, 488, 16);
			if(GAME_WINDOW_CURRENT == GAME_WINDOW_MAIN_MENU){
				g2d.setColor(BUTTON_AREA_BACKGROUND_COLOR);
			} else if(GAME_WINDOW_CURRENT == GAME_WINDOW_BATTLE_SETUP) {
				g2d.setColor(BATTLE_SETUP_BACKGROUND_COLOR);
			} else if(GAME_WINDOW_CURRENT == GAME_WINDOW_BATTLE) {
				g2d.setColor(BATTLE_BACKGROUND_COLOR);
			} else if(GAME_WINDOW_CURRENT == GAME_WINDOW_RECRUIT) {
				g2d.setColor(RECRUIT_BACKGROUND_COLOR);
			} else if(GAME_WINDOW_CURRENT == GAME_WINDOW_OTHER_TEAMS) {
				g2d.setColor(RECRUIT_BACKGROUND_COLOR);
			} else if(GAME_WINDOW_CURRENT == GAME_WINDOW_MAGE) {
				g2d.setColor(RECRUIT_BACKGROUND_COLOR);
			} else if(GAME_WINDOW_CURRENT == GAME_WINDOW_SHOP) {
				g2d.setColor(RECRUIT_BACKGROUND_COLOR);
			} else if(GAME_WINDOW_CURRENT == GAME_WINDOW_SCHOOL) {
				g2d.setColor(SCHOOL_BACKGROUND_COLOR);
			} else if(GAME_WINDOW_CURRENT == GAME_WINDOW_CHANGE_PLAYER) {
				g2d.setColor(CHANGE_PLAYER_BACKGROUND_COLOR);
			}
			g2d.fillRect(0, 48, 489, 347);
			g2d.setColor(BOTTOM_TXT_BACKGROUND_COLOR);
			if(GAME_WINDOW_CURRENT == GAME_WINDOW_BATTLE) {
				g2d.fillRect(0, 392, 489, 136);
			} else {
				g2d.fillRect(0, 395, 489, 133);
			}
			g2d.setColor(Color.BLACK);
			g2d.setFont(BIG_FONT);
			g2d.drawString(Game.currentTeam.name, 32, 21);
			g2d.drawString(Constants.MONEY_PREFIX + " " + Game.currentTeam.money + " " + Constants.MONEY_SUFFIX, 249, 21);
			g2d.setFont(MAIN_FONT);
			g2d.drawString(RACE_OF_MONTH_TXT + ": " + Game.RACE_OF_MONTH.name, 30, 45);
			if(Game.currentBattle == null){
				g2d.drawString(Constants.MONTHS[Game.MONTH] + " " + Game.YEAR + " " + Constants.YEAR_SUFFIX, 248, 45);
			} else {
				g2d.drawString(Constants.MONTHS[Game.MONTH] + " " + Game.YEAR + " " + Constants.YEAR_SUFFIX+" ("+ROUND_TXT+" "+(Game.currentBattle.round+1)+"/"+Constants.MAX_BATTLE_ROUNDS+")", 248, 45);
			}

			g2d.setFont(MSG_FONT);
			for (int i = 0; i < GamePanel.MESSAGES.length; i++) {
				String s = GamePanel.MESSAGES[i];
				g2d.drawString(s, 0, 408 + (i * 15));
			}
	    	
			g2d.setFont(BOTTOM_TXT_FONT);
			
			if(Game.currentTeam.gladiators.size() > 0 || GAME_WINDOW_CURRENT == GAME_WINDOW_RECRUIT || GAME_WINDOW_CURRENT == GAME_WINDOW_OTHER_TEAMS){
				g2d.setColor(GLADIATOR_TITLE_COLOR);
				if(Game.currentGladiator != null){
					if(Game.currentGladiator.specialGladiator){
						g2d.setColor(SPECIAL_GLADIATOR_TITLE_COLOR);
					}
				}
				if(Game.currentGladiator != null)
					Util.drawCenteredUnderlinedString(Game.currentGladiator.name, GLADIATOR_NAME_FONT, 640, 50, g2d);
				g2d.setColor(GLADIATOR_TITLE_COLOR);
				if(Game.currentGladiator != null)
					g2d.drawString(Util.underlinedString(SKILL_EFFECTS_TXT + " " + Game.currentGladiator.skillEffectCount, GLADIATOR_STAT_TITLE_FONT), 551, 157);

				g2d.drawString(Util.underlinedString(MELEE_SKILLS_TITLE_TXT, GLADIATOR_STAT_TITLE_FONT), 495, 229);
				g2d.drawString(Util.underlinedString(MELEE_SKILLS_ATK_TXT, GLADIATOR_STAT_TITLE_FONT), 545, 229);
				g2d.drawString(Util.underlinedString(MELEE_SKILLS_DEF_TXT, GLADIATOR_STAT_TITLE_FONT), 587, 229);

				g2d.drawString(Util.underlinedString(RANGED_SKILLS_TITLE_TXT, GLADIATOR_STAT_TITLE_FONT), 628, 229);
				g2d.drawString(Util.underlinedString(RANGED_SKILLS_ATK_TXT, GLADIATOR_STAT_TITLE_FONT), 733, 229);

				g2d.drawString(Util.underlinedString(OTHER_SKILLS_TITLE_TXT, GLADIATOR_STAT_TITLE_FONT), 628, 277);
				g2d.drawString(Util.underlinedString(OTHER_SKILLS_VALUE_TXT, GLADIATOR_STAT_TITLE_FONT), 733, 277);

				g2d.setFont(GLADIATOR_STAT_TITLE_FONT);
				g2d.drawString(EQUIPMENT_TITLE_TXT, 611, 333);

				g2d.drawString(SPELLS_TITLE_TXT, 541, 430);

				g2d.setFont(MAIN_FONT);
				g2d.setColor(Color.BLACK);

				g2d.drawString(RACE_TXT, 495, 69);
				g2d.drawString(SALARY_TXT, 495, 85);
				g2d.drawString(TAKEOUTS_TXT, 495, 101);
				g2d.drawString(HP_TXT, 495, 125);
				g2d.drawString(MANA_TXT, 495, 141);

				g2d.drawString(AGE_TXT, 625, 69);
				g2d.drawString(HURT_TXT, 625, 85);
				g2d.drawString(MATCHES_TXT, 625, 101);
				g2d.drawString(STR_TXT, 625, 125);
				g2d.drawString(SPEED_TXT, 625, 141);

				g2d.drawString(MORAL_TXT_1, 495, 173);
				g2d.drawString(CONDITION_TXT_1, 495, 189);
				g2d.drawString(SPELLS_TXT, 495, 205);

				g2d.drawString("(" + MORAL_TXT_2 + ")", 625, 173);
				g2d.drawString("(" + CONDITION_TXT_2 + ")", 625, 189);

				g2d.drawString(MELEE_WEAPON_TXT_1, 495, 244);
				setColorForSkill(g2d, "melee_2");
				g2d.drawString(MELEE_WEAPON_TXT_2, 495, 260);
				setColorForSkill(g2d, "melee_3");
				g2d.drawString(MELEE_WEAPON_TXT_3, 495, 276);
				setColorForSkill(g2d, "melee_4");
				g2d.drawString(MELEE_WEAPON_TXT_4, 495, 292);
				setColorForSkill(g2d, "melee_5");
				g2d.drawString(MELEE_WEAPON_TXT_5, 495, 308);

				setColorForSkill(g2d, "ranged_1");
				g2d.drawString(RANGED_WEAPON_TXT_1, 628, 244);
				setColorForSkill(g2d, "ranged_2");
				g2d.drawString(RANGED_WEAPON_TXT_2, 628, 260);

				g2d.setColor(Color.BLACK);
				
				g2d.drawString(OTHER_ABILITY_TXT_1, 628, 292);
				g2d.drawString(OTHER_ABILITY_TXT_2, 628, 308);

				if(Game.currentGladiator != null){
					if(Game.currentGladiator.race == Game.RACE_OF_MONTH){
						g2d.setColor(RACE_OF_MONTH_COLOR);
					}
				}
				
				if(Game.currentGladiator != null){
					g2d.drawString(Game.currentGladiator.race.name, 543, 69);
					g2d.setColor(Color.BLACK);
					g2d.drawString(Game.currentGladiator.salary + " " + Constants.MONEY_SUFFIX, 543, 85);
					g2d.drawString(Game.currentGladiator.takeOuts_1 + "/" + Game.currentGladiator.takeOuts_2, 543, 101);
					g2d.drawString(Game.currentGladiator.hp + "/" + Game.currentGladiator.maxHp, 543, 125);
					g2d.drawString(Game.currentGladiator.mana + "/" + Game.currentGladiator.maxMana + " "+Util.getBonusTxt(Game.currentGladiator.manaBonus), 543, 141);
	
					g2d.drawString(Game.currentGladiator.age + "/" + Constants.GLADIATOR_MAX_AGE, 728, 69);
					if(Game.currentGladiator.hurt > 0){
						g2d.setColor(RACE_OF_MONTH_COLOR);
						g2d.drawString(Game.currentGladiator.hurt+" kk", 728, 85);
						g2d.setColor(Color.BLACK);
					} else {
						g2d.drawString("Ei ole", 728, 85);
					}
					g2d.drawString(""+Game.currentGladiator.matches, 728, 101);
					g2d.drawString(Game.currentGladiator.str + " " + Util.getBonusTxt(Game.currentGladiator.strBonus), 728, 125);
					g2d.drawString(Game.currentGladiator.speed + " " + Util.getBonusTxt(Game.currentGladiator.speedBonus), 728, 141);
	
					g2d.drawString(""+Game.currentGladiator.moral, 543, 173);
					g2d.drawString(""+Game.currentGladiator.condition, 543, 189);
					g2d.drawString(""+Game.currentGladiator.spells, 543, 205);
	
					
					setColorForSkill(g2d, "melee_1");
					g2d.drawString(""+Game.currentGladiator.melee_weapon_atk_1, 545, 244);
					setColorForSkill(g2d, "melee_2");
					g2d.drawString(""+Game.currentGladiator.melee_weapon_atk_2, 545, 260);
					setColorForSkill(g2d, "melee_3");
					g2d.drawString(""+Game.currentGladiator.melee_weapon_atk_3, 545, 276);
					setColorForSkill(g2d, "melee_4");
					g2d.drawString(""+Game.currentGladiator.melee_weapon_atk_4, 545, 292);
					setColorForSkill(g2d, "melee_5");
					g2d.drawString(""+Game.currentGladiator.melee_weapon_atk_5, 545, 308);
	
					setColorForSkill(g2d, "melee_1");
					g2d.drawString(""+Game.currentGladiator.melee_weapon_def_1, 587, 244);
					setColorForSkill(g2d, "melee_2");
					g2d.drawString(""+Game.currentGladiator.melee_weapon_def_2, 587, 260);
					setColorForSkill(g2d, "melee_3");
					g2d.drawString(""+Game.currentGladiator.melee_weapon_def_3, 587, 276);
					setColorForSkill(g2d, "melee_4");
					g2d.drawString(""+Game.currentGladiator.melee_weapon_def_4, 587, 292);
					setColorForSkill(g2d, "melee_5");
					g2d.drawString(""+Game.currentGladiator.melee_weapon_def_4, 587, 308);
	
					setColorForSkill(g2d, "ranged_1");
					g2d.drawString(""+Game.currentGladiator.ranged_weapon_atk_1, 733, 244);
					setColorForSkill(g2d, "ranged_2");
					g2d.drawString(""+Game.currentGladiator.ranged_weapon_atk_2, 733, 260);
					
					g2d.setColor(Color.BLACK);
	
					g2d.drawString(""+Game.currentGladiator.ability1, 733, 292);
					g2d.drawString(""+Game.currentGladiator.ability2, 733, 308);
					
					g2d.drawString(Game.currentGladiator.getMeleeDmgValue(), 737, 357);
					g2d.drawString(Game.currentGladiator.getRangedDmgValue(), 737, 381);
					g2d.drawString(Game.currentGladiator.getArmorValue()+" PP", 737, 405);
				}
			
				boolean showCurrentTeamGladiators = true;
				if(Game.currentGladiator != null){
					if(Game.currentGladiator.owner != Game.currentTeam)
						showCurrentTeamGladiators = false;
				}
				
				if (showCurrentTeamGladiators && (GAME_WINDOW_CURRENT != GAME_WINDOW_RECRUIT && GAME_WINDOW_CURRENT != GAME_WINDOW_OTHER_TEAMS)) {
					for(int i = 0; i < Game.currentTeam.gladiators.size(); i++){
						Gladiator gladiator_ = Game.currentTeam.gladiators.get(i);
						g2d.drawImage(ImageHandler.gladiatorImages[gladiator_.race.getIdx()], 497 + (i*31), 1, this);
						if(gladiator_.hurt > 0){
				    		g2d.setFont(HURT_FONT);
				    		g2d.setColor(Color.RED);
				    		g2d.drawString(""+gladiator_.hurt, 498 + (i*31), 9);
				    	}
					}

					g2d.setColor(SELECTED_GLADIATOR_COLOR);
					if(Game.currentGladiator != null){
						int idx = Game.currentTeam.gladiators.indexOf(Game.currentGladiator);
						/*
						if(idx == -1){
							System.out.println(idx+" - "+Game.currentGladiator.name+" - "+Game.currentGladiator.owner.name+" "+Game.currentTeam.gladiators.size());
							for(int i = 0; i < Game.currentTeam.gladiators.size(); i++){
								System.out.println(Game.currentTeam.gladiators.get(i).name);
							}
						}
						*/
						//System.out.println(idx+" "+Game.currentGladiator.name+" "+Game.currentGladiator.owner.name);
						g2d.drawRect(496 + (idx*31), 0, 31, 31);
					}
					
				} else {
					
				}
			}
		}
	    
	    if (GAME_WINDOW_CURRENT == GAME_WINDOW_BATTLE_SETUP) {
	    	g2d.setFont(MAIN_FONT);
	    	g2d.setColor(Color.WHITE);
	    	g2d.fillRect(16, 64, 457, 193);
	    	if(Game.getNextOpponentForTeam(Game.currentTeam) != null)
	    		g2d.drawString(OPPONENT_TXT+" "+Game.getNextOpponentForTeam(Game.currentTeam).name+".", 16, 277);
	    	g2d.drawString(RESTING_GLADIATORS_SALARY_TXT, 16, 309);
	    	g2d.drawString(BATTLE_COST_TXT+" "+Game.getSalaryForGladiators(Game.currentTeam)+" "+Constants.MONEY_SUFFIX+".", 16, 325);
	    	
	    	g2d.setColor(Color.BLACK);
	    	g2d.drawString(BATTLE_FORMATION_TXT, 44, 93);
	    	
	    	g2d.setFont(MENU_FONT);
	    	for(int i = 0; i < Constants.MAX_GLADIATORS_IN_BATTLE; i++){
	    		if(Game.currentTeam.battleGladiators[i] != null)
	    			continue;
	    		int rows = 2;
	    		int offX = 7;
	    		int offY = 7;
	    		int divider = Constants.MAX_GLADIATORS_IN_BATTLE / rows;
	    		Util.drawStringMultiLine(g2d, EMPTY_SLOT_TXT, 15, 195 + ((i%divider)*(33+offX)), 133 + ((i/divider)*(33+offY)), 13);
			}
	    	for(int i = 0; i < Constants.MAX_GLADIATORS_IN_BATTLE; i++){
	    		if(Game.currentTeam.battleGladiators[i] == null)
	    			continue;
	    		int rows = 2;
	    		int offX = 7;
	    		int offY = 7;
	    		int divider = Constants.MAX_GLADIATORS_IN_BATTLE / rows;
	    		g2d.drawImage(ImageHandler.gladiatorImages[Game.currentTeam.battleGladiators[i].race.getIdx()], 193 + ((i%divider)*(33+offX)), 121 + ((i/divider)*(33+offY)), this);
			}
	    	for(int i = 0; i < Constants.MAX_GLADIATORS_IN_TEAM; i++){
	    		if(i < Game.currentTeam.gladiators.size())
	    			continue;
	    		int rows = 1;
	    		int offX = 7;
	    		int offY = 7;
	    		int divider = Constants.MAX_GLADIATORS_IN_TEAM / rows;
	    		Util.drawStringMultiLine(g2d, EMPTY_SLOT_TXT, 15, 75 + ((i%divider)*(33+offX)), 229 + ((i/divider)*(33+offY)), 13);
			}
	    	for(int i = 0; i < Game.currentTeam.gladiators.size(); i++){
	    		int rows = 1;
	    		int offX = 7;
	    		int offY = 7;
	    		int divider = Constants.MAX_GLADIATORS_IN_TEAM / rows;
	    		Gladiator gladiator = Game.currentTeam.gladiators.get(i);
	    		g2d.drawImage(ImageHandler.gladiatorImages[gladiator.race.getIdx()], 72 + ((i%divider)*(33+offX)), 216 + ((i/divider)*(33+offY)), this);
	    		if(gladiator.hurt > 0){
		    		g2d.setFont(HURT_FONT);
		    		g2d.setColor(Color.RED);
		    		g2d.drawString(""+gladiator.hurt, 73 + ((i%divider)*(33+offX)), 224 + ((i/divider)*(33+offY)));
		    	}
			}

	    	g2d.drawImage(ImageHandler.battleSetupArrowImage, 160, 121, this);
	    	g2d.drawImage(ImageHandler.battleSetupArrowImage, 320, 121, this);
	    	
	    	g2d.setColor(SELECTED_GLADIATOR_COLOR);
	    	for(int i = 0; i < Constants.MAX_GLADIATORS_IN_BATTLE; i++){
	    		int rows = 2;
	    		int offX = 7;
	    		int offY = 7;
	    		int divider = Constants.MAX_GLADIATORS_IN_BATTLE / rows;
	    		g2d.drawRect(192 + ((i%divider)*(33+offX)), 120 + ((i/divider)*(33+offY)), 33, 33);
			}
	    	
	    }
	    
	    if (GAME_WINDOW_CURRENT == GAME_WINDOW_BATTLE) {
	    	g2d.setFont(MAIN_FONT);
	    	g2d.drawImage(ImageHandler.mapImages[Game.BATTLE_MAP], 0, 48, this);
	    	g2d.setColor(Color.WHITE);
	    	g2d.drawString(VS_TXT, 360, 93);
	    	
	    	if(Constants.DRAW_BATTLE_GRID){
	    		g2d.setColor(Color.BLACK);
	    		for(int x = 0; x < Constants.MAP_WIDTH; x++){
	    			for(int y = 0; y < Constants.MAP_HEIGHT; y++){
	    				g2d.drawRect(8 + (x*(Constants.MAP_TILE_SIZE-1)), 54 + (y*(Constants.MAP_TILE_SIZE-1)), Constants.MAP_TILE_SIZE-1, Constants.MAP_TILE_SIZE-1);
	    			}
	    		}
	    	}
	    	for(int x = 0; x < Constants.MAP_WIDTH; x++){
    			for(int y = 0; y < Constants.MAP_HEIGHT; y++){
    				if(Game.currentBattle.map.mapTiles[x][y].gladiatorOnTile != null){
    					Gladiator gladiator = Game.currentBattle.map.mapTiles[x][y].gladiatorOnTile;
    					g2d.drawImage(ImageHandler.gladiatorImages[gladiator.race.getIdx()], 8 + (x*(Constants.MAP_TILE_SIZE-1)), 54 + (y*(Constants.MAP_TILE_SIZE-1)), this);
    					if(gladiator.receivedDmg >= 0){
    						if(gladiator.receivedDmg == 0){
    							g2d.drawImage(ImageHandler.hitmarkImages[Constants.HITMARK_MISS], 8 + (x*(Constants.MAP_TILE_SIZE-1)) + 10, 54 + (y*(Constants.MAP_TILE_SIZE-1)) + 8, this);
    						} else {
    							g2d.drawImage(ImageHandler.hitmarkImages[Constants.HITMARK_HIT], 8 + (x*(Constants.MAP_TILE_SIZE-1)) + 10, 54 + (y*(Constants.MAP_TILE_SIZE-1)) + 8, this);
    						}
    					}
    					if(gladiator.hp <= 0){
    						g2d.drawImage(ImageHandler.hitmarkImages[Constants.HITMARK_KO], 8 + (x*(Constants.MAP_TILE_SIZE-1)) + 10, 54 + (y*(Constants.MAP_TILE_SIZE-1)) + 8, this);
    					}
    				}
    			}
	    	}
	    	if(Game.currentBattle.battleStarted){
	    		g2d.setColor(SELECTED_GLADIATOR_COLOR);
	    		Gladiator gladiator = Game.currentBattle.getCurrentBattleGladiator();
	    		if(gladiator != null){
	    			int x = gladiator.pos.getX();
	    			int y = gladiator.pos.getY();
	    			g2d.drawRect(8 + (x*(Constants.MAP_TILE_SIZE-1)), 54 + (y*(Constants.MAP_TILE_SIZE-1)), Constants.MAP_TILE_SIZE-1, Constants.MAP_TILE_SIZE-1);
	    		}
	    	}
	    	
	    	if(Game.currentGladiator != null && Game.currentGladiator.owner != Game.currentTeam){
	    		g2d.drawImage(ImageHandler.gladiatorImages[Game.currentGladiator.race.getIdx()], 623, 0, this);
	    	}
	    }
	    
	    if (GAME_WINDOW_CURRENT == GAME_WINDOW_RECRUIT) {
	    	g2d.setFont(RECRUIT_TITLE_FONT);
	    	g2d.setColor(Color.BLACK);
	    	g2d.drawString(RECRUIT_TITLE_TXT, 76, 71);
	    	
	    	g2d.setFont(RECRUIT_TABLE_TITLE_FONT);
	    	g2d.drawString(RECRUIT_TABLE_GLADIATOR_NAME_TITLE_TXT, 8, 89);
	    	g2d.drawString(RECRUIT_TABLE_GLADIATOR_RACE_TITLE_TXT, 133, 89);
	    	g2d.drawString(RECRUIT_TABLE_GLADIATOR_ASK_TITLE_TXT, 224, 89);
	    	g2d.drawString(RECRUIT_TABLE_GLADIATOR_OFFER_TITLE_TXT, 287, 89);
	    	g2d.drawString(RECRUIT_TABLE_GLADIATOR_TEAM_TITLE_TXT, 350, 89);
	    	
	    	if(Game.currentGladiator != null){
	    		g2d.drawImage(ImageHandler.gladiatorImages[Game.currentGladiator.race.getIdx()], 623, 0, this);
	    	}
	    }
	    
	    if (GAME_WINDOW_CURRENT == GAME_WINDOW_MAGE) {
	    	g2d.setFont(RECRUIT_TITLE_FONT);
	    	g2d.setColor(Color.BLACK);
	    	g2d.drawString(MAGE_TITLE_TXT, 214, 71);
	    	g2d.setFont(RECRUIT_TABLE_TITLE_FONT);
	    	g2d.drawString(MAGE_TABLE_SPELL_NAME_TITLE_TXT, 7, 89);
	    	g2d.drawString(MAGE_TABLE_SPELL_TYPE_TITLE_TXT, 203, 89);
	    	g2d.drawString(MAGE_TABLE_SPELL_PRICE_TITLE_TXT, 345, 89);
	    }
	    
	    if (GAME_WINDOW_CURRENT == GAME_WINDOW_SHOP) {
	    	g2d.setFont(RECRUIT_TITLE_FONT);
	    	g2d.setColor(Color.BLACK);
	    	g2d.drawString(SHOP_TITLE_TXT, 211, 71);
	    	g2d.setFont(RECRUIT_TABLE_TITLE_FONT);
	    	if(SUB_GAME_WINDOW_CURRENT == SUB_GAME_WINDOW_SHOP_MELEE){
	    		g2d.drawString(MELEE_TABLE_ITEM_NAME_TITLE_TXT, 8, 155);
	    		g2d.drawString(MELEE_TABLE_DMG_TITLE_TXT, 140, 155);
	    		g2d.drawString(MELEE_TABLE_HB_TITLE_TXT, 209, 155);
	    		g2d.drawString(MELEE_TABLE_PB_TITLE_TXT, 243, 155);
	    		g2d.drawString(MELEE_TABLE_PRICE_TITLE_TXT, 277, 155);
	    	} else if(SUB_GAME_WINDOW_CURRENT == SUB_GAME_WINDOW_SHOP_RANGED){
	    		g2d.drawString(RANGED_TABLE_ITEM_NAME_TITLE_TXT, 8, 155);
	    		g2d.drawString(RANGED_TABLE_DMG_TITLE_TXT, 189, 155);
	    		g2d.drawString(RANGED_TABLE_PRICE_TITLE_TXT, 258, 155);
	    	} else if(SUB_GAME_WINDOW_CURRENT == SUB_GAME_WINDOW_SHOP_ARMOUR){
	    		g2d.drawString(ARMOUR_TABLE_ITEM_NAME_TITLE_TXT, 8, 155);
	    		g2d.drawString(ARMOUR_TABLE_PP_TITLE_TXT, 218, 155);
	    		g2d.drawString(ARMOUR_TABLE_PRICE_TITLE_TXT, 280, 155);
	    	} else if(SUB_GAME_WINDOW_CURRENT == SUB_GAME_WINDOW_SHOP_DISCOUNT){
	    		g2d.drawString(DISCOUNT_TABLE_ITEM_NAME_TITLE_TXT, 8, 155);
	    		g2d.drawString(DISCOUNT_TABLE_DISCOUNT_PRICE_TITLE_TXT, 182, 155);
	    		g2d.drawString(DISCOUNT_TABLE_NORMAL_PRICE_TITLE_TXT, 252, 155);
	    	}

	    }
	    
	    if (GAME_WINDOW_CURRENT == GAME_WINDOW_OTHER_TEAMS) {
	    	g2d.setFont(RECRUIT_TITLE_FONT);
	    	g2d.setColor(Color.BLACK);
	    	g2d.drawString(OTHER_TEAMS_TITLE_TXT, 154, 71);
	    	g2d.setFont(RECRUIT_TABLE_TITLE_FONT);
	    	g2d.drawString(OTHER_TEAMS_TABLE_TEAM_NAME_TITLE_TXT, 8, 89);
	    	g2d.drawString(OTHER_TEAMS_TABLE_TEAM_DIVISION_TITLE_TXT, 204, 89);
	    	g2d.drawString(OTHER_TEAMS_TABLE_TEAM_TOUGHNESS_TITLE_TXT, 344, 89);
	    	
	    	if(Game.showGladiatorsForTeam != null){
	    	
		    	for(int i = 0; i < Game.showGladiatorsForTeam.gladiators.size(); i++){
					Gladiator gladiator_ = Game.showGladiatorsForTeam.gladiators.get(i);
					g2d.drawImage(ImageHandler.gladiatorImages[gladiator_.race.getIdx()], 497 + (i*31), 1, this);
					if(gladiator_.hurt > 0){
			    		g2d.setFont(HURT_FONT);
			    		g2d.setColor(Color.RED);
			    		g2d.drawString(""+gladiator_.hurt, 498 + (i*31), 9);
			    	}
				}
		    	
		    	g2d.setColor(SELECTED_GLADIATOR_COLOR);
				if(Game.currentGladiator != null){
					int idx = Game.showGladiatorsForTeam.gladiators.indexOf(Game.currentGladiator);
					g2d.drawRect(496 + (idx*31), 0, 31, 31);
				}
			
	    	}
	    }
	    
	    if (GAME_WINDOW_CURRENT == GAME_WINDOW_MAIN_MENU) {
			
	    	g2d.setFont(BOTTOM_TXT_FONT);
			g2d.setColor(Color.WHITE);
			g2d.drawString(RATING_TXT + " (" + Game.currentTeam.rating + ") - " + ARENA_POINTS_TXT + ": " + Game.currentTeam.arenaPoints, 79, 388);

	    }

	}
	
	public void setColorForSkill(Graphics2D g2d, String skill){
		g2d.setColor(Color.BLACK);
		if(Game.currentGladiator == null)
			return;
		if(Game.currentGladiator.getMeleeWeapon() != null){
			Item weapon = Game.currentGladiator.getMeleeWeapon();
			if(weapon.subtype == Item.SUBTYPE_MELEE_WEAPON_SPEAR){
				if(skill.equals("melee_2")){
					g2d.setColor(RACE_OF_MONTH_COLOR);
				}
			}
			else if(weapon.subtype == Item.SUBTYPE_MELEE_WEAPON_SWORD){
				if(skill.equals("melee_3")){
					g2d.setColor(RACE_OF_MONTH_COLOR);
				}
			}
			else if(weapon.subtype == Item.SUBTYPE_MELEE_WEAPON_AXE){
				if(skill.equals("melee_4")){
					g2d.setColor(RACE_OF_MONTH_COLOR);
				}
			}
			else if(weapon.subtype == Item.SUBTYPE_MELEE_WEAPON_MACE){
				if(skill.equals("melee_5")){
					g2d.setColor(RACE_OF_MONTH_COLOR);
				}
			}
		} else {
			if(skill.equals("melee_1")){
				g2d.setColor(RACE_OF_MONTH_COLOR);
			}
		}
		if(Game.currentGladiator.getRangedWeapon() != null){
			Item weapon = Game.currentGladiator.getRangedWeapon();
			if(weapon.subtype == Item.SUBTYPE_RANGED_WEAPON_BOW){
				if(skill.equals("ranged_1")){
					g2d.setColor(RACE_OF_MONTH_COLOR);
				}
			}
			else if(weapon.subtype == Item.SUBTYPE_RANGED_WEAPON_CROSSBOW){
				if(skill.equals("ranged_2")){
					g2d.setColor(RACE_OF_MONTH_COLOR);
				}
			}
		}
	}
	
	public void offerRecruitAmount(int amount){
		JTable table = findTable(RECRUIT_TABLE);
		if(table.getSelectedRow() != -1){
			Gladiator gladiator = Game.recruitableGladiators.get(table.getSelectedRow());
			Game.putRecruitOffer(gladiator, amount);
			addGladiatorsToRecruitTable();
		}
	}
	
	public Gladiator recruitGladiator(){
		JTable table = findTable(RECRUIT_TABLE);
		if(table.getSelectedRow() != -1){
			Gladiator gladiator = Game.recruitableGladiators.get(table.getSelectedRow());
			return gladiator;
		}
		return null;
	}
	
	public void trainSkill(String s){
		if(Game.currentGladiator == null)
			return;
		Game.trainSkill(s);
	}
	
	public Item buyItem(){
		Item item = null;
		String itemName = "";
		if(Game.currentGladiator == null)
			return null;
		if (SUB_GAME_WINDOW_CURRENT == SUB_GAME_WINDOW_SHOP_MELEE) {
			JTable table = findTable(MELEE_TABLE);
			if(table.getSelectedRow() != -1){
				Object obj1 = Util.GetData(table, table.getSelectedRow(), 0);
				itemName = (String)obj1;
			}
		}
		if (SUB_GAME_WINDOW_CURRENT == SUB_GAME_WINDOW_SHOP_RANGED) {
			JTable table = findTable(RANGED_TABLE);
			if(table.getSelectedRow() != -1){
				Object obj1 = Util.GetData(table, table.getSelectedRow(), 0);
				itemName = (String)obj1;
			}
		}
		if (SUB_GAME_WINDOW_CURRENT == SUB_GAME_WINDOW_SHOP_ARMOUR) {
			JTable table = findTable(ARMOUR_TABLE);
			if(table.getSelectedRow() != -1){
				Object obj1 = Util.GetData(table, table.getSelectedRow(), 0);
				itemName = (String)obj1;
			}
		}
		if (SUB_GAME_WINDOW_CURRENT == SUB_GAME_WINDOW_SHOP_DISCOUNT) {
			JTable table = findTable(DISCOUNT_TABLE);
			if(table.getSelectedRow() != -1){
				Object obj1 = Util.GetData(table, table.getSelectedRow(), 0);
				itemName = (String)obj1;
			}
		}
		item = Item.findItem(itemName);
		return item;
	}
	
	public Spell buySpell(){
		if(Game.currentGladiator == null)
			return null;
		JTable table = findTable(MAGE_TABLE);
		if(table.getSelectedRow() != -1){
			Spell spell = Spell.spells.get(table.getSelectedRow());
			return spell;
		}
		return null;
	}
	
	public void setWindowMessage(){
		if(GAME_WINDOW_CURRENT == GAME_WINDOW_MAIN_MENU){
			GamePanel.MESSAGES = new String[]{"Rotujen jumalien v�lisess� valtataistelussa voitokas rotu on "+Game.RACE_OF_MONTH.name.toUpperCase()+".",
					"",
					"Rodun edustajien tekem�t vahingot ja loitsujen vaikutukset ovat t�ss� kuussa kaksinkertaiset."};
		}	
		else if(GAME_WINDOW_CURRENT == GAME_WINDOW_RECRUIT){
			if(Game.currentRecruitRound > Constants.RECRUIT_TURNS){
				GamePanel.MESSAGES = new String[]{"T�m�n kuukauden tarjousvaihe on p��ttynyt ja v�rv�tyt gladiaattorit ovat liittyneet uusiin",
										"joukkueisiinsa."};
			} else {
				Team team = Game.teamRecruitTurnOrder.get(Game.currentRecruitTeamIdx-1);
				GamePanel.MESSAGES = new String[]{"Nyt on menossa tarjouskierros "+Game.currentRecruitRound+"/"+Constants.RECRUIT_TURNS+" ja tarjousvuorossa oleva joukkue on "+team.name+".",
						"",
						"Listassa ovat tarjouksia odottavat gladiaattorit. Tee tarjous haluamallesi gladiaattorille tai j�t�",
						"tarjousvuorosi v�liin. Taistelut voi aloittaa vasta kun kaikki t�m�n kuukauden tarjouskierrokset",
						"on k�yty l�pi."};
			}
			/*
			MESSAGES = new String[]{"Nyt on menossa tarjouskierros "+currentRecruitRound+"/"+Constants.RECRUIT_TURNS+" ja tarjousvuorossa oleva joukkue on "+team.name+".",
					"",
					"Listassa ovat tarjouksia odottavat gladiaattorit. Tee tarjous haluamallesi gladiaattorille tai j�t�",
					"tarjousvuorosi v�liin. Taistelut voi aloittaa vasta kun kaikki t�m�n kuukauden tarjouskierrokset",
					"on k�yty l�pi.",
					"",
					"V�rv�yst� odottavien gladiaattorien joukossa on YKSI YLITSE MUIDEN, legendaarinen",
					"SANKARI nimelt� Merlin."};
			*/
		}
		else if(GAME_WINDOW_CURRENT == GAME_WINDOW_BATTLE_SETUP){
			GamePanel.MESSAGES = new String[]{"Taisteluun voi osallistua enint��n 6 gladiaattoria."};
		}
		else if(GAME_WINDOW_CURRENT == GAME_WINDOW_OTHER_TEAMS){
			GamePanel.MESSAGES = new String[]{"Uskollinen apulaisesi toimittaa muista joukkueista yll�tt�v�n tarkat tiedot..."};
		}
	}
	
	public GamePanel(Jframe frame){
		this.mainFrame = frame;
		this.setLayout(null);

		this.add(button(GAME_WINDOW_MAIN_MENU, START_FIGHT_CHANGE_PLAYER_BUTTON_NAME, START_FIGHT_TXT, MAIN_BUTTON_FONT, 48, 64, 393, 33));
		this.add(button(GAME_WINDOW_MAIN_MENU, SHOP_TXT, MAIN_BUTTON_FONT, 248, 112, 193, 33));
		this.add(button(GAME_WINDOW_MAIN_MENU, RECRUIT_BUTTON_NAME, RECRUIT_TXT, RECRUIT_BUTTON_FONT, 48, 112, 193, 33));
		this.add(button(GAME_WINDOW_MAIN_MENU, MAGE_SHOP_TXT, MAIN_BUTTON_FONT, 48, 152, 193, 33));
		this.add(button(GAME_WINDOW_MAIN_MENU, TRAINING_TXT, MAIN_BUTTON_FONT, 248, 152, 193, 33));
		this.add(button(GAME_WINDOW_MAIN_MENU, MATCH_SCHELUDE_TXT, MAIN_BUTTON_FONT, 48, 200, 393, 33));
		this.add(button(GAME_WINDOW_MAIN_MENU, STANDINGS_TXT, MAIN_BUTTON_FONT, 48, 248, 193, 33));
		this.add(button(GAME_WINDOW_MAIN_MENU, CUP_STANDINGS_TXT, MAIN_BUTTON_FONT, 248, 248, 193, 33));
		this.add(button(GAME_WINDOW_MAIN_MENU, OWN_TEAM_TXT, MAIN_BUTTON_FONT, 48, 288, 193, 33));
		this.add(button(GAME_WINDOW_MAIN_MENU, OTHER_TEAMS_TXT, MAIN_BUTTON_FONT, 248, 288, 193, 33));
		this.add(button(GAME_WINDOW_MAIN_MENU, SHOW_OPPONENT_BUTTON_NAME, SHOW_OPPONENT_TXT, MAIN_BUTTON_FONT, 48, 336, 393, 33));
		this.add(button(new int[]{GAME_WINDOW_MAIN_MENU, GAME_WINDOW_BATTLE_SETUP, GAME_WINDOW_BATTLE, GAME_WINDOW_RECRUIT, GAME_WINDOW_SCHOOL, GAME_WINDOW_MAGE, GAME_WINDOW_SHOP, GAME_WINDOW_OTHER_TEAMS, GAME_WINDOW_CHANGE_PLAYER}, CURRENT_GLADIATOR_MELEE_WEAPON_BUTTON_NAME, CURRENT_GLADIATOR_MELEE_WEAPON_TXT, EQUIPMENT_BUTTON_FONT, 496, 336, 233, 25));
		this.add(button(new int[]{GAME_WINDOW_MAIN_MENU, GAME_WINDOW_BATTLE_SETUP, GAME_WINDOW_BATTLE, GAME_WINDOW_RECRUIT, GAME_WINDOW_SCHOOL, GAME_WINDOW_MAGE, GAME_WINDOW_SHOP, GAME_WINDOW_OTHER_TEAMS, GAME_WINDOW_CHANGE_PLAYER}, CURRENT_GLADIATOR_RANGED_WEAPON_BUTTON_NAME, CURRENT_GLADIATOR_RANGED_WEAPON_TXT, EQUIPMENT_BUTTON_FONT, 496, 360, 233, 25));
		this.add(button(new int[]{GAME_WINDOW_MAIN_MENU, GAME_WINDOW_BATTLE_SETUP, GAME_WINDOW_BATTLE, GAME_WINDOW_RECRUIT, GAME_WINDOW_SCHOOL, GAME_WINDOW_MAGE, GAME_WINDOW_SHOP, GAME_WINDOW_OTHER_TEAMS, GAME_WINDOW_CHANGE_PLAYER}, CURRENT_GLADIATOR_ARMOR_BUTTON_NAME, CURRENT_GLADIATOR_ARMOR_TXT, EQUIPMENT_BUTTON_FONT, 496, 384, 233, 25));
		this.add(button(new int[]{GAME_WINDOW_MAIN_MENU, GAME_WINDOW_BATTLE_SETUP, GAME_WINDOW_BATTLE, GAME_WINDOW_RECRUIT, GAME_WINDOW_SCHOOL, GAME_WINDOW_MAGE, GAME_WINDOW_SHOP, GAME_WINDOW_OTHER_TEAMS, GAME_WINDOW_CHANGE_PLAYER}, CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_1, CURRENT_GLADIATOR_KNOWN_SPELLS_TXT_1, EQUIPMENT_BUTTON_FONT, 496, 432, 145, 25));
		this.add(button(new int[]{GAME_WINDOW_MAIN_MENU, GAME_WINDOW_BATTLE_SETUP, GAME_WINDOW_BATTLE, GAME_WINDOW_RECRUIT, GAME_WINDOW_SCHOOL, GAME_WINDOW_MAGE, GAME_WINDOW_SHOP, GAME_WINDOW_OTHER_TEAMS, GAME_WINDOW_CHANGE_PLAYER}, CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_2, CURRENT_GLADIATOR_KNOWN_SPELLS_TXT_2, EQUIPMENT_BUTTON_FONT, 640, 432, 145, 25));
		this.add(button(new int[]{GAME_WINDOW_MAIN_MENU, GAME_WINDOW_BATTLE_SETUP, GAME_WINDOW_BATTLE, GAME_WINDOW_RECRUIT, GAME_WINDOW_SCHOOL, GAME_WINDOW_MAGE, GAME_WINDOW_SHOP, GAME_WINDOW_OTHER_TEAMS, GAME_WINDOW_CHANGE_PLAYER}, CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_3, CURRENT_GLADIATOR_KNOWN_SPELLS_TXT_3, EQUIPMENT_BUTTON_FONT, 496, 456, 145, 25));
		this.add(button(new int[]{GAME_WINDOW_MAIN_MENU, GAME_WINDOW_BATTLE_SETUP, GAME_WINDOW_BATTLE, GAME_WINDOW_RECRUIT, GAME_WINDOW_SCHOOL, GAME_WINDOW_MAGE, GAME_WINDOW_SHOP, GAME_WINDOW_OTHER_TEAMS, GAME_WINDOW_CHANGE_PLAYER}, CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_4, CURRENT_GLADIATOR_KNOWN_SPELLS_TXT_4, EQUIPMENT_BUTTON_FONT, 640, 456, 145, 25));
		this.add(button(new int[]{GAME_WINDOW_MAIN_MENU, GAME_WINDOW_BATTLE_SETUP, GAME_WINDOW_BATTLE, GAME_WINDOW_RECRUIT, GAME_WINDOW_SCHOOL, GAME_WINDOW_MAGE, GAME_WINDOW_SHOP, GAME_WINDOW_OTHER_TEAMS, GAME_WINDOW_CHANGE_PLAYER}, CURRENT_GLADIATOR_SPELL_EFFECTS_BUTTON_NAME, CURRENT_GLADIATOR_SPELL_EFFECTS_TXT, EQUIPMENT_BUTTON_FONT, 496, 496, 289, 25));
		
		this.add(button(GAME_WINDOW_BATTLE_SETUP, BEGIN_FIGHT_TXT, EQUIPMENT_BUTTON_FONT, 16, 352, 265, 33));
		this.add(button(GAME_WINDOW_BATTLE_SETUP, BACK_TO_MAIN_MENU_TXT, EQUIPMENT_BUTTON_FONT, 320, 352, 153, 33));
		this.add(button(GAME_WINDOW_BATTLE_SETUP, SHOW_OPPONENT_TEAM_TXT, EQUIPMENT_BUTTON_FONT, 264, 264, 209, 17));
		this.add(button(GAME_WINDOW_BATTLE_SETUP, AUTO_FORMATION_BUTTON_NAME, AUTO_FORMATION_TXT, EQUIPMENT_BUTTON_FONT, 32, 120, 113, 81));
		this.add(button(GAME_WINDOW_BATTLE_SETUP, CLEAR_FORMATION_BUTTON_NAME, CLEAR_FORMATION_TXT, EQUIPMENT_BUTTON_FONT, 353, 121, 104, 81));
		
		this.add(button(GAME_WINDOW_SCHOOL, TRAIN_STR_TXT, SCHOOL_BUTTON_FONT, 8, 64, 472, 33));
		this.add(button(GAME_WINDOW_SCHOOL, TRAIN_SPEED_TXT, SCHOOL_BUTTON_FONT, 8, 104, 472, 33));
		this.add(button(GAME_WINDOW_SCHOOL, TRAIN_MANA_TXT, SCHOOL_BUTTON_FONT, 8, 144, 472, 33));
		this.add(button(GAME_WINDOW_SCHOOL, TRAIN_HP_TXT, SCHOOL_BUTTON_FONT, 8, 184, 472, 33));
		this.add(button(GAME_WINDOW_SCHOOL, EXIT_SCHOOL_TXT, SCHOOL_BUTTON_FONT, 8, 353, 472, 33));
		
		this.add(button(GAME_WINDOW_BATTLE, OK_BATTLE_BUTTON_NAME, CONFIRM_BATTLE_TXT, EQUIPMENT_BUTTON_FONT, 256, 56, 225, 331));
		this.add(button(GAME_WINDOW_BATTLE, BATTLE_TEAM_1_BUTTON_NAME, BATTLE_TEAM_1_TXT, EQUIPMENT_BUTTON_FONT, 256, 56, 225, 25));
		this.add(button(GAME_WINDOW_BATTLE, BATTLE_TEAM_2_BUTTON_NAME, BATTLE_TEAM_2_TXT, EQUIPMENT_BUTTON_FONT, 256, 96, 225, 25));
		this.add(button(GAME_WINDOW_BATTLE, AUTO_ROUND, EQUIPMENT_BUTTON_FONT, 256, 144, 225, 25));
		this.add(button(GAME_WINDOW_BATTLE, BATTLE_DIR_BUTTON_NW, EQUIPMENT_BUTTON_FONT, 280, 176, 57, 41));
		this.add(button(GAME_WINDOW_BATTLE, BATTLE_DIR_BUTTON_N, EQUIPMENT_BUTTON_FONT, 336, 176, 57, 41));
		this.add(button(GAME_WINDOW_BATTLE, BATTLE_DIR_BUTTON_NE, EQUIPMENT_BUTTON_FONT, 392, 176, 57, 41));
		this.add(button(GAME_WINDOW_BATTLE, BATTLE_DIR_BUTTON_W, EQUIPMENT_BUTTON_FONT, 280, 216, 57, 41));
		this.add(button(GAME_WINDOW_BATTLE, BATTLE_DIR_BUTTON_E, EQUIPMENT_BUTTON_FONT, 392, 216, 57, 41));
		this.add(button(GAME_WINDOW_BATTLE, BATTLE_DIR_BUTTON_SW, EQUIPMENT_BUTTON_FONT, 280, 256, 57, 41));
		this.add(button(GAME_WINDOW_BATTLE, BATTLE_DIR_BUTTON_S, EQUIPMENT_BUTTON_FONT, 336, 256, 57, 41));
		this.add(button(GAME_WINDOW_BATTLE, BATTLE_DIR_BUTTON_SE, EQUIPMENT_BUTTON_FONT, 392, 256, 57, 41));
		this.add(button(GAME_WINDOW_BATTLE, SKIP_ROUND, EQUIPMENT_BUTTON_FONT, 336, 216, 57, 41));
		
		this.add(button(GAME_WINDOW_BATTLE, SHOOT_BUTTON_NAME, SHOOT_TXT, EQUIPMENT_BUTTON_FONT, 256, 304, 225, 25));
		this.add(button(GAME_WINDOW_BATTLE, BATTLE_SPELL_BUTTON_NAME_1, BATTLE_SPELL_TXT_1, EQUIPMENT_BUTTON_FONT, 256, 336, 112, 24));
		this.add(button(GAME_WINDOW_BATTLE, BATTLE_SPELL_BUTTON_NAME_2, BATTLE_SPELL_TXT_2, EQUIPMENT_BUTTON_FONT, 368, 336, 112, 24));
		this.add(button(GAME_WINDOW_BATTLE, BATTLE_SPELL_BUTTON_NAME_3, BATTLE_SPELL_TXT_3, EQUIPMENT_BUTTON_FONT, 256, 360, 112, 24));
		this.add(button(GAME_WINDOW_BATTLE, BATTLE_SPELL_BUTTON_NAME_4, BATTLE_SPELL_TXT_4, EQUIPMENT_BUTTON_FONT, 368, 360, 112, 24));
		
		this.add(button(GAME_WINDOW_RECRUIT, OFFER_BUTTON_NAME, OFFER_TXT, MSG_FONT, 8, 368, 153, 25));
		this.add(button(GAME_WINDOW_RECRUIT, SKIP_OFFER_TXT, MSG_FONT, 168, 368, 153, 25));
		this.add(button(GAME_WINDOW_RECRUIT, EXIT_RECRUIT_TXT, MSG_FONT, 328, 368, 153, 25));
		this.add(table(new int[]{GAME_WINDOW_RECRUIT}, RECRUIT_TABLE, new int[]{127, 91, 63, 63, 127}, RECRUIT_TABLE_TITLE_FONT, 8, 94, 268));
		
		this.add(titlebutton(GAME_WINDOW_TITLESCREEN, NEW_GAME_TXT, TITLESCREEN_BUTTON_FONT, 112, 144, 121, 25));
		this.add(titlebutton(GAME_WINDOW_TITLESCREEN, LOAD_GAME_TXT, TITLESCREEN_BUTTON_FONT, 112, 176, 121, 25));
		this.add(titlebutton(GAME_WINDOW_TITLESCREEN, ONLINE_GAME_TXT, TITLESCREEN_BUTTON_FONT, 112, 208, 121, 25));
		this.add(titlebutton(GAME_WINDOW_TITLESCREEN, EXIT_GAME_TXT, TITLESCREEN_BUTTON_FONT, 112, 238, 121, 25));
		
		this.add(button(GAME_WINDOW_MAGE, BUY_SPELL_TXT, MSG_FONT, 8, 368, 233, 25));
		this.add(button(GAME_WINDOW_MAGE, EXIT_MAGE_BUTTON_NAME, EXIT_MAGE_TXT, MSG_FONT, 248, 368, 233, 25));
		this.add(table(new int[]{GAME_WINDOW_MAGE}, MAGE_TABLE, new int[]{204, 146, 121}, RECRUIT_TABLE_TITLE_FONT, 8, 94, 268));
		
		this.add(button(GAME_WINDOW_SHOP, SPEAR_TXT, MSG_FONT, 8, 80, 113, 25));
		this.add(button(GAME_WINDOW_SHOP, SWORD_TXT, MSG_FONT, 128, 80, 113, 25));
		this.add(button(GAME_WINDOW_SHOP, AXE_TXT, MSG_FONT, 248, 80, 113, 25));
		this.add(button(GAME_WINDOW_SHOP, MACE_TXT, MSG_FONT, 368, 80, 113, 25));
		this.add(button(GAME_WINDOW_SHOP, BOW_TXT, MSG_FONT, 8, 112, 113, 25));
		this.add(button(GAME_WINDOW_SHOP, CROSSBOW_TXT, MSG_FONT, 128, 112, 113, 25));
		this.add(button(GAME_WINDOW_SHOP, ARMOUR_TXT, MSG_FONT, 248, 112, 113, 25));
		this.add(button(GAME_WINDOW_SHOP, DISCOUNT_TXT, MSG_FONT, 368, 112, 113, 25));
		this.add(button(GAME_WINDOW_SHOP, BUY_ITEM_TXT, MSG_FONT, 8, 368, 233, 25));
		this.add(button(GAME_WINDOW_SHOP, EXIT_SHOP_TXT, MSG_FONT, 248, 368, 233, 25));
		this.add(table(new int[]{GAME_WINDOW_SHOP}, new int[]{SUB_GAME_WINDOW_SHOP_MELEE}, MELEE_TABLE, new int[]{134, 70, 35, 35, 197}, RECRUIT_TABLE_TITLE_FONT, 8, 168, 198));
		this.add(table(new int[]{GAME_WINDOW_SHOP}, new int[]{SUB_GAME_WINDOW_SHOP_RANGED}, RANGED_TABLE, new int[]{183, 70, 218}, RECRUIT_TABLE_TITLE_FONT, 8, 168, 198));
		this.add(table(new int[]{GAME_WINDOW_SHOP}, new int[]{SUB_GAME_WINDOW_SHOP_ARMOUR}, ARMOUR_TABLE, new int[]{211, 63, 197}, RECRUIT_TABLE_TITLE_FONT, 8, 168, 198));
		this.add(table(new int[]{GAME_WINDOW_SHOP}, new int[]{SUB_GAME_WINDOW_SHOP_DISCOUNT}, DISCOUNT_TABLE, new int[]{176, 70, 225}, RECRUIT_TABLE_TITLE_FONT, 8, 168, 198));
		
		this.add(button(GAME_WINDOW_OTHER_TEAMS, SHOW_OTHER_TEAM_TXT, MSG_FONT, 8, 368, 233, 25));
		this.add(button(GAME_WINDOW_OTHER_TEAMS, EXIT_OTHER_TEAM_BUTTON_NAME, EXIT_OTHER_TEAM_TXT, MSG_FONT, 248, 368, 233, 25));
		this.add(table(new int[]{GAME_WINDOW_OTHER_TEAMS}, OTHER_TEAMS_TABLE, new int[]{197, 141, 133}, RECRUIT_TABLE_TITLE_FONT, 8, 94, 268));
		
		this.add(button(GAME_WINDOW_CHANGE_PLAYER, CHANGE_TO_P1_BUTTON_NAME, CHANGE_TO_P1_TXT, SCHOOL_BUTTON_FONT, 48, 94, 377, 33));
		this.add(button(GAME_WINDOW_CHANGE_PLAYER, CHANGE_TO_P2_BUTTON_NAME, CHANGE_TO_P2_TXT, SCHOOL_BUTTON_FONT, 48, 142, 377, 33));
		this.add(button(GAME_WINDOW_CHANGE_PLAYER, CHANGE_TO_P3_BUTTON_NAME, CHANGE_TO_P3_TXT, SCHOOL_BUTTON_FONT, 48, 190, 377, 33));
		this.add(button(GAME_WINDOW_CHANGE_PLAYER, CHANGE_TO_P4_BUTTON_NAME, CHANGE_TO_P4_TXT, SCHOOL_BUTTON_FONT, 48, 238, 377, 33));
		this.add(button(GAME_WINDOW_CHANGE_PLAYER, PLAYER_CHANGE_START_FIGHT_BUTTON_NAME, PLAYER_CHANGE_START_FIGHT_TXT, SCHOOL_BUTTON_FONT, 48, 318, 377, 33));
		
		updateButtons();

		setButtonImage(BATTLE_DIR_BUTTON_NW, ImageHandler.battleDirArrowImages[0]);
		setButtonImage(BATTLE_DIR_BUTTON_N, ImageHandler.battleDirArrowImages[1]);
		setButtonImage(BATTLE_DIR_BUTTON_NE, ImageHandler.battleDirArrowImages[2]);
		setButtonImage(BATTLE_DIR_BUTTON_E, ImageHandler.battleDirArrowImages[3]);
		setButtonImage(BATTLE_DIR_BUTTON_SE, ImageHandler.battleDirArrowImages[4]);
		setButtonImage(BATTLE_DIR_BUTTON_S, ImageHandler.battleDirArrowImages[5]);
		setButtonImage(BATTLE_DIR_BUTTON_SW, ImageHandler.battleDirArrowImages[6]);
		setButtonImage(BATTLE_DIR_BUTTON_W, ImageHandler.battleDirArrowImages[7]);
	}
	
	public void handleTableSelectionIdx(int idx){
		if(idx < 0)
			return;
		if(GAME_WINDOW_CURRENT == GAME_WINDOW_RECRUIT){
			Game.currentGladiator = Game.recruitableGladiators.get(idx);
			this.refresh();
		}
		else if(GAME_WINDOW_CURRENT == GAME_WINDOW_OTHER_TEAMS){
			Game.showGladiatorsForTeam = Game.teams.get(idx);
			Game.currentGladiator = Game.showGladiatorsForTeam.gladiators.get(0);
			this.refresh();
		}
	}
	
	public void checkIfClickedHero(Position pos){
		if(GAME_WINDOW_CURRENT == GAME_WINDOW_RECRUIT)
			return;
		for(int i = 0; i < Constants.MAX_GLADIATORS_IN_TEAM; i++){
			Area area = new Area(496 + (i*31), 0, 496 + (i*31) + 31, 31);
			if(area.contains(pos)){
				System.out.println("Clicked main panel gladiator: "+i);
				if(GAME_WINDOW_CURRENT == GAME_WINDOW_OTHER_TEAMS){
					if(i < Game.showGladiatorsForTeam.gladiators.size()){
						Gladiator clickedGladiator = Game.showGladiatorsForTeam.gladiators.get(i);
						if(clickedGladiator != Game.currentGladiator){
							Game.currentGladiator = clickedGladiator;
							updateButtons();
							refresh();
						}
					}
				} else {
					if(i < Game.currentTeam.gladiators.size()){
						Gladiator clickedGladiator = Game.currentTeam.gladiators.get(i);
						if(clickedGladiator != Game.currentGladiator){
							Game.currentGladiator = clickedGladiator;
							updateButtons();
							refresh();
						}
					}
				}
				return;
			}
		}
	}
	
	public Gladiator checkIfPressedBattleHero(Position pos){
		if(GAME_WINDOW_CURRENT != GAME_WINDOW_BATTLE)
			return null;
		for(int x = 0; x < Constants.MAP_WIDTH; x++){
			for(int y = 0; y < Constants.MAP_HEIGHT; y++){
				int x1 = 8 + x * Constants.MAP_TILE_SIZE;
	    		int y1 = 54 + y * Constants.MAP_TILE_SIZE;
	    		int x2 = x1 + Constants.MAP_TILE_SIZE;
	    		int y2 = y1 + Constants.MAP_TILE_SIZE;
				Area area = new Area(x1, y1, x2, y2);
				if(area.contains(pos)){
					System.out.println("Clicked map tile: "+x+","+y);
					Gladiator gladiator = Game.currentBattle.map.mapTiles[x][y].gladiatorOnTile;
					if(gladiator != null){
						return gladiator;
					}
				}
			}
		}
		return null;
	}
	
	public Gladiator checkIfPressedHero(Position pos){
		if(GAME_WINDOW_CURRENT == GAME_WINDOW_RECRUIT)
			return null;
		if(GAME_WINDOW_CURRENT == GAME_WINDOW_BATTLE_SETUP){
			for(int i = 0; i < Constants.MAX_GLADIATORS_IN_TEAM; i++){
				int rows = 1;
	    		int offX = 7;
	    		int offY = 7;
	    		int divider = Constants.MAX_GLADIATORS_IN_TEAM / rows;
	    		int x1 = 71 + ((i%divider)*(33+offX));
	    		int y1 = 215 + ((i/divider)*(33+offY));
	    		int x2 = x1 + 31;
	    		int y2 = y1 + 31;
				Area area = new Area(x1, y1, x2, y2);
				if(area.contains(pos)){
					System.out.println("Clicked battlesetup gladiator: "+i);
					if(i < Game.currentTeam.gladiators.size()){
						return Game.currentTeam.gladiators.get(i);
					}
				}
			}
		}
		return null;
	}
	
	public Gladiator checkIfPressedHeroInBattleFormationSlot(Position pos){
		if(GAME_WINDOW_CURRENT == GAME_WINDOW_RECRUIT)
			return null;
		for(int i = 0; i < Constants.MAX_GLADIATORS_IN_BATTLE; i++){
			int rows = 2;
    		int offX = 7;
    		int offY = 7;
    		int divider = Constants.MAX_GLADIATORS_IN_BATTLE / rows;
    		int x1 = 191 + ((i%divider)*(33+offX));
    		int y1 = 119 + ((i/divider)*(33+offY));
    		int x2 = x1 + 33;
    		int y2 = y1 + 33;
    		Area area = new Area(x1, y1, x2, y2);
			if(area.contains(pos)){
				System.out.println("Clicked battleformation slot: "+i);
				if(Game.currentTeam.battleGladiators[i] != null)
					return Game.currentTeam.battleGladiators[i];
			}
		}
		return null;
	}
	
	public int checkIfMouseInBattleFormationSlot(Position pos){
		if(GAME_WINDOW_CURRENT == GAME_WINDOW_RECRUIT)
			return -1;
		for(int i = 0; i < Constants.MAX_GLADIATORS_IN_BATTLE; i++){
			int rows = 2;
    		int offX = 7;
    		int offY = 7;
    		int divider = Constants.MAX_GLADIATORS_IN_BATTLE / rows;
    		int x1 = 191 + ((i%divider)*(33+offX));
    		int y1 = 119 + ((i/divider)*(33+offY));
    		int x2 = x1 + 33;
    		int y2 = y1 + 33;
    		Area area = new Area(x1, y1, x2, y2);
			if(area.contains(pos)){
				System.out.println("Clicked battleformation slot: "+i);
				return i;
			}
		}
		return -1;
	}
	
	public void addGladiatorsToRecruitTable(){
		findTableModel(RECRUIT_TABLE).setRowCount(0);
		for(Gladiator gladiator : Game.recruitableGladiators){
			Vector<Object> newRow = new Vector<Object>();
			newRow.add(gladiator.name);
			newRow.add(gladiator.race.name);
			newRow.add(gladiator.askPrice+" "+Constants.MONEY_SUFFIX);
			newRow.add(gladiator.offerPrice <= 0 ? "-" : gladiator.offerPrice+" "+Constants.MONEY_SUFFIX);
			newRow.add(gladiator.offerTeam == null ? "-" : gladiator.offerTeam.name);
			findTableModel(RECRUIT_TABLE).addRow(newRow);
		}
	}
	
	public void addSpellsToTable(){
		for(Spell spell : Spell.spells){
			Vector<Object> newRow = new Vector<Object>();
			newRow.add(spell.name);
			newRow.add(Spell.TYPES[spell.type]);
			newRow.add(spell.price);
			findTableModel(MAGE_TABLE).addRow(newRow);
		}
	}
	
	public void addItemsToTable(String table, String type){
		findTableModel(table).setRowCount(0);
		ArrayList<Item> items = new ArrayList<Item>();
		if(table.equals(MELEE_TABLE)){
			if(type.equals(SPEAR_TXT)){
				items = Item.spears;
			} else if(type.equals(SWORD_TXT)){
				items = Item.swords;
			} else if(type.equals(AXE_TXT)){
				items = Item.axes;
			} else if(type.equals(MACE_TXT)){
				items = Item.maces;
			}
			for(Item item : items){
				Vector<Object> newRow = new Vector<Object>();
				newRow.add(item.name);
				newRow.add(item.dmg);
				newRow.add(item.hb_val);
				newRow.add(item.pb_val);
				if(item.discounted){
					newRow.add(item.getDiscountedPrice()+" (T)");
				} else {
					newRow.add(item.getNormalPrice());
				}
				findTableModel(table).addRow(newRow);
			}
		} else if(table.equals(RANGED_TABLE)){
			if(type.equals(BOW_TXT)){
				items = Item.bows;
			} else if(type.equals(CROSSBOW_TXT)){
				items = Item.crossbows;
			}
			for(Item item : items){
				Vector<Object> newRow = new Vector<Object>();
				newRow.add(item.name);
				newRow.add(item.dmg);
				if(item.discounted){
					newRow.add(item.getDiscountedPrice()+" (T)");
				} else {
					newRow.add(item.getNormalPrice());
				}
				findTableModel(table).addRow(newRow);
			}
		} else if(table.equals(ARMOUR_TABLE)){
			items = Item.armours;
			for(Item item : items){
				Vector<Object> newRow = new Vector<Object>();
				newRow.add(item.name);
				newRow.add(item.pp_val);
				if(item.discounted){
					newRow.add(item.getDiscountedPrice()+" (T)");
				} else {
					newRow.add(item.getNormalPrice());
				}
				findTableModel(table).addRow(newRow);
			}
		} else if(table.equals(DISCOUNT_TABLE)){
			items = Item.getDiscountedItems();
			for(Item item : items){
				Vector<Object> newRow = new Vector<Object>();
				newRow.add(item.name);
				newRow.add(item.getDiscountedPrice());
				newRow.add(item.getNormalPrice());
				findTableModel(table).addRow(newRow);
			}
		}
	}
	
	public void addTeamsToTable(){
		findTableModel(OTHER_TEAMS_TABLE).setRowCount(0);
		for(Team team : Game.teams){
			Vector<Object> newRow = new Vector<Object>();
			newRow.add(team.name);
			newRow.add((team.division+1));
			newRow.add(team.getToughness());
			findTableModel(OTHER_TEAMS_TABLE).addRow(newRow);
		}
	}
	
	public void refresh(){
		this.setWindowMessage();
		this.revalidate();
		this.repaint();
		/*
		mainFrame.validate();
		mainFrame.repaint();
		*/
	}
	
	public void showBattleResult(){
		//mainFrame.showBattleResult();
	}
	
	public void showBattleResults(){
		mainFrame.showBattleResultsPanel();
	}
	
	public ArrayList<Table> tables = new ArrayList<Table>();
	
	public JScrollPane table(int[] windowName, String tableName, int[] columnWidths, Font f, int x, int y, int height){
		return table(windowName, null, tableName, columnWidths, f, x, y, height);
	}
	
	public JScrollPane table(int[] windowName, int[] sub_windowName, String tableName, int[] columnWidths, Font f, int x, int y, int height){

		Object[][] data = {{}};
		
		String[] columns = new String[columnWidths.length];
		for(int i = 0; i < columns.length; i++){
			columns[i] = "";
		}
		
		JTable jt = new JTable() {

			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column) {                
	                return false;               
	        };
	    };
	    
	    DefaultTableModel tableModel = new DefaultTableModel(data, columns);
	    jt.setModel(tableModel);
	    tableModel.removeRow(0);
	    
	    int width = 0;
	    for(int i = 0; i < columns.length; i++){
	    	jt.getColumnModel().getColumn(i).setPreferredWidth(columnWidths[i]);
	    	jt.getColumnModel().getColumn(i).setResizable(false);
	    	jt.getColumnModel().getColumn(i).setCellRenderer(new CustomTableCellRenderer());	
	    	width += columnWidths[i];
	    }
	    jt.getTableHeader().setUI(null);
	    jt.getTableHeader().setReorderingAllowed(false);
	    jt.setForeground(Color.BLACK);
		jt.setBackground(MAIN_BACKGROUND_COLOR);
		jt.setFont(f);
		jt.setShowGrid(false);
		jt.setRowHeight(14);
	    
        JScrollPane scrollpane = new JScrollPane(jt);
		scrollpane.setBounds(x, y, width, height);        
		for(Table table : tables){
			if(table.table_name.equals(tableName)){
				System.out.println("[ERROR]: Table with same name already exists! "+tableName);
			}
		}
		jt.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        public void valueChanged(ListSelectionEvent event) {
	            handleTableSelectionIdx(jt.getSelectedRow());
	        }
	    });
		scrollpane.getViewport().setBackground(MAIN_BACKGROUND_COLOR);
		scrollpane.setBorder(BorderFactory.createLineBorder(TABLE_BORDER_COLOR));
		tables.add(new Table(windowName, sub_windowName, tableName, jt, scrollpane, tableModel));
        return scrollpane;
	}
	
	public void setButtonImage(String buttonName, Image image){
		JButton button = findButton(buttonName);
		button.setText("");
		button.setIcon(new ImageIcon(image));
	}
	
	public Toolkit toolkit = Toolkit.getDefaultToolkit();
	
	public void updateButtons(){
		if(Player.getPlayers().size() > 1 && !Game.isOnlineGame){
			findButton(START_FIGHT_CHANGE_PLAYER_BUTTON_NAME).setText(CHANGE_PLAYER_TXT);
		} else {
			findButton(START_FIGHT_CHANGE_PLAYER_BUTTON_NAME).setText(START_FIGHT_TXT);
		}
		if(Game.currentRecruitRound > Constants.RECRUIT_TURNS){
			findButton(RECRUIT_BUTTON_NAME).setText("V�rv�ys (ohi)");
			findButton(OFFER_BUTTON_NAME).setText("Tee tarjous");
			findButton(SKIP_OFFER_TXT).setEnabled(false);
			findButton(OFFER_BUTTON_NAME).setEnabled(false);
		} else {
			findButton(RECRUIT_BUTTON_NAME).setText("V�rv�ys ("+Game.currentRecruitRound+"/"+Constants.RECRUIT_TURNS+")");
			findButton(OFFER_BUTTON_NAME).setText("Tee "+Game.currentRecruitRound+". tarjouksesi");
			findButton(SKIP_OFFER_TXT).setEnabled(true);
			findButton(OFFER_BUTTON_NAME).setEnabled(true);
		}
		if(Game.teamRecruitTurnOrder.size() > 0){
			if(Game.currentTeam != Game.teamRecruitTurnOrder.get(Game.currentRecruitTeamIdx-1)){
				findButton(OFFER_BUTTON_NAME).setText("Tee tarjous");
				findButton(SKIP_OFFER_TXT).setEnabled(false);
				findButton(OFFER_BUTTON_NAME).setEnabled(false);
			}
		}
		
		Util.setButtonStatus(findButton(CURRENT_GLADIATOR_RANGED_WEAPON_BUTTON_NAME), "ei heittoasetta");
		Util.setButtonStatus(findButton(CURRENT_GLADIATOR_ARMOR_BUTTON_NAME), "vaatteet");
		Util.setButtonStatus(findButton(CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_1), "-");
		Util.setButtonStatus(findButton(CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_2), "-");
		Util.setButtonStatus(findButton(CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_3), "-");
		Util.setButtonStatus(findButton(CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_4), "-");
		Util.setButtonStatus(findButton(CURRENT_GLADIATOR_SPELL_EFFECTS_BUTTON_NAME), "gladiaattoriin vaikuttavat loitsut (ei ole)");
		for(Button button : buttons){
			boolean hide = true;
			for(int i = 0; i < button.window_name.length; i++){
				int s = button.window_name[i];
				if(s == GAME_WINDOW_CURRENT){
					hide = false;
				}
			}
			button.jButton.setVisible(!hide);
		}
		
		for(Table table : tables){
			boolean hide = true;
			for(int i = 0; i < table.window_name.length; i++){
				int s = table.window_name[i];
				if(s == GAME_WINDOW_CURRENT){
					hide = false;
				}
				if(table.sub_window_name != null){
					hide = true;
					for(int i2 = 0; i2 < table.sub_window_name.length; i2++){
						s = table.sub_window_name[i2];
						if(s == SUB_GAME_WINDOW_CURRENT){
							hide = false;
						}
					}
				}
			}
			table.jTable.setVisible(!hide);
			table.jScrollpane.setVisible(!hide);
		}
		
		if(GAME_WINDOW_CURRENT == GAME_WINDOW_CHANGE_PLAYER){
			JButton button = findButton(CHANGE_TO_P1_BUTTON_NAME);
			button.setText(Player.getPlayers().get(0).controlledTeam.name);
			button.setVisible(true);
			button = findButton(CHANGE_TO_P2_BUTTON_NAME);
			button.setText(Player.getPlayers().get(1).controlledTeam.name);
			button.setVisible(true);
			if(Player.getPlayers().size() > 2){
				button = findButton(CHANGE_TO_P3_BUTTON_NAME);
				button.setText(Player.getPlayers().get(2).controlledTeam.name);
				button.setVisible(true);
			} else {
				button = findButton(CHANGE_TO_P3_BUTTON_NAME);
				button.setVisible(false);
			}
			if(Player.getPlayers().size() > 3){
				button = findButton(CHANGE_TO_P4_BUTTON_NAME);
				button.setText(Player.getPlayers().get(3).controlledTeam.name);
				button.setVisible(true);
			} else {
				button = findButton(CHANGE_TO_P4_BUTTON_NAME);
				button.setVisible(false);
			}
		}
		
		if(GAME_WINDOW_CURRENT == GAME_WINDOW_BATTLE){
			JButton button = findButton(OK_BATTLE_BUTTON_NAME);
			button.setVisible(!Game.currentBattle.showBattleButtons);
			button = findButton(BATTLE_TEAM_1_BUTTON_NAME);
			button.setText(Game.currentBattle.topTeam.name);
			button.setVisible(Game.currentBattle.showBattleButtons);
			button = findButton(BATTLE_TEAM_2_BUTTON_NAME);
			button.setText(Game.currentBattle.bottomTeam.name);
			button.setVisible(Game.currentBattle.showBattleButtons);
			button = findButton(AUTO_ROUND);
			button.setVisible(Game.currentBattle.showBattleButtons);
			button = findButton(BATTLE_DIR_BUTTON_NW);
			button.setVisible(Game.currentBattle.showBattleButtons);
			button = findButton(BATTLE_DIR_BUTTON_N);
			button.setVisible(Game.currentBattle.showBattleButtons);
			button = findButton(BATTLE_DIR_BUTTON_NE);
			button.setVisible(Game.currentBattle.showBattleButtons);
			button = findButton(BATTLE_DIR_BUTTON_E);
			button.setVisible(Game.currentBattle.showBattleButtons);
			button = findButton(BATTLE_DIR_BUTTON_SE);
			button.setVisible(Game.currentBattle.showBattleButtons);
			button = findButton(BATTLE_DIR_BUTTON_S);
			button.setVisible(Game.currentBattle.showBattleButtons);
			button = findButton(BATTLE_DIR_BUTTON_SW);
			button.setVisible(Game.currentBattle.showBattleButtons);
			button = findButton(BATTLE_DIR_BUTTON_W);
			button.setVisible(Game.currentBattle.showBattleButtons);
			button = findButton(SKIP_ROUND);
			button.setVisible(Game.currentBattle.showBattleButtons);
			button = findButton(SHOOT_BUTTON_NAME);
			button.setVisible(Game.currentBattle.showBattleButtons);
			button = findButton(BATTLE_SPELL_BUTTON_NAME_1);
			button.setVisible(Game.currentBattle.showBattleButtons);
			button = findButton(BATTLE_SPELL_BUTTON_NAME_2);
			button.setVisible(Game.currentBattle.showBattleButtons);
			button = findButton(BATTLE_SPELL_BUTTON_NAME_3);
			button.setVisible(Game.currentBattle.showBattleButtons);
			button = findButton(BATTLE_SPELL_BUTTON_NAME_4);
			button.setVisible(Game.currentBattle.showBattleButtons);
			Gladiator gladiator = Game.currentBattle.getCurrentBattleGladiator();
			if(gladiator != null){
				button = findButton(SHOOT_BUTTON_NAME);
				if(gladiator.needsReload){
					button.setText("Lataa varsijousi");
				} else {
					button.setText("Ammu");
				}
				if(gladiator.items[Constants.RANGED_WEAPON_SLOT] != null){
					button.setVisible(true);
				} else {
					button.setVisible(false);
				}
				button = findButton(BATTLE_SPELL_BUTTON_NAME_1);
				if(gladiator.knownSpells[0] != null){
					button.setEnabled(true);
					button.setText(gladiator.knownSpells[0].name);
				} else {
					button.setEnabled(false);
					button.setText("-");
				}
				button = findButton(BATTLE_SPELL_BUTTON_NAME_2);
				if(gladiator.knownSpells[1] != null){
					button.setEnabled(true);
					button.setText(gladiator.knownSpells[1].name);
				} else {
					button.setEnabled(false);
					button.setText("-");
				}
				button = findButton(BATTLE_SPELL_BUTTON_NAME_3);
				if(gladiator.knownSpells[2] != null){
					button.setEnabled(true);
					button.setText(gladiator.knownSpells[2].name);
				} else {
					button.setEnabled(false);
					button.setText("-");
				}
				button = findButton(BATTLE_SPELL_BUTTON_NAME_4);
				if(gladiator.knownSpells[3] != null){
					button.setEnabled(true);
					button.setText(gladiator.knownSpells[3].name);
				} else {
					button.setEnabled(false);
					button.setText("-");
				}
				if(gladiator.getKnownSpells().size() == 0){
					button = findButton(BATTLE_SPELL_BUTTON_NAME_1);
					button.setVisible(false);
					button = findButton(BATTLE_SPELL_BUTTON_NAME_2);
					button.setVisible(false);
					button = findButton(BATTLE_SPELL_BUTTON_NAME_3);
					button.setVisible(false);
					button = findButton(BATTLE_SPELL_BUTTON_NAME_4);
					button.setVisible(false);
				} else {
					button = findButton(BATTLE_SPELL_BUTTON_NAME_1);
					button.setVisible(true);
					button = findButton(BATTLE_SPELL_BUTTON_NAME_2);
					button.setVisible(true);
					button = findButton(BATTLE_SPELL_BUTTON_NAME_3);
					button.setVisible(true);
					button = findButton(BATTLE_SPELL_BUTTON_NAME_4);
					button.setVisible(true);
				}
			}
			
		}
		
		if(Division.divisions.size() > 0) {
			JButton button = findButton(SHOW_OPPONENT_BUTTON_NAME);
			String s = "sarjaottelun";
			if(Game.monthType[Game.MONTH] == Game.MONTH_TYPE_CUP_BATTLE){
				s = "CUP-ottelu";
			}
			if(Game.getNextOpponentForTeam(Game.currentTeam) != null){
				button.setEnabled(true);
				button.setText("N�yt� "+s+" vastustajasi "+Game.getNextOpponentForTeam(Game.currentTeam).name);
			} else {
				button.setEnabled(false);
				if(Game.monthType[Game.MONTH] == Game.MONTH_TYPE_CUP_BATTLE){
					button.setText("Joukkueesi on tippunut CUPIsta");
				}
				else if(Game.monthType[Game.MONTH] == Game.MONTH_TYPE_WINTER_BREAK){
					button.setText("On talvitauko");
				}
			}
		}
		
		if(Game.currentGladiator != null){
			if(Game.currentGladiator.items[Constants.MELEE_WEAPON_SLOT] != null){
				JButton button = findButton(CURRENT_GLADIATOR_MELEE_WEAPON_BUTTON_NAME);
				button.setText(Game.currentGladiator.items[Constants.MELEE_WEAPON_SLOT].name);
			} else {
				JButton button = findButton(CURRENT_GLADIATOR_MELEE_WEAPON_BUTTON_NAME);
				button.setText("Nyrkki");
			}
			if(Game.currentGladiator.items[Constants.RANGED_WEAPON_SLOT] != null){
				JButton button = findButton(CURRENT_GLADIATOR_RANGED_WEAPON_BUTTON_NAME);
				button.setEnabled(true);
				button.setText(Game.currentGladiator.items[Constants.RANGED_WEAPON_SLOT].name+" ("+Game.currentGladiator.ammo+")");
			} else {
				JButton button = findButton(CURRENT_GLADIATOR_RANGED_WEAPON_BUTTON_NAME);
				button.setEnabled(false);
				button.setText("Ei heittoasetta");
			}
			if(Game.currentGladiator.items[Constants.ARMOUR_SLOT] != null){
				JButton button = findButton(CURRENT_GLADIATOR_ARMOR_BUTTON_NAME);
				button.setEnabled(true);
				button.setText(Game.currentGladiator.items[Constants.ARMOUR_SLOT].name);
			} else {
				JButton button = findButton(CURRENT_GLADIATOR_ARMOR_BUTTON_NAME);
				button.setEnabled(false);
				button.setText("Vaatteet");
			}
			if(Game.currentGladiator.knownSpells[0] != null){
				JButton button = findButton(CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_1);
				button.setEnabled(true);
				button.setText(Game.currentGladiator.knownSpells[0].name+" ("+Game.currentGladiator.spellAtk[0]+")");
			} else {
				JButton button = findButton(CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_1);
				button.setEnabled(false);
				button.setText("-");
			}
			if(Game.currentGladiator.knownSpells[1] != null){
				JButton button = findButton(CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_2);
				button.setEnabled(true);
				button.setText(Game.currentGladiator.knownSpells[1].name+" ("+Game.currentGladiator.spellAtk[1]+")");
			} else {
				JButton button = findButton(CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_2);
				button.setEnabled(false);
				button.setText("-");
			}
			if(Game.currentGladiator.knownSpells[2] != null){
				JButton button = findButton(CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_3);
				button.setEnabled(true);
				button.setText(Game.currentGladiator.knownSpells[2].name+" ("+Game.currentGladiator.spellAtk[2]+")");
			} else {
				JButton button = findButton(CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_3);
				button.setEnabled(false);
				button.setText("-");
			}
			if(Game.currentGladiator.knownSpells[3] != null){
				JButton button = findButton(CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_4);
				button.setEnabled(true);
				button.setText(Game.currentGladiator.knownSpells[3].name+" ("+Game.currentGladiator.spellAtk[3]+")");
			} else {
				JButton button = findButton(CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_4);
				button.setEnabled(false);
				button.setText("-");
			}
			if(Game.currentGladiator.str < Util.roll(Game.currentGladiator.race.str, "max")){
				findButton(TRAIN_STR_TXT).setEnabled(true);
			} else {
				findButton(TRAIN_STR_TXT).setEnabled(false);
			}
			if(Game.currentGladiator.speed < Util.roll(Game.currentGladiator.race.speed, "max")){
				findButton(TRAIN_SPEED_TXT).setEnabled(true);
			} else {
				findButton(TRAIN_SPEED_TXT).setEnabled(false);
			}
			if(Game.currentGladiator.maxMana < Util.roll(Game.currentGladiator.race.mana, "max")){
				findButton(TRAIN_MANA_TXT).setEnabled(true);
			} else {
				findButton(TRAIN_MANA_TXT).setEnabled(false);
			}
			if(Game.currentGladiator.maxHp < Util.roll(Game.currentGladiator.race.health, "max")){
				findButton(TRAIN_HP_TXT).setEnabled(true);
			} else {
				findButton(TRAIN_HP_TXT).setEnabled(false);
			}
		}
		if (GAME_WINDOW_CURRENT != GAME_WINDOW_TITLESCREEN && GAME_WINDOW_CURRENT != GAME_WINDOW_RECRUIT && GAME_WINDOW_CURRENT != GAME_WINDOW_OTHER_TEAMS) {
			if(Game.currentTeam.gladiators.size() == 0){
				JButton button = findButton(CURRENT_GLADIATOR_MELEE_WEAPON_BUTTON_NAME);
				button.setVisible(false);
				button = findButton(CURRENT_GLADIATOR_RANGED_WEAPON_BUTTON_NAME);
				button.setVisible(false);
				button = findButton(CURRENT_GLADIATOR_ARMOR_BUTTON_NAME);
				button.setVisible(false);
				button = findButton(CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_1);
				button.setVisible(false);
				button = findButton(CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_2);
				button.setVisible(false);
				button = findButton(CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_3);
				button.setVisible(false);
				button = findButton(CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_4);
				button.setVisible(false);
				button = findButton(CURRENT_GLADIATOR_SPELL_EFFECTS_BUTTON_NAME);
				button.setVisible(false);
			}
		}
	}
	
	Jframe mainFrame;

	public ArrayList<Button> buttons = new ArrayList<Button>();
	public static String[] MESSAGES = new String[]{"Rotujen jumalien v�lisess� valtataistelussa voitokas rotu on MUUMIO.",
	"",
	"Rodun edustajien tekem�t vahingot ja loitsujen vaikutukset ovat t�ss� kuussa kaksinkertaiset."};
	public static String EMPTY_SLOT_TXT = "Tyhj� paikka";
	public static String CLEAR_FORMATION_TXT = "<html><div text-align:center>Tyhjenn�<br>muodostelma</div></html>";
	public static String CLEAR_FORMATION_BUTTON_NAME = "Clear formation";
	public static String AUTO_FORMATION_TXT = "<html><div text-align:center>Tee muodostelma<br>automaattisesti</div></html>";
	public static String AUTO_FORMATION_BUTTON_NAME = "Auto formation";
	public static String SHOW_OPPONENT_TEAM_TXT = "N�yt� vastustajan joukkue";
	public static String BACK_TO_MAIN_MENU_TXT = "Takaisin P��valikkoon";
	public static String BEGIN_FIGHT_TXT = "Taistelu Alkakoon!";
	
	public static String CONFIRM_BATTLE_TXT = "OK";
	public static String OK_BATTLE_BUTTON_NAME = "Confirm battle";
	public static String BATTLE_TEAM_1_TXT = "Tomunpurijat";
	public static String BATTLE_TEAM_1_BUTTON_NAME = "Battle team 1";
	public static String BATTLE_TEAM_2_TXT = "Gladiaattorit Oy";
	public static String BATTLE_TEAM_2_BUTTON_NAME = "Battle team 2";
	public static String AUTO_ROUND = "Pikakierros";
	public static String BATTLE_DIR_BUTTON_NW = "Battle dir NW";
	public static String BATTLE_DIR_BUTTON_N = "Battle dir N";
	public static String BATTLE_DIR_BUTTON_NE = "Battle dir NE";
	public static String BATTLE_DIR_BUTTON_W = "Battle dir W";
	public static String BATTLE_DIR_BUTTON_E = "Battle dir E";
	public static String BATTLE_DIR_BUTTON_SW = "Battle dir SW";
	public static String BATTLE_DIR_BUTTON_S = "Battle dir S";
	public static String BATTLE_DIR_BUTTON_SE = "Battle dir SE";
	public static String SKIP_ROUND = "Pass";
	
	public static String OFFER_TXT = "Tee 1. tarjouksesi";
	public static String OFFER_BUTTON_NAME = "Offer";
	
	public static String SKIP_OFFER_TXT = "J�t� tarjousvuoro v�liin";
	
	public static String EXIT_RECRUIT_TXT = "Pois";
	
	public static String EXIT_OTHER_TEAM_TXT = "Pois";
	public static String EXIT_OTHER_TEAM_BUTTON_NAME = "Exit other team";
	
	public static String SHOW_OTHER_TEAM_TXT = "N�yt� joukkue";
	
	public static String BUY_SPELL_TXT = "Osta valittu loitsu";
	public static String EXIT_MAGE_TXT = "Pois";
	public static String EXIT_MAGE_BUTTON_NAME = "Exit mage";
	
	public static String SPEAR_TXT = "Keih�s";
	public static String SWORD_TXT = "Miekka";
	public static String AXE_TXT = "Kirves";
	public static String MACE_TXT = "Nuija";
	public static String BOW_TXT = "Jousi";
	public static String CROSSBOW_TXT = "Varsijousi";
	public static String ARMOUR_TXT = "Panssarit";
	public static String DISCOUNT_TXT = "Tarjoukset";
	public static String BUY_ITEM_TXT = "Osta valittu tavara";
	public static String EXIT_SHOP_TXT = "Poistu kaupasta";
	
	public static String TRAIN_STR_TXT = "Bodaa voimaa.";
	public static String TRAIN_SPEED_TXT = "Treenaa nopeutta.";
	public static String TRAIN_MANA_TXT = "Mietiskele manaa.";
	public static String TRAIN_HP_TXT = "Koettele kestoa.";
	public static String EXIT_SCHOOL_TXT = "Poistu koulusta.";
	
	public static String START_FIGHT_TXT = "Aloita Taistelut";
	public static String CHANGE_PLAYER_TXT = "Seuraava pelaaja / Aloita Taistelut";
	public static String SHOP_TXT = "Kauppa";
	public static String RECRUIT_TXT = "V�rv�ys (1/3)";
	public static String RECRUIT_BUTTON_NAME = "Recruit";
	public static String MAGE_SHOP_TXT = "Maagi";
	public static String TRAINING_TXT = "Koulu";
	public static String MATCH_SCHELUDE_TXT = "Vuoden otteluohjelma";
	public static String STANDINGS_TXT = "Sarjataulukot";
	public static String CUP_STANDINGS_TXT = "CUP-taulukko";
	public static String OWN_TEAM_TXT = "Oma joukkue";
	public static String OTHER_TEAMS_TXT = "Muut joukkueet";
	public static String SHOW_OPPONENT_BUTTON_NAME = "Show opponent";
	public static String SHOW_OPPONENT_TXT = "N�yt� sarjaottelun vastustajasi Tomunpurijat";
	
	public static String CHANGE_TO_P1_TXT = "P1 Team";
	public static String CHANGE_TO_P1_BUTTON_NAME = "Change to P1";
	public static String CHANGE_TO_P2_TXT = "P2 Team";
	public static String CHANGE_TO_P2_BUTTON_NAME = "Change to P2";
	public static String CHANGE_TO_P3_TXT = "P3 Team";
	public static String CHANGE_TO_P3_BUTTON_NAME = "Change to P3";
	public static String CHANGE_TO_P4_TXT = "P4 Team";
	public static String CHANGE_TO_P4_BUTTON_NAME = "Change to P4";
	
	public static String SHOOT_TXT = "Ammu";
	public static String SHOOT_BUTTON_NAME = "Shoot / Reload";
	
	public static String BATTLE_SPELL_TXT_1 = "-";
	public static String BATTLE_SPELL_TXT_2 = "-";
	public static String BATTLE_SPELL_TXT_3 = "-";
	public static String BATTLE_SPELL_TXT_4 = "-";
	public static String BATTLE_SPELL_BUTTON_NAME_1 = "Battle spell 1";
	public static String BATTLE_SPELL_BUTTON_NAME_2 = "Battle spell 2";
	public static String BATTLE_SPELL_BUTTON_NAME_3 = "Battle spell 3";
	public static String BATTLE_SPELL_BUTTON_NAME_4 = "Battle spell 4";
	
	public static String PLAYER_CHANGE_START_FIGHT_TXT = "ALOITA TAISTELUT";
	public static String PLAYER_CHANGE_START_FIGHT_BUTTON_NAME = "Player change start fights";
	
	public static String CURRENT_GLADIATOR_MELEE_WEAPON_TXT = "Nyrkki";
	public static String CURRENT_GLADIATOR_RANGED_WEAPON_TXT = "Ei heittoasetta";
	public static String CURRENT_GLADIATOR_ARMOR_TXT = "Vaatteet";
	
	public static String START_FIGHT_CHANGE_PLAYER_BUTTON_NAME = "start fight / change player";
	
	public static String CURRENT_GLADIATOR_MELEE_WEAPON_BUTTON_NAME = "Melee weapon";
	public static String CURRENT_GLADIATOR_RANGED_WEAPON_BUTTON_NAME = "Ranged weapon";
	public static String CURRENT_GLADIATOR_ARMOR_BUTTON_NAME = "Armor";
	
	public static String CURRENT_GLADIATOR_KNOWN_SPELLS_TXT_1 = "-";
	public static String CURRENT_GLADIATOR_KNOWN_SPELLS_TXT_2 = "-";
	public static String CURRENT_GLADIATOR_KNOWN_SPELLS_TXT_3 = "-";
	public static String CURRENT_GLADIATOR_KNOWN_SPELLS_TXT_4 = "-";
	
	public static String CURRENT_GLADIATOR_SPELL_EFFECTS_TXT = "Gladiaattoriin vaikuttavat loitsut (Ei ole)";
	
	public static String CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_1 = "Known spell 1";
	public static String CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_2 = "Known spell 2";
	public static String CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_3 = "Known spell 3";
	public static String CURRENT_GLADIATOR_KNOWN_SPELLS_BUTTON_NAME_4 = "Known spell 4";
	
	public static String CURRENT_GLADIATOR_SPELL_EFFECTS_BUTTON_NAME = "Spell effects";
	
	public JButton button(int windowName, String s, Font f, int x, int y, int width, int height){
		return button(windowName, s, s, f, x, y, width, height);
	}
	
	public JButton button(int windowName, String buttonName, String s, Font f, int x, int y, int width, int height){
		JButton jButton = new JButton(s);
		jButton.setForeground(Color.BLACK);
		jButton.setFont(f);
		jButton.setBounds(x, y, width, height);
		jButton.setMargin(new Insets(0, 0, 0, 0));
		jButton.setVisible(true);
		jButton.setActionCommand(buttonName);
		jButton.addActionListener(this.mainFrame);
		for(Button button : buttons){
			if(button.button_name.equals(buttonName)){
				System.out.println("[ERROR]: Button with same name already exists! "+buttonName);
			}
		}
		buttons.add(new Button(new int[]{windowName}, buttonName, jButton));
		return jButton;
	}
	
	public JButton button(int[] windowName, String s, Font f, int x, int y, int width, int height){
		return button(windowName, s, s, f, x, y, width, height);
	}
	
	public JButton button(int[] windowName, String buttonName, String s, Font f, int x, int y, int width, int height){
		JButton jButton = new JButton(s);
		jButton.setForeground(Color.BLACK);
		jButton.setFont(f);
		jButton.setBounds(x, y, width, height);
		jButton.setMargin(new Insets(0, 0, 0, 0));
		jButton.setVisible(true);
		jButton.setActionCommand(buttonName);
		jButton.addActionListener(this.mainFrame);
		for(Button button : buttons){
			if(button.button_name.equals(buttonName)){
				System.out.println("[ERROR]: Button with same name already exists! "+buttonName);
			}
		}
		buttons.add(new Button(windowName, buttonName, jButton));
		return jButton;
	}
	
	public JButton titlebutton(int windowName, String s, Font f, int x, int y, int width, int height){
		return titlebutton(windowName, s, s, f, x, y, width, height);
	}
	
	public JButton titlebutton(int windowName, String buttonName, String s, Font f, int x, int y, int width, int height){
		JButton jButton = new JButton(s);
		jButton.setForeground(Color.WHITE);
		jButton.setBackground(GAME_NAME_OUTLINE_COLOR);
		jButton.setFont(f);
		jButton.setBounds(x, y, width, height);
		jButton.setMargin(new Insets(0, 0, 0, 0));
		jButton.setVisible(true);
		jButton.setActionCommand(buttonName);
		jButton.addActionListener(this.mainFrame);
		for(Button button : buttons){
			if(button.button_name.equals(buttonName)){
				System.out.println("[ERROR]: Button with same name already exists! "+buttonName);
			}
		}
		buttons.add(new Button(new int[]{windowName}, buttonName, jButton));
		return jButton;
	}
	
	public JButton findButton(String s){
		for(Button button : buttons){
			if(button.button_name.toLowerCase().equals(s.toLowerCase())){
				return button.jButton;
			}
		}
		return null;
	}
	
	public DefaultTableModel findTableModel(String s){
		for(Table table : tables){
			if(table.table_name.toLowerCase().equals(s.toLowerCase())){
				return table.tableModel;
			}
		}
		return null;
	}
	
	public JTable findTable(String s){
		for(Table table : tables){
			if(table.table_name.toLowerCase().equals(s.toLowerCase())){
				return table.jTable;
			}
		}
		return null;
	}

	public static void setMessage(String... message){
		MESSAGES = message;
	}
	
}
