import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JPanel;

public class StandingsPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	//COLORS
	public static Color BACKGROUND_COLOR = new Color(224, 224, 224);
	
	//FONTS
	public static Font PANEL_FONT = new Font("courier new", Font.PLAIN, 12);
	public static Font BUTTON_FONT = new Font("Segoe UI", Font.BOLD, 12);
	
	public static int GAME_WINDOW_STANDINGS_VIEW = 11;
	
	public StandingsPanel(Jframe frame){
		this.mainFrame = frame;
		this.setLayout(null);
		this.add(button(GAME_WINDOW_STANDINGS_VIEW, "close standings view", "OK", BUTTON_FONT, 8, 432, 777, 105));
	}
	
	public void paintComponent (Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
	    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    g2d.setColor(BACKGROUND_COLOR);
	    g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
	    g2d.setColor(Color.BLACK);
	    g2d.setFont(PANEL_FONT);
	    int divisionOffY = 102;
	    int teamOffY = 17;
	    for(int i = 0; i < Division.divisions.size(); i++){
	    	Division division = Division.divisions.get(i);
	    	g2d.drawString("-- "+(i+1)+". DIVISIOONA --", 306, 29+(i*divisionOffY));
	    	ArrayList<Team> teams = division.teams;
	    	Util.organizeTeams(teams);
	    	for(int j = 0; j < teams.size(); j++){
	    		Team team = teams.get(j);
	    		g2d.drawString(team.name, 306, 46+(i*divisionOffY)+(j*teamOffY));
	    		g2d.drawString(""+team.wins, 468, 46+(i*divisionOffY)+(j*teamOffY));
	    	}
	    }
	}
	
	public void refresh(){
		this.revalidate();
		this.repaint();
	}
	
	public ArrayList<Button> buttons = new ArrayList<Button>();
	
	public JButton button(int windowName, String s, Font f, int x, int y, int width, int height){
		return button(windowName, s, s, f, x, y, width, height);
	}
	
	public JButton button(int windowName, String buttonName, String s, Font f, int x, int y, int width, int height){
		JButton jButton = new JButton(s);
		jButton.setForeground(Color.BLACK);
		jButton.setFont(f);
		jButton.setBounds(x, y, width, height);
		jButton.setMargin(new Insets(0, 0, 0, 0));
		jButton.setVisible(true);
		jButton.setActionCommand(buttonName);
		jButton.addActionListener(this.mainFrame);
		for(Button button : buttons){
			if(button.button_name.equals(buttonName)){
				System.out.println("[ERROR]: Button with same name already exists! "+buttonName);
			}
		}
		buttons.add(new Button(new int[]{windowName}, buttonName, jButton));
		return jButton;
	}
	
	public JButton button(int[] windowName, String s, Font f, int x, int y, int width, int height){
		return button(windowName, s, s, f, x, y, width, height);
	}
	
	public JButton button(int[] windowName, String buttonName, String s, Font f, int x, int y, int width, int height){
		JButton jButton = new JButton(s);
		jButton.setForeground(Color.BLACK);
		jButton.setFont(f);
		jButton.setBounds(x, y, width, height);
		jButton.setMargin(new Insets(0, 0, 0, 0));
		jButton.setVisible(true);
		jButton.setActionCommand(buttonName);
		jButton.addActionListener(this.mainFrame);
		for(Button button : buttons){
			if(button.button_name.equals(buttonName)){
				System.out.println("[ERROR]: Button with same name already exists! "+buttonName);
			}
		}
		buttons.add(new Button(windowName, buttonName, jButton));
		return jButton;
	}
	
	public JButton findButton(String s){
		for(Button button : buttons){
			if(button.button_name.toLowerCase().equals(s.toLowerCase())){
				return button.jButton;
			}
		}
		return null;
	}
	
	Jframe mainFrame;
	
}
