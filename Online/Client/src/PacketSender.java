

import java.io.DataOutputStream;
import java.io.IOException;

public class PacketSender {
	
	private DataOutputStream out;
	private Client client;

	public PacketSender(Client client) {
		this.client = client;
		this.out = this.client.out;
	}

	public void sendTeamNamePacket(String name) {
		int packetId = 0;
		try {
			out.writeByte(packetId);
			out.writeUTF(name);
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendStartGamePacket() {
		int packetId = 1;
		try {
			out.writeByte(packetId);
			boolean b = client.gamePanel.mainFrame.newGamePanel.findCheckBox(NewGamePanel.AUTOMATIC_START_TEAMS).isSelected() == true;
			out.writeBoolean(b);
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendOfferRecruitPacket(int idx, int amount) {
		int packetId = 2;
		try {
			out.writeByte(packetId);
			out.writeByte(idx);
			out.writeInt(amount);
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendBuyItemPacket(Item item) {
		int packetId = 3;
		try {
			out.writeByte(packetId);
			out.writeByte(Game.ownTeam.gladiators.indexOf(Game.currentGladiator));
			out.writeUTF(item.name);
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendBuySpellPacket(Spell spell) {
		int packetId = 4;
		try {
			out.writeByte(packetId);
			out.writeByte(Game.ownTeam.gladiators.indexOf(Game.currentGladiator));
			out.writeUTF(spell.name);
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendSellItemPacket(int slot) {
		int packetId = 5;
		try {
			out.writeByte(packetId);
			out.writeByte(Game.ownTeam.gladiators.indexOf(Game.currentGladiator));
			out.writeByte(slot);
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendTrainPacket(String s) {
		int packetId = 6;
		try {
			out.writeByte(packetId);
			out.writeByte(Game.ownTeam.gladiators.indexOf(Game.currentGladiator));
			out.writeUTF(s);
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendSkipRecruitPacket() {
		int packetId = 7;
		try {
			out.writeByte(packetId);
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendBattleReadyPacket() {
		int packetId = 8;
		try {
			out.writeByte(packetId);
			
			for(int i = 0; i < Game.currentTeam.battleGladiators.length; i++){
				Gladiator gladiator = Game.currentTeam.battleGladiators[i];
				int idx = -1;
				if(gladiator != null){
					idx = Game.ownTeam.gladiators.indexOf(gladiator);
				}
				out.writeByte(idx);
			}
			
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendBattleStartPacket() {
		int packetId = 9;
		try {
			out.writeByte(packetId);
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendBattleMovementPacket(int movementType) {
		int packetId = 10;
		try {
			out.writeByte(packetId);
			out.writeByte(movementType);
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendRequestNewMonthDataPacket() {
		int packetId = 11;
		try {
			out.writeByte(packetId);
			out.flush();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
}
