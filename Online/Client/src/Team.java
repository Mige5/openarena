import java.util.ArrayList;

public class Team {

	String name;
	int money;
	//boolean playerControlled;
	int rating;
	int arenaPoints;
	int wins;
	int division;
	Player controllingPlayer = null;
	boolean addedToBattle = false;
	int currentMonthMoneyBalance = 0;
	int currentMonthIncome = 0;
	
	int[] battleResult = new int[12];
	
	public ArrayList<Gladiator> gladiators = new ArrayList<Gladiator>();
	public Gladiator[] battleGladiators = new Gladiator[Constants.MAX_GLADIATORS_IN_BATTLE];
	
	public Team(String name){
		this.name = name;
	}
	
	public Team(String name, Player controllingPlayer){
		this.name = name;
		this.controllingPlayer = controllingPlayer;
	}
	
	public Team(){
		
	}
	
	public void modifyMoney(int mod){
		this.money += mod;
		this.currentMonthMoneyBalance += mod;
	}
	
	public boolean droppedFromCUP(){
		if(battleResult[2] == Battle.LOSE){
			return true;
		}
		if(battleResult[4] == Battle.LOSE){
			return true;
		}
		if(battleResult[6] == Battle.LOSE){
			return true;
		}
		if(battleResult[8] == Battle.LOSE){
			return true;
		}
		return false;
	}
	
	public boolean battleTeamContainsGladiator(Gladiator gladiator){
		for(int i = 0; i < battleGladiators.length; i++){
			if(battleGladiators[i] != null){
				if(battleGladiators[i] == gladiator)
					return true;
			}
		}
		return false;
	}
	
	public int getBattleTeamSize(){
		int count = 0;
		for(int i = 0; i < battleGladiators.length; i++){
			if(battleGladiators[i] != null){
				count++;
			}
		}
		return count;
	}
	
	public int battleTeamGetNextEmptySlot(){
		int idx = -1;
		for(int i = 0; i < battleGladiators.length; i++){
			Gladiator gladiator = battleGladiators[i];
			if(gladiator == null)
				return i;
		}
		return idx;
	}
	
	public ArrayList<Race> getGladiatorRaces(){
		ArrayList<Race> races = new ArrayList<Race>();
		for(Gladiator gladiator : gladiators){
			races.add(gladiator.race);
		}
		return races;
	}
	
	public int getHealthyGladiatorCount(){
		int count = 0;
		for(Gladiator gladiator : gladiators){
			if(gladiator.hurt == 0){
				count++;
			}
		}
		return count;
	}
	
	public int getToughness(){//TODO:
		return 100;
	}
	
}
