import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Battle {
	
	int round;
	Map map;
	Team topTeam;
	Team bottomTeam;
	Team winningTeam;
	Team losingTeam;
	boolean isAIonlyBattle;
	public ArrayList<Gladiator> gladiatorBattleTurnOrder = new ArrayList<Gladiator>();
	public int currentBattleGladiatorIdx = 0;
	public boolean showBattleButtons = false;
	public boolean battleStarted = false;
	
	public static int WIN = 1;
	public static int LOSE = 2;
	
	public Battle(Team topTeam, Team bottomTeam){
		this.topTeam = topTeam;
		this.bottomTeam = bottomTeam;
		if(topTeam.controllingPlayer == null && bottomTeam.controllingPlayer == null){
			isAIonlyBattle = true;
		} else {
			isAIonlyBattle = false;
		}
		map = new Map();
		createClipMapData();
	}
	
	public static Color CLIPPED_TILE_COLOR = new Color(237, 28, 36);
	
	public void createClipMapData(){
		BufferedImage image = ImageHandler.toBufferedImage(ImageHandler.clipmapImages[Game.BATTLE_MAP]);
        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
            	Color c = new Color(image.getRGB(x, y));
            	if(c.getRGB() == CLIPPED_TILE_COLOR.getRGB()){
            		map.mapTiles[x][y].clipped = true;
            	}
            }
        }
	}

	public boolean moveGladiator(String dir){
		int move_X = 0;
		int move_Y = 0;
		if(dir.equals("NW")){
			move_X = -1;
			move_Y = -1;
		}
		if(dir.equals("N")){
			move_Y = -1;
		}
		if(dir.equals("NE")){
			move_X = 1;
			move_Y = -1;
		}
		if(dir.equals("E")){
			move_X = 1;
		}
		if(dir.equals("SE")){
			move_X = 1;
			move_Y = 1;
		}
		if(dir.equals("S")){
			move_Y = 1;
		}
		if(dir.equals("SW")){
			move_X = -1;
			move_Y = 1;
		}
		if(dir.equals("W")){
			move_X = -1;
		}
		Gladiator gladiator = Game.currentBattle.getCurrentBattleGladiator();
		int cur_X = gladiator.pos.getX();
		int cur_Y = gladiator.pos.getY();
		if(cur_X <= 0 && move_X < 0)
			return false;
		if(cur_Y <= 0 && move_Y < 0)
			return false;
		if(cur_X >= Constants.MAP_WIDTH-1 && move_X > 0)
			return false;
		if(cur_Y >= Constants.MAP_HEIGHT-1 && move_Y > 0)
			return false;
		int new_X = cur_X + move_X;
		int new_Y = cur_Y + move_Y;
		if(map.mapTiles[new_X][new_Y].clipped)
			return false;
		this.currentBattleGladiatorIdx++;
		Game.currentBattle.resetDmgHitmarks();
		Gladiator otherGladiator = map.mapTiles[new_X][new_Y].gladiatorOnTile;
		if(otherGladiator != null){
			if(otherGladiator.owner == gladiator.owner || otherGladiator.hp <= 0){
				otherGladiator.pos = new Position(cur_X, cur_Y);
			} else {
				Game.meleeAtk(gladiator, otherGladiator);
				return true;
			}
		}
		map.mapTiles[cur_X][cur_Y].gladiatorOnTile = otherGladiator;
		map.mapTiles[new_X][new_Y].gladiatorOnTile = gladiator;
		gladiator.pos = new Position(new_X, new_Y);
		return true;
	}

	public void reloadCrossbow(){
		Gladiator gladiator = Game.currentBattle.getCurrentBattleGladiator();
		if(gladiator.needsReload){
			gladiator.needsReload = false;
			this.currentBattleGladiatorIdx++;
			Game.currentBattle.startNewBattleTurn();
			GamePanel.setMessage(gladiator.name+" latasi varsijousensa.");
		}
	}

	public boolean doSpellCast(int idx){
		Gladiator gladiator = Game.currentBattle.getCurrentBattleGladiator();
		Game.currentShootSpell = gladiator.knownSpells[idx];
		if(gladiator.mana < Game.currentShootSpell.manaCost){
			Game.currentShootSpell = null;
			return false;
		}
		if(Game.currentShootSpell.isFriendlySpell()){
			int atkVal = gladiator.getAtkSkillValueForSpell(Game.currentShootSpell);
			if(!Game.successfullFriendlySpellCast(gladiator, Game.currentShootSpell)){
				gladiator.mana -= Game.currentShootSpell.manaCost;
				GamePanel.setMessage(Game.currentBattle.getCurrentBattleGladiator().name+" heitt�� loitsua nimelt� "+Game.currentShootSpell.name+".",
									"Loitsu ("+atkVal+") ep�onnistui.");
				Game.currentShootSpell = null;
				this.currentBattleGladiatorIdx++;
				Game.currentBattle.startNewBattleTurn();
				return false;
			}
		}
		return true;
	}

	public void resetDmgHitmarks(){
		Battle battle = this;
		Team team1 = battle.bottomTeam;
		Team team2 = battle.topTeam;
		for(int i = 0; i < Constants.MAX_GLADIATORS_IN_BATTLE; i++){
			if(team1.battleGladiators[i] == null)
				continue;
			team1.battleGladiators[i].receivedDmg = -1;
		}
		for(int i = 0; i < Constants.MAX_GLADIATORS_IN_BATTLE; i++){
			if(team2.battleGladiators[i] == null)
				continue;
			team2.battleGladiators[i].receivedDmg = -1;
		}
	}

	public void confirmBattle(){
		this.showBattleButtons = true;
		this.battleStarted = true;
		this.currentBattleGladiatorIdx = 0;
		Game.currentBattle.startNewBattleTurn();
		Game.updateButtons();
	}
	
	public int getBaseIncome(){
		ArrayList<Gladiator> bottomTeamGladiators = new ArrayList<Gladiator>();
		ArrayList<Gladiator> topTeamGladiators = new ArrayList<Gladiator>();
		for(int i = 0; i < Constants.MAX_GLADIATORS_IN_BATTLE; i++){
			if(bottomTeam.battleGladiators[i] == null)
    			continue;
    		bottomTeamGladiators.add(bottomTeam.battleGladiators[i]);
		}
		for(int i = 0; i < Constants.MAX_GLADIATORS_IN_BATTLE; i++){
			if(topTeam.battleGladiators[i] == null)
    			continue;
    		topTeamGladiators.add(topTeam.battleGladiators[i]);
		}
		return (bottomTeamGladiators.size() + topTeamGladiators.size()) * 25;//TODO: figure out how this should work
	}
	
	public int getWinBonus(){
		return getBaseIncome()/2;//TODO: figure out how this should work
	}
	
	public void startNewBattleTurn(){
		showBattleButtons = true;
		ArrayList<Gladiator> aliveTopTeamGladiators = new ArrayList<Gladiator>();
		ArrayList<Gladiator> aliveBottomTeamGladiators = new ArrayList<Gladiator>();
		
		for(Gladiator gladiator : topTeam.battleGladiators){
			if(gladiator == null)
				continue;
			if(gladiator.hp <= 0)
				continue;
			aliveTopTeamGladiators.add(gladiator);
		}
		for(Gladiator gladiator : bottomTeam.battleGladiators){
			if(gladiator == null)
				continue;
			if(gladiator.hp <= 0)
				continue;
			aliveBottomTeamGladiators.add(gladiator);
		}
		if(aliveTopTeamGladiators.size() == 0 || aliveBottomTeamGladiators.size() == 0 || round >= Constants.MAX_BATTLE_ROUNDS-1){
			Game.endBattle(this);
			return;
		}
		
		Gladiator gladiator = null;
		gladiator = Game.currentBattle.getCurrentBattleGladiator();
		if(gladiator.owner.controllingPlayer == null){
			Game.currentBattle.playAIBattleTurn(gladiator);
			return;
		}
		Game.currentTeam = gladiator.owner;
		Game.currentGladiator = gladiator;
		Game.updateButtons();
	}

	public Gladiator getCurrentBattleGladiator(){
		Gladiator gladiator = null;
		boolean deadGladiator = true;
		while (deadGladiator) {
			if(this.currentBattleGladiatorIdx >= gladiatorBattleTurnOrder.size()){
				this.currentBattleGladiatorIdx = 0;
				round++;
			}
			gladiator = gladiatorBattleTurnOrder.get(this.currentBattleGladiatorIdx);
			if(gladiator.hp > 0){
				deadGladiator = false;
			} else {
				this.currentBattleGladiatorIdx++;
			}
		}
		return gladiator;
	}

	public void playAIBattleTurn(Gladiator gladiator){
		Team opponentTeam = null;
		if(gladiator.owner == bottomTeam){
			opponentTeam = topTeam;
		} else {
			opponentTeam = bottomTeam;
		}
		ArrayList<Gladiator> opponentGladiators = new ArrayList<Gladiator>();
		for(Gladiator op_glad : opponentTeam.battleGladiators){
			if(op_glad == null)
				continue;
			if(op_glad.hp <= 0)
				continue;
			opponentGladiators.add(op_glad);
		}
		int maxDist = 99;
		ArrayList<Gladiator> nearestOpponentGladiators = new ArrayList<Gladiator>();
		for(Gladiator op_glad : opponentGladiators){
			int dist = Util.getDistance(gladiator.pos, op_glad.pos);
			if(dist < maxDist){
				nearestOpponentGladiators.clear();
				maxDist = dist;
				nearestOpponentGladiators.add(op_glad);
			}
			else if(dist == maxDist){
				nearestOpponentGladiators.add(op_glad);
			}
		}
		Gladiator targetOpponent = null;
		if(gladiator.getRangedWeapon() == null || maxDist == 1){
			if(nearestOpponentGladiators.size() > 1){
				int maxHP = 99;
				ArrayList<Gladiator> lowestHPOpponentGladiators = new ArrayList<Gladiator>();
				for(Gladiator op_glad : nearestOpponentGladiators){
					int hp = op_glad.hp;
					if(hp < maxHP){
						lowestHPOpponentGladiators.clear();
						maxHP = hp;
						lowestHPOpponentGladiators.add(op_glad);
					}
					else if(hp == maxHP){
						lowestHPOpponentGladiators.add(op_glad);
					}
				}
				if(lowestHPOpponentGladiators.size() > 1){
					targetOpponent = lowestHPOpponentGladiators.get(Util.random_(lowestHPOpponentGladiators.size()));
				} else {
					targetOpponent = lowestHPOpponentGladiators.get(0);
				}
			} else if(nearestOpponentGladiators.size() == 1) {
				targetOpponent = nearestOpponentGladiators.get(0);
			}
			if(targetOpponent != null){
				String dir = Game.currentBattle.getMovementTowardsPosition(gladiator, targetOpponent.pos);
				if(dir != null)
					Game.currentBattle.moveGladiatorAI(gladiator, dir);
			}
		}
		this.currentBattleGladiatorIdx++;
		if(showBattleButtons)
			startNewBattleTurn();
	}

	public String getMovementTowardsPosition(Gladiator gladiator, Position to){
		Position from = gladiator.pos;
		String selectedMove = null;
		int move_X = 0;
		int move_Y = 0;
		if(to.getY() < from.getY()){
			move_Y = -1;
		}
		if(to.getY() > from.getY()){
			move_Y = 1;
		}
		if(to.getX() < from.getX()){
			move_X = -1;
		}
		if(to.getX() > from.getX()){
			move_X = 1;
		}
		ArrayList<String> allMoves = new ArrayList<String>();
		allMoves.add("NW");
		allMoves.add("N");
		allMoves.add("NE");
		allMoves.add("E");
		allMoves.add("SE");
		allMoves.add("S");
		allMoves.add("SW");
		allMoves.add("W");
		ArrayList<String> allMovesTowardsPosition = new ArrayList<String>();
		if(move_X == -1 && move_Y == -1){
			allMovesTowardsPosition.add("NW");
			allMovesTowardsPosition.add("N");
			allMovesTowardsPosition.add("W");
		}
		if(move_X == 1 && move_Y == 1){
			allMovesTowardsPosition.add("SE");
			allMovesTowardsPosition.add("S");
			allMovesTowardsPosition.add("E");
		}
		if(move_X == 0 && move_Y == -1){
			allMovesTowardsPosition.add("N");
		}
		if(move_X == 1 && move_Y == -1){
			allMovesTowardsPosition.add("NE");
			allMovesTowardsPosition.add("E");
			allMovesTowardsPosition.add("N");
		}
		if(move_X == 1 && move_Y == 0){
			allMovesTowardsPosition.add("E");
		}
		if(move_X == 0 && move_Y == 1){
			allMovesTowardsPosition.add("S");
		}
		if(move_X == -1 && move_Y == 1){
			allMovesTowardsPosition.add("SW");
			allMovesTowardsPosition.add("S");
			allMovesTowardsPosition.add("W");
		}
		if(move_X == -1 && move_Y == 0){
			allMovesTowardsPosition.add("W");
		}
		ArrayList<String> allPossibleMovesTowardsPosition = new ArrayList<String>();
		for(String dir : allMovesTowardsPosition){
			if(Game.currentBattle.validMove(from, dir)){
				if(Game.currentBattle.getPossibleOwnGladitorFromEndTile(gladiator, dir) == null)
					allPossibleMovesTowardsPosition.add(dir);
			}
		}
		if(allPossibleMovesTowardsPosition.size() == 0){
			ArrayList<String> allPossibleMoves = new ArrayList<String>();
			for(String dir : allMoves){
				if(Game.currentBattle.validMove(from, dir)){
					allPossibleMoves.add(dir);
				}
			}
			if(allPossibleMoves.size() > 0){
				selectedMove = allPossibleMoves.get(Util.random_(allPossibleMoves.size()));
			}
		} else {
			selectedMove = allPossibleMovesTowardsPosition.get(0);
		}
		return selectedMove;
	}

	public boolean validMove(Position pos, String dir){
		int move_X = 0;
		int move_Y = 0;
		if(dir.equals("NW")){
			move_X = -1;
			move_Y = -1;
		}
		if(dir.equals("N")){
			move_Y = -1;
		}
		if(dir.equals("NE")){
			move_X = 1;
			move_Y = -1;
		}
		if(dir.equals("E")){
			move_X = 1;
		}
		if(dir.equals("SE")){
			move_X = 1;
			move_Y = 1;
		}
		if(dir.equals("S")){
			move_Y = 1;
		}
		if(dir.equals("SW")){
			move_X = -1;
			move_Y = 1;
		}
		if(dir.equals("W")){
			move_X = -1;
		}
		int cur_X = pos.getX();
		int cur_Y = pos.getY();
		if(cur_X <= 0 && move_X < 0)
			return false;
		if(cur_Y <= 0 && move_Y < 0)
			return false;
		if(cur_X >= Constants.MAP_WIDTH-1 && move_X > 0)
			return false;
		if(cur_Y >= Constants.MAP_HEIGHT-1 && move_Y > 0)
			return false;
		int new_X = cur_X + move_X;
		int new_Y = cur_Y + move_Y;
		if(map.mapTiles[new_X][new_Y].clipped)
			return false;
		return true;
	}

	public boolean moveGladiatorAI(Gladiator gladiator, String dir){
		int move_X = 0;
		int move_Y = 0;
		if(dir.equals("NW")){
			move_X = -1;
			move_Y = -1;
		}
		if(dir.equals("N")){
			move_Y = -1;
		}
		if(dir.equals("NE")){
			move_X = 1;
			move_Y = -1;
		}
		if(dir.equals("E")){
			move_X = 1;
		}
		if(dir.equals("SE")){
			move_X = 1;
			move_Y = 1;
		}
		if(dir.equals("S")){
			move_Y = 1;
		}
		if(dir.equals("SW")){
			move_X = -1;
			move_Y = 1;
		}
		if(dir.equals("W")){
			move_X = -1;
		}
		int cur_X = gladiator.pos.getX();
		int cur_Y = gladiator.pos.getY();
		if(cur_X <= 0 && move_X < 0)
			return false;
		if(cur_Y <= 0 && move_Y < 0)
			return false;
		if(cur_X >= Constants.MAP_WIDTH-1 && move_X > 0)
			return false;
		if(cur_Y >= Constants.MAP_HEIGHT-1 && move_Y > 0)
			return false;
		int new_X = cur_X + move_X;
		int new_Y = cur_Y + move_Y;
		if(map.mapTiles[new_X][new_Y].clipped)
			return false;
		resetDmgHitmarks();
		Gladiator otherGladiator = map.mapTiles[new_X][new_Y].gladiatorOnTile;
		if(otherGladiator != null){
			if(otherGladiator.owner == gladiator.owner || otherGladiator.hp <= 0){
				otherGladiator.pos = new Position(cur_X, cur_Y);
			} else {
				Game.meleeAtk(gladiator, otherGladiator);
				return true;
			}
		}
		map.mapTiles[cur_X][cur_Y].gladiatorOnTile = otherGladiator;
		map.mapTiles[new_X][new_Y].gladiatorOnTile = gladiator;
		gladiator.pos = new Position(new_X, new_Y);
		return true;
	}

	public Gladiator getPossibleOwnGladitorFromEndTile(Gladiator gladiator, String dir){
		int move_X = 0;
		int move_Y = 0;
		if(dir.equals("NW")){
			move_X = -1;
			move_Y = -1;
		}
		if(dir.equals("N")){
			move_Y = -1;
		}
		if(dir.equals("NE")){
			move_X = 1;
			move_Y = -1;
		}
		if(dir.equals("E")){
			move_X = 1;
		}
		if(dir.equals("SE")){
			move_X = 1;
			move_Y = 1;
		}
		if(dir.equals("S")){
			move_Y = 1;
		}
		if(dir.equals("SW")){
			move_X = -1;
			move_Y = 1;
		}
		if(dir.equals("W")){
			move_X = -1;
		}
		int cur_X = gladiator.pos.getX();
		int cur_Y = gladiator.pos.getY();
	
		int new_X = cur_X + move_X;
		int new_Y = cur_Y + move_Y;
	
		Gladiator otherGladiator = map.mapTiles[new_X][new_Y].gladiatorOnTile;
		if(otherGladiator != null){
			if(otherGladiator.owner == gladiator.owner && otherGladiator.hp > 0){
				return otherGladiator;
			}
		}
		return null;
	}
	
}
