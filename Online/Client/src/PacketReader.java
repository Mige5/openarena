

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class PacketReader {

	private DataInputStream in;
	private Client client;

	public PacketReader(Client client) {
		this.client = client;
		this.in = this.client.in;
	}
	
	public void readPacket(int packetId) throws IOException{
		if(packetId == 0){
			int teamId = in.readByte();
			String teamName = in.readUTF();
			Player player = new Player(teamId, teamName);
			Game.ownPlayer = player;
			Game.createOwnPlayer();
			Game.startNewGame();
			client.gamePanel.mainFrame.newGamePanel.updateButtons();
		}
		else if(packetId == 1){
			int playerCount = in.readByte();
			for(int i = 0; i < playerCount; i++){
				int teamId = in.readByte();
				String teamName = in.readUTF();
				if(teamId != Game.ownPlayer.playerId){
					Player player = new Player(teamId, teamName);
				}
			}
			client.gamePanel.mainFrame.newGamePanel.updateButtons();
		}
		else if(packetId == 2){
			client.gamePanel.mainFrame.dialog.setVisible(false);
			client.gamePanel.mainFrame.waitDialog.setVisible(false);
			client.gamePanel.mainFrame.waiting = false;
		}
		else if(packetId == 3){
			Game.recruitableGladiators.clear();
			int count = in.readByte();
			for(int i = 0; i < count; i++){
				Game.recruitableGladiators.add(readGladiatorData());
			}
			client.gamePanel.addGladiatorsToRecruitTable();
		}
		else if(packetId == 4){
			int window = in.readByte();
			int subWindow = in.readByte();
			client.gamePanel.mainFrame.changeWindow(window, subWindow);
		}
		else if(packetId == 5){
			client.gamePanel.setWindowMessage();
		}
		else if(packetId == 6){
			Game.YEAR = in.readShort();
			Game.MONTH = in.readByte();
			int race_i = in.readByte();
			Game.RACE_OF_MONTH = Race.races.get(race_i);
		}
		else if(packetId == 7){
			Game.currentRecruitTeamIdx = in.readByte();
			Game.currentRecruitRound = in.readByte();
		}
		else if(packetId == 8){
			int divCount = in.readByte();
			for(int i = 0; i < divCount; i++){
				int teamCount = in.readByte();
				for(int j = 0; j < teamCount; j++){
					int div = in.readByte();
					String teamName = in.readUTF();
					boolean playerControlled = in.readBoolean();
					if(teamName.equals(Game.ownTeam.name)){
						Division.divisions.get(i).teams.add(Game.ownTeam);
						Game.ownTeam.controllingPlayer = Game.ownPlayer;
						Game.ownTeam.division = div;
						Game.teams.add(Game.ownTeam);
					} else {
						Team team = new Team(teamName);
						team.division = div;
						if(playerControlled){
							Player player = new Player(team);
							team.controllingPlayer = player;
						}
						Division.divisions.get(i).teams.add(team);
						Game.teams.add(team);
					}
				}
			}
			Game.setupRecruitOrder();
		}
		else if(packetId == 9){
			Game.ownTeam.money = in.readInt();
			Game.ownTeam.arenaPoints = in.readByte();
			Game.ownTeam.rating = in.readByte();
		}
		else if(packetId == 10){
			Game.ownTeam.gladiators.clear();
			int count = in.readByte();
			for(int i = 0; i < count; i++){
				Game.ownTeam.gladiators.add(readGladiatorData());
			}
		}
		else if(packetId == 11){
			client.gamePanel.updateButtons();
			client.gamePanel.refresh();
		}
		else if(packetId == 12){
			int round = in.readByte();
			int size = in.readByte();
			ArrayList<Battle> battles = new ArrayList<Battle>();
			for(int i = 0; i < size; i++){
				String topTeamName = in.readUTF();
				Team topTeam = Game.findTeamForName(topTeamName);
				String bottomTeamName = in.readUTF();
				Team bottomTeam = Game.findTeamForName(bottomTeamName);
				battles.add(new Battle(topTeam, bottomTeam));
			}
			if(round == 1){
				Game.cupBattles_round1.clear();
				Game.cupBattles_round1.addAll(battles);
			}
			else if(round == 2){
				Game.cupBattles_round2.clear();
				Game.cupBattles_round2.addAll(battles);
			}
			else if(round == 3){
				Game.cupBattles_round3.clear();
				Game.cupBattles_round3.addAll(battles);
			}
			else if(round == 4){
				Game.cupBattles_round4.clear();
				Game.cupBattles_round4.addAll(battles);
			}
		}
		else if(packetId == 13){
			String teamName = in.readUTF();
			Team team = Game.findTeamForName(teamName);
			team.gladiators.clear();
			int count = in.readByte();
			for(int i = 0; i < count; i++){
				team.gladiators.add(readGladiatorData());
			}
		}
		else if(packetId == 14){
			Item.discountedItems.clear();
			int count = in.readByte();
			for(int i = 0; i < count; i++){
				String itemName = in.readUTF();
				Item item = Item.findItem(itemName);
				Item.addDiscountedItem(item);
			}
		}
		else if(packetId == 15){
			client.gamePanel.mainFrame.showWaitDialog();
		}
		else if(packetId == 16){
			ArrayList<Gladiator> bottomTeamGladiators = new ArrayList<Gladiator>();
			ArrayList<Gladiator> topTeamGladiators = new ArrayList<Gladiator>();
			String topTeamName = in.readUTF();
			Team topTeam = Game.findTeamForName(topTeamName);
			String bottomTeamName = in.readUTF();
			Team bottomTeam = Game.findTeamForName(bottomTeamName);
			Battle battle = new Battle(topTeam, bottomTeam);
			Game.currentBattle = battle;
			
			for(int x = 0; x < Constants.MAP_WIDTH; x++){
    			for(int y = 0; y < Constants.MAP_HEIGHT; y++){
    				Game.currentBattle.map.mapTiles[x][y].gladiatorOnTile = null;
    			}
			}
    				
			
			for(int j = 0; j < 2; j++){
				Team team = battle.topTeam;
				if(j == 1)
					team = battle.bottomTeam;
				for(int i = 0; i < team.battleGladiators.length; i++){
					Gladiator gladiator = null;
					int idx = in.readByte();
					if(idx >= 0){
						gladiator = team.gladiators.get(idx);
					}
					int x = in.readByte();
					int y = in.readByte();
					if(gladiator != null){
						gladiator.pos = new Position(x, y);
						if(team == topTeam){
							topTeamGladiators.add(gladiator);
						} else {
							bottomTeamGladiators.add(gladiator);
						}
						Game.currentBattle.map.mapTiles[x][y].gladiatorOnTile = gladiator;
					}
				}
			}
			battle.baseIncome = in.readInt();
			battle.winBonus = in.readInt();
			
			bottomTeam.modifyMoney(battle.baseIncome);
			topTeam.modifyMoney(battle.baseIncome);
			bottomTeam.currentMonthIncome = battle.baseIncome;
			topTeam.currentMonthIncome = battle.baseIncome;
			
			for(int i = 0; i < Constants.MAX_GLADIATORS_IN_BATTLE; i++){
				if(i < bottomTeamGladiators.size())
					battle.gladiatorBattleTurnOrder.add(bottomTeamGladiators.get(i));
				if(i < topTeamGladiators.size())
					battle.gladiatorBattleTurnOrder.add(topTeamGladiators.get(i));
			}
		}
		else if(packetId == 17){
			Battle battle = Game.currentBattle;
			battle.round = in.readByte();
			battle.currentBattleGladiatorIdx = in.readByte();
			int global_idx = in.readInt();
			Game.currentGladiator = Gladiator.gladiatorMap.get(global_idx);
			//System.out.println(Game.currentGladiator.owner.name);
			/*
			if(Game.ownTeam.gladiators.contains(battle.getCurrentBattleGladiator())){
				
			}
			�/
			//Game.currentGladiator = Game.currentTeam.gladiators.get(battle.currentBattleGladiatorIdx);
			
			/*
			 		if(i < bottomTeamGladiators.size())
					battle.gladiatorBattleTurnOrder.add(bottomTeamGladiators.get(i));
				if(i < topTeamGladiators.size())
					battle.gladiatorBattleTurnOrder.add(topTeamGladiators.get(i));
			 */
			
			for(int x = 0; x < Constants.MAP_WIDTH; x++){
    			for(int y = 0; y < Constants.MAP_HEIGHT; y++){
    				battle.map.mapTiles[x][y].gladiatorOnTile = null;
    			}
			}
			
			for(int j = 0; j < 2; j++){
				Team team = battle.topTeam;
				if(j == 1)
					team = battle.bottomTeam;
				for(int i = 0; i < team.battleGladiators.length; i++){
					Gladiator gladiator = null;
					int idx = in.readByte();
					if(idx >= 0){
						gladiator = team.gladiators.get(idx);
					}
					int x = in.readByte();
					int y = in.readByte();
					if(gladiator != null){
						gladiator.pos = new Position(x, y);
						battle.map.mapTiles[x][y].gladiatorOnTile = gladiator;
					}
				}
			}
			client.gamePanel.refresh();
		}
		else if(packetId == 18){
			String winningTeamName = in.readUTF();
			String losingTeamName = in.readUTF();
			int reward = in.readInt();
			Team winningTeam = Game.findTeamForName(winningTeamName);
			Team losingTeam = Game.findTeamForName(losingTeamName);
			client.gamePanel.mainFrame.showBattleResult(winningTeam, losingTeam, reward);
		}
		else if(packetId == 19){
			int count = in.readByte();
			String[] messages = new String[count];
			for(int i = 0; i < count; i++){
				messages[i] = in.readUTF();
			}
			GamePanel.setMessage(messages);
		}
		else if(packetId == 20){
			int count = in.readByte();
			
			ArrayList<Battle> battles = new ArrayList<Battle>();
			for(int i = 0; i < count; i++){
				String topTeamName = in.readUTF();
				String bottomTeamName = in.readUTF();
				String winningTeamName = in.readUTF();
				String losingTeamName = in.readUTF();
				Team topTeam = Game.findTeamForName(topTeamName);
				Team bottomTeam = Game.findTeamForName(bottomTeamName);
				Team winningTeam = Game.findTeamForName(winningTeamName);
				Team losingTeam = Game.findTeamForName(losingTeamName);
				Battle battle = new Battle(topTeam, bottomTeam);
				battle.winningTeam = winningTeam;
				battle.losingTeam = losingTeam;
				battles.add(battle);
				battle.winningTeam.wins++;
			}
			Game.progressToNextMonth = true;
			Game.battleList = battles;
			client.gamePanel.mainFrame.showBattleResultsPanel();
		}

	}

	public Gladiator readGladiatorData(){
		try {
			Gladiator gladiator = new Gladiator();
			int global_idx = in.readInt();
			String name = in.readUTF();
			int race_i = in.readByte();
			if(Gladiator.gladiatorMap.get(global_idx) != null){
				gladiator = Gladiator.gladiatorMap.get(global_idx);
			} else {
				gladiator = new Gladiator(name, race_i, global_idx);
			}
			gladiator.str = in.readByte();
			gladiator.speed = in.readByte();
			gladiator.maxMana = in.readByte();
			gladiator.maxHp = in.readByte();
			gladiator.mana = in.readByte();
			gladiator.hp = in.readByte();
			gladiator.age = in.readByte();
			gladiator.askPrice = in.readByte();
			gladiator.salary = in.readByte();
			gladiator.melee_weapon_atk_1 = in.readByte();
			gladiator.melee_weapon_atk_2 = in.readByte();
			gladiator.melee_weapon_atk_3 = in.readByte();
			gladiator.melee_weapon_atk_4 = in.readByte();
			gladiator.melee_weapon_atk_5 = in.readByte();
			gladiator.melee_weapon_def_1 = in.readByte();
			gladiator.melee_weapon_def_2 = in.readByte();
			gladiator.melee_weapon_def_3 = in.readByte();
			gladiator.melee_weapon_def_4 = in.readByte();
			gladiator.melee_weapon_def_5 = in.readByte();
			gladiator.ranged_weapon_atk_1 = in.readByte();
			gladiator.ranged_weapon_atk_2 = in.readByte();
			gladiator.ability1 = in.readByte();
			gladiator.ability2 = in.readByte();
			for(int i = 0; i < gladiator.spellAtk.length; i++){
				gladiator.spellAtk[i] = in.readByte();
			}
			gladiator.speedBonus = in.readByte();
			gladiator.strBonus = in.readByte();
			gladiator.manaBonus = in.readByte();
			
			String ownerName = in.readUTF();
			if(!ownerName.equals("")){
				Team team = Game.findTeamForName(ownerName);
				if(team != null){
					gladiator.owner = team;
				}
			}
			String offerTeam = in.readUTF();
			if(!offerTeam.equals("")){
				Team team = Game.findTeamForName(offerTeam);
				if(team != null){
					gladiator.offerTeam = team;
				}
			}
			gladiator.askPrice = in.readInt();
			gladiator.offerPrice = in.readInt();
			
			gladiator.takeOuts_1 = in.readInt();
			gladiator.takeOuts_2 = in.readInt();
			gladiator.hurt = in.readByte();
			gladiator.matches = in.readInt();
			gladiator.skillEffectCount = in.readByte();
			gladiator.moral = in.readByte();
			gladiator.condition = in.readByte();
			gladiator.spells = in.readByte();
			gladiator.specialGladiator = in.readBoolean();
			for(int i = 0; i < gladiator.items.length; i++){
				String itemName = in.readUTF();
				Item item = Item.findItem(itemName);
				gladiator.items[i] = item;
			}
			for(int i = 0; i < gladiator.knownSpells.length; i++){
				String spellName = in.readUTF();
				Spell spell = Spell.findSpell(spellName);
				gladiator.knownSpells[i] = spell;
			}
			int x = in.readByte();
			int y = in.readByte();
			if(x != -1 && y != -1){
				gladiator.pos = new Position(x, y);
			}
			gladiator.receivedDmg = in.readByte();
			gladiator.ammo = in.readByte();
			gladiator.needsReload = in.readBoolean();
			return gladiator;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
