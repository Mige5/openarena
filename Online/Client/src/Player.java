import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;

public class Player {

	public int playerId = -1;
	public String teamName = "";
	
	//public static ArrayList<Player> players = new ArrayList<Player>();
	public static HashMap<Integer, Player> players2 = new HashMap<Integer, Player>();
	
	Team controlledTeam;
	
	public Player(Team team){
		this.controlledTeam = team;
		//players.add(this);
	}
	
	public Player(int playerId, String teamName){
		this.playerId = playerId;
		this.teamName = teamName;
		//players.add(this);
		players2.put(this.playerId, this);
	}
	
	public static ArrayList<Player> getPlayers(){
		ArrayList<Player> players_ = new ArrayList<Player>();
		for (Entry<Integer, Player> mapEntry : players2.entrySet()) {
			int key = mapEntry.getKey();
			Player player_ = mapEntry.getValue();
			players_.add(player_);
        }
		organizePlayers(players_);
		return players_;
	}
	
	public static void organizePlayers(ArrayList<Player> players){
		Collections.sort(players, new Comparator<Player>() {
		    public int compare(Player p1, Player p2) {
		    	return Integer.compare(p1.playerId, p2.playerId);
		    }
		});
	}
	
}
