import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;

public class Jframe extends Applet implements ActionListener, MouseListener, KeyListener {

	private static final long serialVersionUID = 1L;

	public JFrame frame;
	
	public GamePanel gamePanel;
	public NewGamePanel newGamePanel;
	public TeamViewPanel teamViewPanel;
	public StandingsPanel standingsPanel;
	public BattleResultsPanel battleResultsPanel;
	public CupStandingsPanel cupStandingsPanel;
	public MatchScheludePanel matchScheludePanel;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new Jframe();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public Jframe() {
		init();
	}
	
	public static Font MENU_FONT = new Font("Segoe UI", Font.PLAIN, 12);
	
	public void init() {
		frame = new JFrame();
		frame.setTitle(Constants.GAME_NAME+" "+Constants.GAME_VERSION);
		frame.setLayout(new BorderLayout());
		frame.setResizable(false);
		frame.setDefaultCloseOperation(3);
		setFontsForPopups();
		Race.initRaces();
		ImageHandler.loadImages();
		frame.setIconImage(ImageHandler.gladiatorImages[11]);
		JMenuBar menuBar = new JMenuBar();
		JPanel e = new GamePanel(this);
		newGamePanel = new NewGamePanel(this);
		gamePanel = (GamePanel)e;
		Game.gamePanel = gamePanel;
		e.addMouseListener(this);
		e.setPreferredSize(new Dimension(794, 528));
		frame.addKeyListener(this);
		frame.setFocusable(true);
		teamViewPanel = new TeamViewPanel(this);
		teamViewPanel.setPreferredSize(new Dimension(792, 546));
		standingsPanel = new StandingsPanel(this);
		standingsPanel.setPreferredSize(new Dimension(794, 548));
		battleResultsPanel = new BattleResultsPanel(this);
		battleResultsPanel.setPreferredSize(new Dimension(794, 548));
		cupStandingsPanel = new CupStandingsPanel(this);
		cupStandingsPanel.setPreferredSize(new Dimension(794, 548));
		matchScheludePanel = new MatchScheludePanel(this);
		matchScheludePanel.setPreferredSize(new Dimension(794, 548));
		frame.getContentPane().add(e, BorderLayout.CENTER);
		frame.getContentPane().add(menuBar, BorderLayout.NORTH);

		JMenu menu_1 = new JMenu("Tiedosto");
		String[] menu_1_buttons = new String[] {"Uusi peli", "Avaa...", "Tallenna", "Tallenna nimell�...", "-", "Sankarit k�ytett�viss�", "-", "Lopeta"};
		addMenuButtons(menu_1, menu_1_buttons);
		JMenu menu_2 = new JMenu("Vaikeustaso");
		String[] menu_2_buttons = new String[] {"Helppo", "Normaali", "Vaikea", "Mahdoton"};
		addMenuButtons(menu_2, menu_2_buttons);
		JMenu menu_3 = new JMenu("Joukkue");
		String[] menu_3_buttons = new String[] {"Avaa joukkue...", "Tallenna joukkue..."};
		addMenuButtons(menu_3, menu_3_buttons);
		JMenu menu_4 = new JMenu("Pelaaja");
		String[] menu_4_buttons = new String[] {"Gladiaattorit Oy", "2. pelaajan joukkue", "3. pelaajan joukkue", "4. pelaajan joukkue", "-", "Vaihda joukkueen nimi", "-", "J�t� eroanomus", "-", "Uusi pelaaja"};
		addMenuButtons(menu_4, menu_4_buttons);
		JMenu menu_5 = new JMenu("Tilastot");
		String[] menu_5_buttons = new String[] {"Mestareiden marmoritaulu", "-", "Kaikkien aikojen kaadot", "-", "Kaadot", "Moraali", "Kovuus", "Palkka", "Kaatoja / ottelu", "Otteluja", "Kalleimmat varusteet", "-", "Voima", "Nopeus", "Mana", "Kesto", "-", "Meleehy�kk�ys", "Torjunta", "Heittoasehy�kk�ys", "V�ist�", "Loitsun heittotaito", "Loitsujen vastustamiskyky", "-", "Mestaruudet", "CUP-voitot", "Joukkueiden voittoprosentit", "Joukkueiden kovuusindeksit", "Joukkueiden palkkamenot"};
		addMenuButtons(menu_5, menu_5_buttons);
		JMenu menu_6 = new JMenu("Apua");
		String[] menu_6_buttons = new String[] {"Ohje", "-", "Rotujen ominaisuudet", "-", "Tietoja..."};
		addMenuButtons(menu_6, menu_6_buttons);

		menu_1.setFont(MENU_FONT);
		menu_2.setFont(MENU_FONT);
		menu_3.setFont(MENU_FONT);
		menu_4.setFont(MENU_FONT);
		menu_5.setFont(MENU_FONT);
		menu_6.setFont(MENU_FONT);
		
		menuBar.add(menu_1);
		menuBar.add(menu_2);
		menuBar.add(menu_3);
		menuBar.add(menu_4);
		menuBar.add(menu_5);
		menuBar.add(menu_6);
		
		frame.pack();
		frame.setLocationRelativeTo((Component) null);
		frame.setVisible(true);
		
		String txt = getAsOneString(Constants.GAME_NAME+" is free software, licenced under the GPL3 licence.");
		JOptionPane.showMessageDialog(frame, txt, "Licence", JOptionPane.PLAIN_MESSAGE);
		buildWaitDialog();

	}

	public void addMenuButtons(JMenu menu, String[] buttons){
		for (String name : buttons) {
			JMenuItem menuItem = new JMenuItem(name);
			menuItem.setFont(MENU_FONT);
			if (name.equalsIgnoreCase("-")) {
				menu.addSeparator();
			} else {
				menuItem.addActionListener(this);
				menu.add(menuItem);
			}
		}
	}
	
	public void changeWindow(int newWindow){
		changeWindow(newWindow, 0);
	}

	public void changeWindow(int newWindow, int subWindow){
		gamePanel.GAME_WINDOW_CURRENT = newWindow;
		gamePanel.SUB_GAME_WINDOW_CURRENT = subWindow;
		gamePanel.updateButtons();
		frame.validate();
		frame.repaint();
		if(gamePanel.GAME_WINDOW_CURRENT == GamePanel.GAME_WINDOW_RECRUIT){
			gamePanel.findTable(GamePanel.RECRUIT_TABLE).setRowSelectionInterval(0, 0);
			Game.currentGladiator = Game.recruitableGladiators.get(0);
		}
		if(gamePanel.GAME_WINDOW_CURRENT == GamePanel.GAME_WINDOW_OTHER_TEAMS){
			gamePanel.findTable(GamePanel.OTHER_TEAMS_TABLE).setRowSelectionInterval(0, 0);
			Game.showGladiatorsForTeam = Game.teams.get(0);
			Game.currentGladiator = Game.showGladiatorsForTeam.gladiators.get(0);
		}
		if(gamePanel.GAME_WINDOW_CURRENT == GamePanel.GAME_WINDOW_MAIN_MENU){
			if(Game.currentTeam.gladiators.size() > 0){
				Game.currentGladiator = Game.currentTeam.gladiators.get(0);
			} else {
				Game.currentGladiator = null;
			}
		}
	}
	
	public void addText(JPanel panel, String text, Font f, int x, int y){
		JLabel label = new JLabel(text);
		label.setFont(f);
		panel.add(label);
		Dimension size = label.getPreferredSize();
		label.setBounds(x, y, size.width, size.height);
	}
	
	public void addButton(JPanel panel, String text, Font f, int x, int y, int width, int height){
		JButton button = new JButton(text);
		button.setFont(f);
		panel.add(button);
		button.setBounds(x, y, width, height);
		button.addActionListener(this);
	}
	
	public void showBattleResult(Team winningTeam, Team losingTeam, int reward){
		//TODO: gladiator skill advancements
		String txt = getAsOneString("Raivoisa taistelu on tauonnut!", "", "Taistelun voittaja on "+winningTeam.name+".", "Taistelun h�vi�j� on "+losingTeam.name+".");
		if(Game.currentTeam == Game.currentBattle.winningTeam){
			txt = getAsOneString("Raivoisa taistelu on tauonnut!", "", "Taistelun voittaja on "+winningTeam.name+".", "Taistelun h�vi�j� on "+losingTeam.name+".", "", "Joukkueen "+winningTeam.name+" johtoporras antaa "+reward+" kultarahan", "voittobonuksen.");
		}
		JOptionPane.showMessageDialog(frame, txt, "Taistelu loppui", JOptionPane.PLAIN_MESSAGE);
	}
	
	public void showBattleResultsPanel(){
		if(Game.monthType[Game.MONTH] == Game.MONTH_TYPE_DIVISION_BATTLE){
			dialog = new JDialog();
			dialog.setTitle(Game.getCurrentRoundNumber()+". SARJAKIERROKSEN TULOKSET");
			dialog.setIconImage(ImageHandler.gladiatorImages[11]);
			dialog.setModal(true);
			dialog.add(battleResultsPanel);
			dialog.pack();
			dialog.setLocationRelativeTo(frame);
			dialog.setVisible(true);
		} else if(Game.monthType[Game.MONTH] == Game.MONTH_TYPE_CUP_BATTLE){
			dialog = new JDialog();
			dialog.setTitle("VUODEN "+Game.YEAR+" CUP-taulukko");
			dialog.setIconImage(ImageHandler.gladiatorImages[11]);
	        dialog.setModal(true);
	        dialog.add(cupStandingsPanel);
	        dialog.pack();
	        dialog.setLocationRelativeTo(frame);
	        dialog.setVisible(true);
		}
	}
	
	public static Font SMALL_BUTTON_FONT = new Font("comic sans ms", Font.PLAIN, 11);
	
	JDialog dialog = new JDialog();
	
	public static String getAsOneString(String... lines){
		String txt = null;
		for(String s : lines){
			if(txt == null){
				txt = s;
			} else {
				txt += "\n"+s;
			}
		}
		return txt;
	}
	
	Client client;
	
	public boolean waiting = false;
	
	public void showWaitDialog(){
		/*
		final JOptionPane optionPane = new JOptionPane("Odotetaan muita pelaajia.", JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
		dialog = new JDialog();
		dialog.setTitle("Odotetaan...");
		dialog.setModal(false);
		dialog.setResizable(false);
		dialog.setContentPane(optionPane);
		dialog.setLocationRelativeTo(frame);
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.pack();
		dialog.setVisible(true);
		dialog.setAlwaysOnTop(true);
		*/
		waitDialog.setLocationRelativeTo(frame);
		waitDialog.setVisible(true);
		waiting = true;
	}
	
	public JDialog waitDialog = new JDialog();
	
	public void buildWaitDialog(){
		final JOptionPane optionPane = new JOptionPane("Odotetaan muita pelaajia.", JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
		waitDialog.setTitle("Odotetaan...");
		waitDialog.setModal(false);
		waitDialog.setResizable(false);
		waitDialog.setContentPane(optionPane);
		waitDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		waitDialog.pack();
		waitDialog.setAlwaysOnTop(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(waiting)
			return;
		System.out.println("Button click: "+e.getActionCommand());
		if (e.getActionCommand().equals(GamePanel.MATCH_SCHELUDE_TXT)){
			dialog = new JDialog();
			matchScheludePanel.setTeam2show(Game.currentTeam);
			dialog.setTitle("VUODEN "+Game.YEAR+" OTTELUOHJELMA");
			dialog.setIconImage(ImageHandler.gladiatorImages[11]);
	        dialog.setModal(true);
	        dialog.add(matchScheludePanel);
	        dialog.pack();
	        dialog.setLocationRelativeTo(frame);
	        dialog.setVisible(true);
		}
		if (e.getActionCommand().equals(GamePanel.CUP_STANDINGS_TXT)){
			dialog = new JDialog();
			dialog.setTitle("VUODEN "+Game.YEAR+" CUP-taulukko");
			dialog.setIconImage(ImageHandler.gladiatorImages[11]);
	        dialog.setModal(true);
	        dialog.add(cupStandingsPanel);
	        dialog.pack();
	        dialog.setLocationRelativeTo(frame);
	        dialog.setVisible(true);
		}
		
		if (e.getActionCommand().equals(GamePanel.STANDINGS_TXT)){
			dialog = new JDialog();
			dialog.setTitle("VUODEN "+Game.YEAR+" SARJATAULUKOT");
			dialog.setIconImage(ImageHandler.gladiatorImages[11]);
	        dialog.setModal(true);
	        dialog.add(standingsPanel);
	        dialog.pack();
	        dialog.setLocationRelativeTo(frame);
	        dialog.setVisible(true);
		}
		if (e.getActionCommand().equals(GamePanel.OWN_TEAM_TXT)){
			dialog = new JDialog();
			teamViewPanel.setTeam2show(Game.currentTeam);
			dialog.setTitle(Game.currentTeam.name);
			dialog.setIconImage(ImageHandler.gladiatorImages[32]);
	        dialog.setModal(true);
	        dialog.add(teamViewPanel);
	        dialog.pack();
	        dialog.setLocationRelativeTo(frame);
	        dialog.setVisible(true);
		}
		if (e.getActionCommand().equals(GamePanel.SHOW_OPPONENT_BUTTON_NAME) || e.getActionCommand().equals(GamePanel.SHOW_OPPONENT_TEAM_TXT)){
			dialog = new JDialog();
			Team team = Game.getNextOpponentForTeam(Game.currentTeam);
			teamViewPanel.setTeam2show(team);
			dialog.setTitle(team.name);
			dialog.setIconImage(ImageHandler.gladiatorImages[32]);
	        dialog.setModal(true);
	        dialog.add(teamViewPanel);
	        dialog.pack();
	        dialog.setLocationRelativeTo(frame);
	        dialog.setVisible(true);
		}
		if (e.getActionCommand().equals(GamePanel.BATTLE_TEAM_1_BUTTON_NAME)){
			dialog = new JDialog();
			Team team = Game.currentBattle.topTeam;
			teamViewPanel.setTeam2show(team);
			dialog.setTitle(team.name);
			dialog.setIconImage(ImageHandler.gladiatorImages[32]);
	        dialog.setModal(true);
	        dialog.add(teamViewPanel);
	        dialog.pack();
	        dialog.setLocationRelativeTo(frame);
	        dialog.setVisible(true);
		}
		if (e.getActionCommand().equals(GamePanel.BATTLE_TEAM_2_BUTTON_NAME)){
			dialog = new JDialog();
			Team team = Game.currentBattle.bottomTeam;
			teamViewPanel.setTeam2show(team);
			dialog.setTitle(team.name);
			dialog.setIconImage(ImageHandler.gladiatorImages[32]);
	        dialog.setModal(true);
	        dialog.add(teamViewPanel);
	        dialog.pack();
	        dialog.setLocationRelativeTo(frame);
	        dialog.setVisible(true);
		}
		if (e.getActionCommand().equals(GamePanel.SHOW_OTHER_TEAM_TXT)){
			dialog = new JDialog();
			Team team = Game.showGladiatorsForTeam;
			teamViewPanel.setTeam2show(team);
			dialog.setTitle(team.name);
			dialog.setIconImage(ImageHandler.gladiatorImages[32]);
	        dialog.setModal(true);
	        dialog.add(teamViewPanel);
	        dialog.pack();
	        dialog.setLocationRelativeTo(frame);
	        dialog.setVisible(true);
		}
		if (e.getActionCommand().equals(GamePanel.NEW_GAME_TXT)){
			/*
			dialog = new JDialog();
			dialog.setTitle("Aloita uusi peli");
			dialog.setIconImage(ImageHandler.gladiatorImages[14]);
	        dialog.setModal(true);
	        dialog.add(newGamePanel);
	        dialog.pack();
	        dialog.setLocationRelativeTo(null);
	        dialog.setVisible(true);
	        */
		}
		if (e.getActionCommand().equals(GamePanel.LOAD_GAME_TXT)){

		}
		if (e.getActionCommand().equals(GamePanel.ONLINE_GAME_TXT)){
			String teamName;
			String txt = getAsOneString("Anna joukkueesi nimi");
			String input = (String) JOptionPane.showInputDialog(frame, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE, null, null, "");
			if(input != null){
				try{
					teamName = input;
					String connectIP;
					txt = getAsOneString("Anna verkkopelin ip-osoite");
					input = (String) JOptionPane.showInputDialog(frame, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE, null, null, "");
					if(input != null){
						try{
							connectIP = input;
							try {
								client = new Client(connectIP, 5000);
								frame.setTitle(Constants.GAME_NAME+" "+Constants.GAME_VERSION+" - "+teamName);
								client.gamePanel = gamePanel;
								client.connect(teamName);
								//client.packetSender.sendTeamNamePacket(teamName);
								dialog = new JDialog();
								dialog.setTitle("Aloita uusi peli");
								dialog.setIconImage(ImageHandler.gladiatorImages[14]);
						        dialog.setModal(true);
						        dialog.add(newGamePanel);
						        dialog.pack();
						        dialog.setLocationRelativeTo(frame);
						        dialog.setVisible(true);
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}catch(Exception ex){
							//ex.printStackTrace();
						}
					}
				}catch(Exception ex){
					//ex.printStackTrace();
				}
			}
		}
		if(dialog.isVisible()){
			newGamePanel.updateButtons();
		}
		if (e.getActionCommand().equals(NewGamePanel.START_GAME)){
			//newGamePanel.startNewGame();
			//Game.startGame();
			client.packetSender.sendStartGamePacket();
    		//dialog.setVisible(false);
    		//changeWindow(GamePanel.GAME_WINDOW_RECRUIT);
    		//gamePanel.setWindowMessage();
    	}
		if (e.getActionCommand().equals("close battle results view")){
    		dialog.setVisible(false);
    		dialog = new JDialog();
			dialog.setTitle("VUODEN "+Game.YEAR+" SARJATAULUKOT");
			dialog.setIconImage(ImageHandler.gladiatorImages[11]);
	        dialog.setModal(true);
	        dialog.add(standingsPanel);
	        dialog.pack();
	        dialog.setLocationRelativeTo(frame);
	        dialog.setVisible(true);
    	}
    	if (e.getActionCommand().equals(NewGamePanel.CANCEL) || e.getActionCommand().equals("close team view")
    			|| e.getActionCommand().equals("close match schelude view")){
    		dialog.setVisible(false);
    	}
    	if (e.getActionCommand().equals("close standings view") || e.getActionCommand().equals("close cup view")){
    		dialog.setVisible(false);
    		if(Game.progressToNextMonth){
    			Game.progressToNextMonth = false;
    			if(Game.MONTH == 9){
    				ArrayList<Team> teams = Division.divisions.get(0).teams;
    				Util.organizeTeams(teams);
    				Team champion = teams.get(0);
    				String txt = getAsOneString("Vuoden "+Game.YEAR+" MESTARIJOUKKUE on "+champion.name+"!");
    				JOptionPane.showMessageDialog(frame, txt, champion.name, JOptionPane.PLAIN_MESSAGE);
    			}
    			//TODO: popup message, for each player?
    			int salary = Game.getSalaryForGladiators(Game.currentTeam);
    			int balance = Game.currentTeam.currentMonthMoneyBalance - salary;
    			String txt = getAsOneString("Gladiaattorien palkkoihin menee "+salary+" "+Constants.MONEY_SUFFIX+".", "Lipputuloja tuli "+Game.currentTeam.currentMonthIncome+" "+Constants.MONEY_SUFFIX+".", "", "Kaikkien tulojen ja menojen yhteenlaskemisen j�lkeen p��dyt", "ARVIOON, ett�", "edellisen kuukauden aikana KASVATIT joukkueen rahavaroja "+balance, "kultarahan verran.");
    			if(balance < 0)
    				txt = getAsOneString("Gladiaattorien palkkoihin menee "+salary+" "+Constants.MONEY_SUFFIX+".", "Lipputuloja tuli "+Game.currentTeam.currentMonthIncome+" "+Constants.MONEY_SUFFIX+".", "", "Kaikkien tulojen ja menojen yhteenlaskemisen j�lkeen p��dyt", "ARVIOON, ett�", "edellisen kuukauden aikana HUKKASIT joukkueen rahavaroja "+Math.abs(balance), "kultarahan edest�.");
    			JOptionPane.showMessageDialog(frame, txt, Game.currentTeam.name, JOptionPane.PLAIN_MESSAGE);
    			//Game.progressToNextMonth();
    			client.packetSender.sendRequestNewMonthDataPacket();
    			changeWindow(GamePanel.GAME_WINDOW_RECRUIT);
    			Game.currentBattle = null;
        		gamePanel.setWindowMessage();
        		Game.currentTeam.currentMonthMoneyBalance = 0;
        		Game.currentTeam.currentMonthIncome = 0;
    		}
    	}
		if (e.getActionCommand().equals(GamePanel.START_FIGHT_CHANGE_PLAYER_BUTTON_NAME)){
			if(Player.getPlayers().size() > 1 && !Game.isOnlineGame){
				changeWindow(GamePanel.GAME_WINDOW_CHANGE_PLAYER);
			} else {
				Game.currentTeam.battleGladiators = new Gladiator[Constants.MAX_GLADIATORS_IN_BATTLE];
				if(Game.getNextOpponentForTeam(Game.currentTeam) != null) {
					if(Game.currentRecruitRound > Constants.RECRUIT_TURNS){
						changeWindow(GamePanel.GAME_WINDOW_BATTLE_SETUP);
						gamePanel.setWindowMessage();
					} else {
						String txt = getAsOneString("T�m�n kuukauden V�RV�YSVAIHE on viel� kesken.", "", "Haluatko J�TT�� K�YTT�M�TT� t�m�n kuukauden", "v�rv�yksesi?");
						Object[] options = {"Kyll�", "Ei"};
						int n = JOptionPane.showOptionDialog(frame,
										txt,
										Constants.GAME_NAME,
						                JOptionPane.YES_NO_OPTION,
						                JOptionPane.QUESTION_MESSAGE,
						                null,
						                options,
						                options[0]);
						if (n == JOptionPane.YES_OPTION) {
							if(!Game.isOnlineGame){
								Game.currentRecruitTeamIdx = 1;
								Game.currentRecruitRound = Constants.RECRUIT_TURNS + 1;
								changeWindow(GamePanel.GAME_WINDOW_BATTLE_SETUP);
								gamePanel.setWindowMessage();
							} else {
								client.packetSender.sendSkipRecruitPacket();
								showWaitDialog();
							}
						}
					}
				} else {
					if(Game.monthType[Game.MONTH] == Game.MONTH_TYPE_WINTER_BREAK){
						String txt = getAsOneString("Gladiaattoreille ei makseta palkkaa talvitauon aikana.");
	    				JOptionPane.showMessageDialog(frame, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE);
	    				if(Game.MONTH == 11){
	        				txt = getAsOneString("Vuoden "+Game.YEAR+" lopulliset sarjataulukot...");
	        				JOptionPane.showMessageDialog(frame, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE);
	        				dialog = new JDialog();
	        				dialog.setTitle("VUODEN "+Game.YEAR+" SARJATAULUKOT");
	        				dialog.setIconImage(ImageHandler.gladiatorImages[11]);
	        		        dialog.setModal(true);
	        		        dialog.add(standingsPanel);
	        		        dialog.pack();
	        		        dialog.setLocationRelativeTo(frame);
	        		        dialog.setVisible(true);
	        			}
						Game.progressToNextMonth();
		    			changeWindow(GamePanel.GAME_WINDOW_RECRUIT);
		        		gamePanel.setWindowMessage();
					} else {
						Game.createBattleGroups();
						Game.currentBattleIdx = 0;
						Game.setupMap(Game.battleList.get(Game.currentBattleIdx));
					}
				}
			}
		}
		if (e.getActionCommand().equals(GamePanel.PLAYER_CHANGE_START_FIGHT_BUTTON_NAME)){
			Game.currentTeam = Player.getPlayers().get(0).controlledTeam;
			Game.currentGladiator = Game.currentTeam.gladiators.get(0);
			Game.currentTeam.battleGladiators = new Gladiator[Constants.MAX_GLADIATORS_IN_BATTLE];
			if(Game.getNextOpponentForTeam(Game.currentTeam) != null) {
				if(Game.currentRecruitRound > Constants.RECRUIT_TURNS){
					changeWindow(GamePanel.GAME_WINDOW_BATTLE_SETUP);
					gamePanel.setWindowMessage();
				} else {
					String txt = getAsOneString("T�m�n kuukauden V�RV�YSVAIHE on viel� kesken.", "", "Haluavatko pelaajat J�TT�� K�YTT�M�TT� t�m�n kuukauden", "v�rv�yksens�?");
					Object[] options = {"Kyll�", "Ei"};
					int n = JOptionPane.showOptionDialog(frame,
									txt,
									Constants.GAME_NAME,
					                JOptionPane.YES_NO_OPTION,
					                JOptionPane.QUESTION_MESSAGE,
					                null,
					                options,
					                options[0]);
					if (n == JOptionPane.YES_OPTION) {
						Game.currentRecruitTeamIdx = 1;
						Game.currentRecruitRound = Constants.RECRUIT_TURNS + 1;
						changeWindow(GamePanel.GAME_WINDOW_BATTLE_SETUP);
						gamePanel.setWindowMessage();
					}
				}
			} else {
				if(Game.monthType[Game.MONTH] == Game.MONTH_TYPE_WINTER_BREAK){
					String txt = getAsOneString("Gladiaattoreille ei makseta palkkaa talvitauon aikana.");
    				JOptionPane.showMessageDialog(frame, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE);
    				if(Game.MONTH == 11){
        				txt = getAsOneString("Vuoden "+Game.YEAR+" lopulliset sarjataulukot...");
        				JOptionPane.showMessageDialog(frame, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE);
        				dialog = new JDialog();
        				dialog.setTitle("VUODEN "+Game.YEAR+" SARJATAULUKOT");
        				dialog.setIconImage(ImageHandler.gladiatorImages[11]);
        		        dialog.setModal(true);
        		        dialog.add(standingsPanel);
        		        dialog.pack();
        		        dialog.setLocationRelativeTo(frame);
        		        dialog.setVisible(true);
        			}
					Game.progressToNextMonth();
	    			changeWindow(GamePanel.GAME_WINDOW_RECRUIT);
	        		gamePanel.setWindowMessage();
				} else {
					Game.createBattleGroups();
					Game.currentBattleIdx = 0;
					Game.setupMap(Game.battleList.get(Game.currentBattleIdx));
				}
			}
		}
		if (e.getActionCommand().equals(GamePanel.OTHER_TEAMS_TXT)){
			gamePanel.addTeamsToTable();
			changeWindow(GamePanel.GAME_WINDOW_OTHER_TEAMS);
			gamePanel.setWindowMessage();
		}
		if (e.getActionCommand().equals(GamePanel.SKIP_OFFER_TXT)){
			/*
			Game.nextRecruitTurn();
			gamePanel.refresh();
			gamePanel.updateButtons();
			*/
			client.packetSender.sendOfferRecruitPacket(-1, 0);
		}
		if (e.getActionCommand().equals(GamePanel.OFFER_BUTTON_NAME)){
			Gladiator gladiatorToRecruit = gamePanel.recruitGladiator();
			if(gladiatorToRecruit != null){
				if(gladiatorToRecruit.offerTeam == Game.currentTeam)
					return;
				int minRecruitPrice = gladiatorToRecruit.askPrice;
				if(gladiatorToRecruit.offerTeam != null){
					minRecruitPrice = gladiatorToRecruit.offerPrice + 10;
				}
				if(Game.currentTeam.getGladiatorRaces().contains(gladiatorToRecruit.race)){
					String txt = getAsOneString("Gladiaattorien Liiton s��nt�jen mukaan joukkueessa ei saa olla kahta","saman rodun edustajaa.");
					JOptionPane.showMessageDialog(frame, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE);
					return;
				}
				if(Game.currentTeam.money >= minRecruitPrice){
					String txt = getAsOneString(gladiatorToRecruit.name+" vaatii v�hint��n "+minRecruitPrice+" "+Constants.MONEY_SUFFIX+" allekirjoituspalkkiota.", "", "Paljonko tarjoat? ("+minRecruitPrice+" "+Constants.MONEY_SUFFIX+" - "+Game.currentTeam.money+" "+Constants.MONEY_SUFFIX+")");
					String input = (String) JOptionPane.showInputDialog(frame, txt, Game.currentTeam.name, JOptionPane.PLAIN_MESSAGE, null, null, minRecruitPrice);
					if(input != null){
						try{
							int inputVal = Integer.parseInt(input);
							if(inputVal >= minRecruitPrice && inputVal <= Game.currentTeam.money){
								gamePanel.offerRecruitAmount(inputVal);
								gamePanel.refresh();
								gamePanel.updateButtons();
							}
						}catch(Exception ex){
							//ex.printStackTrace();
						}
					}
				} else {
					String txt = getAsOneString(gladiatorToRecruit.name+" vaatii v�hint��n "+minRecruitPrice+" "+Constants.MONEY_SUFFIX+" allekirjoituspalkkiota.", "", "Sinulla ei ole tarpeeksi rahaa tarjouksen tekemiseen.");
					JOptionPane.showMessageDialog(frame, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE);
				}
			}
		}
		if (e.getActionCommand().equals(GamePanel.BUY_ITEM_TXT)){
			Item item = gamePanel.buyItem();
			int itemSlot = -1;
			if(item != null){
				Item ownedItem = null;
				int ownedItemSellPrice = 0;
				if(item.type == Item.TYPE_MELEE_WEAPON){
					itemSlot = Constants.MELEE_WEAPON_SLOT;
				}
				else if(item.type == Item.TYPE_RANGED_WEAPON){
					itemSlot = Constants.RANGED_WEAPON_SLOT;
				}
				else if(item.type == Item.TYPE_ARMOUR){
					itemSlot = Constants.ARMOUR_SLOT;
				}
				if(Game.currentGladiator.items[itemSlot] != null){
					ownedItem = Game.currentGladiator.items[itemSlot];
					ownedItemSellPrice = ownedItem.getSellPrice();
				}
				int buyPrice = item.getCurrentPrice()-ownedItemSellPrice;
				if(Game.currentTeam.money >= buyPrice){
					String txt = getAsOneString(item.name+" maksaa "+item.getCurrentPrice()+" kultarahaa.", "", "Ostatko tavaran?");
					if(ownedItem != null){
						if(buyPrice > 0)
							txt = getAsOneString(item.name+" maksaa "+item.getCurrentPrice()+" kultarahaa.", "", "Kauppias tarjoaa tavarasta nimelt� "+ownedItem.name+" vaihdossa "+ownedItemSellPrice, "kultarahaa.", "", "Joutuisit maksamaan "+buyPrice+" kultarahaa.", "", "Ostatko tavaran?");
						else
							txt = getAsOneString(item.name+" maksaa "+item.getCurrentPrice()+" kultarahaa.", "", "Kauppias tarjoaa tavarasta nimelt� "+ownedItem.name+" vaihdossa "+ownedItemSellPrice, "kultarahaa.", "", "Kauppias joutuisi maksamaan "+buyPrice+" kultarahaa.", "", "Ostatko tavaran?");
					}
					Object[] options = {"Kyll�", "Ei"};
					int n = JOptionPane.showOptionDialog(frame,
									txt,
									Game.currentGladiator.name,
					                JOptionPane.YES_NO_OPTION,
					                JOptionPane.QUESTION_MESSAGE,
					                null,
					                options,
					                options[0]);
					if (n == JOptionPane.YES_OPTION) {
						Game.buyItem(item);
						gamePanel.refresh();
						gamePanel.updateButtons();
					} 
				} else {
					String txt = getAsOneString(item.name+" maksaa "+item.getCurrentPrice()+" kultarahaa.", "", "Rahat eiv�t riit�.");
					JOptionPane.showMessageDialog(frame, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE);
				}
			}
		}
		if (e.getActionCommand().equals(GamePanel.BUY_SPELL_TXT)){
			Spell spell = gamePanel.buySpell();
			if(spell != null){
				int buyPrice = spell.price;
				if(Game.currentGladiator.getKnownSpells().size() >= Constants.MAX_SPELLS){
					String txt = getAsOneString("Gladiaattori voi osata korkeintaan "+Constants.MAX_SPELLS+" loitsua.");
					JOptionPane.showMessageDialog(frame, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE);
					return;
				}
				if(Game.currentGladiator.getKnownSpells().contains(spell)){
					String txt = getAsOneString(Game.currentGladiator.name+" osaa loitsun ennest��n!");
					JOptionPane.showMessageDialog(frame, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE);
					return;
				}
				if(Game.currentTeam.money >= buyPrice){
					String txt = getAsOneString("Haluaako "+Game.currentGladiator.name+" ostaa loitsun nimelt� "+spell.name+" hintaan "+spell.price, "kr?");
					Object[] options = {"Kyll�", "Ei"};
					int n = JOptionPane.showOptionDialog(frame,
									txt,
									Game.currentGladiator.name,
					                JOptionPane.YES_NO_OPTION,
					                JOptionPane.QUESTION_MESSAGE,
					                null,
					                options,
					                options[0]);
					if (n == JOptionPane.YES_OPTION) {
						Game.buySpell(spell);
						gamePanel.refresh();
						gamePanel.updateButtons();
					} 
				} else {
					String txt = getAsOneString("Rahat eiv�t riit�.");
					JOptionPane.showMessageDialog(frame, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE);
				}
			}
		}
		if (e.getActionCommand().equals(GamePanel.TRAIN_STR_TXT) || e.getActionCommand().equals(GamePanel.TRAIN_SPEED_TXT)
				|| e.getActionCommand().equals(GamePanel.TRAIN_MANA_TXT) || e.getActionCommand().equals(GamePanel.TRAIN_HP_TXT)){
			String abilityName = Game.getAbilityName(e.getActionCommand());
			int trainCost = Game.getTrainCost(e.getActionCommand());
			if(Game.currentTeam.money >= trainCost){
				String txt = getAsOneString("Gladiaattorin nimelt� "+Game.currentGladiator.name+" ominaisuuden "+abilityName, "kouluttaminen tulisi maksamaan "+trainCost+" "+Constants.MONEY_SUFFIX+".", "", "Ryhdytk� koulutukseen?");
				Object[] options = {"Kyll�", "Ei"};
				int n = JOptionPane.showOptionDialog(frame,
								txt,
								Game.currentGladiator.name,
				                JOptionPane.YES_NO_OPTION,
				                JOptionPane.QUESTION_MESSAGE,
				                null,
				                options,
				                options[0]);
				if (n == JOptionPane.YES_OPTION) {
					gamePanel.trainSkill(e.getActionCommand());
					gamePanel.refresh();
					gamePanel.updateButtons();
				}
			} else {
				String txt = getAsOneString("Gladiaattorin nimelt� "+Game.currentGladiator.name+" ominaisuuden "+abilityName, "kouluttaminen tulisi maksamaan "+trainCost+" "+Constants.MONEY_SUFFIX+".", "", "Rahat eiv�t riit�.");
				JOptionPane.showMessageDialog(frame, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE);
			}
		}
		if (e.getActionCommand().equals(GamePanel.TRAINING_TXT)){
			changeWindow(GamePanel.GAME_WINDOW_SCHOOL);
			GamePanel.setMessage("Koulussa tehd��n selv�ksi, ettei gladiaattoreiden ominaisuuksia voi kouluttaa yli rodun",
					"maksimiarvojen.");
		}
		if (e.getActionCommand().equals(GamePanel.MAGE_SHOP_TXT)){
			changeWindow(GamePanel.GAME_WINDOW_MAGE);
			GamePanel.setMessage("Gladiaattori voi halutessaan unohtaa opettelemansa loitsun (Gladiaattori-menu).");
		}
		if (e.getActionCommand().equals(GamePanel.SHOP_TXT) || e.getActionCommand().equals(GamePanel.DISCOUNT_TXT)){
			gamePanel.addItemsToTable(GamePanel.DISCOUNT_TABLE, GamePanel.DISCOUNT_TXT);
			changeWindow(GamePanel.GAME_WINDOW_SHOP, GamePanel.SUB_GAME_WINDOW_SHOP_DISCOUNT);
			GamePanel.setMessage("Tarjouksessa (10%) olevat varusteet on merkitty listoissaan T:ll�.");
		}
		if (e.getActionCommand().equals(GamePanel.SPEAR_TXT)){
			gamePanel.addItemsToTable(GamePanel.MELEE_TABLE, GamePanel.SPEAR_TXT);
			changeWindow(GamePanel.GAME_WINDOW_SHOP, GamePanel.SUB_GAME_WINDOW_SHOP_MELEE);
			GamePanel.setMessage("Keih��t ovat halpoja, mutta niiden iskut eiv�t t�n�ise kohdetta.");
		}
		if (e.getActionCommand().equals(GamePanel.SWORD_TXT)){
			gamePanel.addItemsToTable(GamePanel.MELEE_TABLE, GamePanel.SWORD_TXT);
			changeWindow(GamePanel.GAME_WINDOW_SHOP, GamePanel.SUB_GAME_WINDOW_SHOP_MELEE);
			GamePanel.setMessage("Miekan isku voi t�n�ist� jonkin verran.");
		}
		if (e.getActionCommand().equals(GamePanel.AXE_TXT)){
			gamePanel.addItemsToTable(GamePanel.MELEE_TABLE, GamePanel.AXE_TXT);
			changeWindow(GamePanel.GAME_WINDOW_SHOP, GamePanel.SUB_GAME_WINDOW_SHOP_MELEE);
			GamePanel.setMessage("Kunnon kirveenisku t�n�isee vastustajan kauaksi.");
		}
		if (e.getActionCommand().equals(GamePanel.MACE_TXT)){
			gamePanel.addItemsToTable(GamePanel.MELEE_TABLE, GamePanel.MACE_TXT);
			changeWindow(GamePanel.GAME_WINDOW_SHOP, GamePanel.SUB_GAME_WINDOW_SHOP_MELEE);
			GamePanel.setMessage("Nuija t�n�isee aseista parhaiten, mutta mokomat ovat melko kalliita.");
		}
		if (e.getActionCommand().equals(GamePanel.BOW_TXT)){
			gamePanel.addItemsToTable(GamePanel.RANGED_TABLE, GamePanel.BOW_TXT);
			changeWindow(GamePanel.GAME_WINDOW_SHOP, GamePanel.SUB_GAME_WINDOW_SHOP_RANGED);
			GamePanel.setMessage("Jousella voi ampua joka vuoro, niin kauan kuin ammuksia (20 kpl) riitt��.");
		}
		if (e.getActionCommand().equals(GamePanel.CROSSBOW_TXT)){
			gamePanel.addItemsToTable(GamePanel.RANGED_TABLE, GamePanel.CROSSBOW_TXT);
			changeWindow(GamePanel.GAME_WINDOW_SHOP, GamePanel.SUB_GAME_WINDOW_SHOP_RANGED);
			GamePanel.setMessage("Varsijousi t�ytyy ladata, jotta sill� voi ampua toistamiseen.");
		}
		if (e.getActionCommand().equals(GamePanel.ARMOUR_TXT)){
			gamePanel.addItemsToTable(GamePanel.ARMOUR_TABLE, GamePanel.ARMOUR_TXT);
			changeWindow(GamePanel.GAME_WINDOW_SHOP, GamePanel.SUB_GAME_WINDOW_SHOP_ARMOUR);
			GamePanel.setMessage("Kunnon panssarit pit�v�t joukkueen ja gladiaattorit koossa.");
		}
		if (e.getActionCommand().equals(GamePanel.BACK_TO_MAIN_MENU_TXT) || e.getActionCommand().equals(GamePanel.EXIT_RECRUIT_TXT)
				|| e.getActionCommand().equals(GamePanel.EXIT_OTHER_TEAM_BUTTON_NAME)){
			changeWindow(GamePanel.GAME_WINDOW_MAIN_MENU);
			gamePanel.setWindowMessage();
		}
		if (e.getActionCommand().equals(GamePanel.EXIT_SCHOOL_TXT) || e.getActionCommand().equals(GamePanel.EXIT_MAGE_BUTTON_NAME)
				|| e.getActionCommand().equals(GamePanel.EXIT_SHOP_TXT)){
			changeWindow(GamePanel.GAME_WINDOW_MAIN_MENU);
			gamePanel.setWindowMessage();
		}
		if (e.getActionCommand().equals(GamePanel.CHANGE_TO_P1_BUTTON_NAME)){
			Game.currentTeam = Player.getPlayers().get(0).controlledTeam;
			changeWindow(GamePanel.GAME_WINDOW_MAIN_MENU);
			gamePanel.refresh();
			gamePanel.setWindowMessage();
		}
		if (e.getActionCommand().equals(GamePanel.CHANGE_TO_P2_BUTTON_NAME)){
			Game.currentTeam = Player.getPlayers().get(1).controlledTeam;
			changeWindow(GamePanel.GAME_WINDOW_MAIN_MENU);
			gamePanel.refresh();
			gamePanel.setWindowMessage();
		}
		if (e.getActionCommand().equals(GamePanel.CHANGE_TO_P3_BUTTON_NAME)){
			Game.currentTeam = Player.getPlayers().get(2).controlledTeam;
			changeWindow(GamePanel.GAME_WINDOW_MAIN_MENU);
			gamePanel.refresh();
			gamePanel.setWindowMessage();
		}
		if (e.getActionCommand().equals(GamePanel.CHANGE_TO_P4_BUTTON_NAME)){
			Game.currentTeam = Player.getPlayers().get(3).controlledTeam;
			changeWindow(GamePanel.GAME_WINDOW_MAIN_MENU);
			gamePanel.refresh();
			gamePanel.setWindowMessage();
		}
		if (e.getActionCommand().equals(GamePanel.CLEAR_FORMATION_BUTTON_NAME)){
			Game.currentTeam.battleGladiators = new Gladiator[Constants.MAX_GLADIATORS_IN_BATTLE];
			frame.validate();
			frame.repaint();
		}
		if (e.getActionCommand().equals(GamePanel.AUTO_FORMATION_BUTTON_NAME)){
			Game.createAutoBattleTeam();
			frame.validate();
			frame.repaint();
		}
		if (e.getActionCommand().equals(GamePanel.RECRUIT_BUTTON_NAME)){
			changeWindow(GamePanel.GAME_WINDOW_RECRUIT);
			gamePanel.setWindowMessage();
		}
		if (e.getActionCommand().equals(GamePanel.BEGIN_FIGHT_TXT)){
			if(Game.currentTeam.getBattleTeamSize() == 0){
				String txt = getAsOneString("Taisteluun t�ytyy valita v�hint��n yksi gladiaattori!");
				JOptionPane.showMessageDialog(frame, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE);
				return;
			}
			client.packetSender.sendBattleReadyPacket();
			/*
			if(Game.currentTeam.getHealthyGladiatorCount() == 0){
				String txt = getAsOneString("Joudut luovuttamaan ottelun, koska kaikki gladiaattorisi ovat", "loukkaantuneet!");
				JOptionPane.showMessageDialog((Component) null, txt, Game.currentTeam.name, JOptionPane.PLAIN_MESSAGE);
				txt = getAsOneString(Game.currentTeam.name+" joutui luovuttamaan ottelunsa joukkuetta", Game.getNextOpponentForTeam(Game.currentTeam).name+" vastaan, koska ei saanut yht��n gladiaattoria jalkeille!");
				JOptionPane.showMessageDialog((Component) null, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE);
			}
			else if(Game.currentTeam.getBattleTeamSize() == 0){
				String txt = getAsOneString("Taisteluun t�ytyy valita v�hint��n yksi gladiaattori!");
				JOptionPane.showMessageDialog((Component) null, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE);
				return;
			}
			if(Player.getPlayers().size() > 1){
				int curPlayerIdx = -1;
				for(int i = 0; i < Player.getPlayers().size(); i++){
					Player player = Player.getPlayers().get(i);
					if(Game.currentTeam.controllingPlayer == player){
						curPlayerIdx = i;
						break;
					}
				}
				if(curPlayerIdx < Player.getPlayers().size()-1){
					curPlayerIdx++;
					Game.currentTeam = Player.getPlayers().get(curPlayerIdx).controlledTeam;
					Game.currentGladiator = Game.currentTeam.gladiators.get(0);
					Game.currentTeam.battleGladiators = new Gladiator[Constants.MAX_GLADIATORS_IN_BATTLE];
					if(Game.getNextOpponentForTeam(Game.currentTeam) != null) {
						changeWindow(GamePanel.GAME_WINDOW_BATTLE_SETUP);
						return;
					}
				}
			}
			Game.currentTeam = Player.getPlayers().get(0).controlledTeam;
			Game.currentGladiator = Game.currentTeam.gladiators.get(0);
			Game.createBattleGroups();
			Game.currentBattleIdx = 0;
			Game.setupMap(Game.battleList.get(Game.currentBattleIdx));
			changeWindow(GamePanel.GAME_WINDOW_BATTLE);
			GamePanel.setMessage("Paina OK aloittaaksesi taistelun!",
					"",
					"Taisteluun osallistuvat joukkueet saavat lipputuloja 191 kultarahaa.",
					"",
					"Yleis�n mielenkiintoa v�hent�� taisteluun osallistuvien gladiaattorien V�H�INEN m��r�.",
					"",
					"Rotujen jumalien v�lisess� valtataistelussa voitokas rotu on MUUMIO.");
			*/
		}
		if (e.getActionCommand().equals(GamePanel.OK_BATTLE_BUTTON_NAME)){
			Game.currentBattle.confirmBattle();
			client.packetSender.sendBattleStartPacket();
			GamePanel.setMessage("Taistelu alkaa!");
			/*
			if(!Game.currentBattle.battleStarted){
				Game.currentBattle.confirmBattle();
				GamePanel.MESSAGES = new String[]{"Taistelu alkaa!"};
			} else {
				Game.currentBattle.startNewBattleTurn();
			}
			frame.validate();
			frame.repaint();
			*/
			gamePanel.updateButtons();
			frame.validate();
			frame.repaint();
		}
		if (e.getActionCommand().equals(GamePanel.AUTO_FORMATION_BUTTON_NAME)){
			Game.createAutoBattleTeam();
			frame.validate();
			frame.repaint();
		}
		if(Game.currentBattle == null && Game.currentGladiator != null && Game.currentGladiator.owner == Game.currentTeam){
			if (e.getActionCommand().equals(GamePanel.CURRENT_GLADIATOR_MELEE_WEAPON_BUTTON_NAME)){
				if(Game.currentGladiator.items[Constants.MELEE_WEAPON_SLOT] != null){
					Item item = Game.currentGladiator.items[Constants.MELEE_WEAPON_SLOT];
					String txt = getAsOneString("Aselaji : "+item.getSubTypeName(), "Vahinko : "+item.dmg+" pistett�", "Hy�kk�ysbonus (HB) : "+item.hb_val, "Puolustusbonus (PB) : "+item.pb_val, "", "Hinta : "+item.price+" "+Constants.MONEY_SUFFIX, "", "HALUATKO MYYD� VARUSTEEN?");
					Object[] options = {"Kyll�", "Ei"};
					int n = JOptionPane.showOptionDialog(frame,
									txt,
									item.name,
					                JOptionPane.YES_NO_OPTION,
					                JOptionPane.QUESTION_MESSAGE,
					                null,
					                options,
					                options[0]);
					if (n == JOptionPane.YES_OPTION) {
						txt = getAsOneString("Kauppias tarjoaa tavarasta nimelt� "+item.name+" "+item.getSellPrice()+" kultarahaa.", "Hyv�ksytk� tarjouksen?");
						n = JOptionPane.showOptionDialog(frame,
								txt,
								item.name,
				                JOptionPane.YES_NO_OPTION,
				                JOptionPane.QUESTION_MESSAGE,
				                null,
				                options,
				                options[0]);
						if (n == JOptionPane.YES_OPTION) {
							Game.sellItem(Constants.MELEE_WEAPON_SLOT);
							gamePanel.refresh();
							gamePanel.updateButtons();
						}
					}
				} else {
					String txt = getAsOneString("Aselaji : Nyrkki", "Vahinko : 1d2 pistett�", "Hy�kk�ysbonus (HB) : 0", "Puolustusbonus (PB) : 0", "", "Nyrkill� torjuntaprosentti on 10 eik� se kehity.");
					JOptionPane.showMessageDialog(frame, txt, "Nyrkki", JOptionPane.PLAIN_MESSAGE);
				}
			}
			if (e.getActionCommand().equals(GamePanel.CURRENT_GLADIATOR_RANGED_WEAPON_BUTTON_NAME)){
				if(Game.currentGladiator.items[Constants.RANGED_WEAPON_SLOT] != null){
					Item item = Game.currentGladiator.items[Constants.RANGED_WEAPON_SLOT];
					String txt = getAsOneString("Aselaji : "+item.getSubTypeName(), "Vahinko : "+item.dmg+" pistett�", "Hinta : "+item.price+" "+Constants.MONEY_SUFFIX, "", "Jousella voi ampua joka vuoro.", "", "HALUATKO MYYD� VARUSTEEN?");
					if(item.subtype == Item.SUBTYPE_RANGED_WEAPON_CROSSBOW) {
						txt = getAsOneString("Aselaji : "+item.getSubTypeName(), "Vahinko : "+item.dmg+" pistett�", "Hinta : "+item.price+" "+Constants.MONEY_SUFFIX, "", "Varsijousi t�ytyy ladata laukauksen j�lkeen (kest�� vuoron)", "ennen kuin sill� voi ampua uudestaan.", "", "HALUATKO MYYD� VARUSTEEN?");
					}
					Object[] options = {"Kyll�", "Ei"};
					int n = JOptionPane.showOptionDialog(frame,
									txt,
									item.name,
					                JOptionPane.YES_NO_OPTION,
					                JOptionPane.QUESTION_MESSAGE,
					                null,
					                options,
					                options[0]);
					if (n == JOptionPane.YES_OPTION) {
						txt = getAsOneString("Kauppias tarjoaa tavarasta nimelt� "+item.name+" "+item.getSellPrice()+" kultarahaa.", "Hyv�ksytk� tarjouksen?");
						n = JOptionPane.showOptionDialog(frame,
								txt,
								item.name,
				                JOptionPane.YES_NO_OPTION,
				                JOptionPane.QUESTION_MESSAGE,
				                null,
				                options,
				                options[0]);
						if (n == JOptionPane.YES_OPTION) {
							Game.sellItem(Constants.RANGED_WEAPON_SLOT);
							gamePanel.refresh();
							gamePanel.updateButtons();
						}
					}
				}
			}
			if (e.getActionCommand().equals(GamePanel.CURRENT_GLADIATOR_ARMOR_BUTTON_NAME)){
				if(Game.currentGladiator.items[Constants.ARMOUR_SLOT] != null){
					Item item = Game.currentGladiator.items[Constants.ARMOUR_SLOT];
					String txt = getAsOneString("Suojaa : "+item.pp_val+" PP", "Hinta : "+item.price+" "+Constants.MONEY_SUFFIX, "", "HALUATKO MYYD� VARUSTEEN?");
					Object[] options = {"Kyll�", "Ei"};
					int n = JOptionPane.showOptionDialog(frame,
									txt,
									item.name,
					                JOptionPane.YES_NO_OPTION,
					                JOptionPane.QUESTION_MESSAGE,
					                null,
					                options,
					                options[0]);
					if (n == JOptionPane.YES_OPTION) {
						txt = getAsOneString("Kauppias tarjoaa tavarasta nimelt� "+item.name+" "+item.getSellPrice()+" kultarahaa.", "Hyv�ksytk� tarjouksen?");
						n = JOptionPane.showOptionDialog(frame,
								txt,
								item.name,
				                JOptionPane.YES_NO_OPTION,
				                JOptionPane.QUESTION_MESSAGE,
				                null,
				                options,
				                options[0]);
						if (n == JOptionPane.YES_OPTION) {
							Game.sellItem(Constants.ARMOUR_SLOT);
							gamePanel.refresh();
							gamePanel.updateButtons();
						}
					}
				}
			}
		}
		
		if(shootMode == 0 && Game.currentBattle != null){
			if (e.getActionCommand().equals(GamePanel.SKIP_ROUND)){
				/*
				Game.autoRound = false;
				Game.currentBattle.startNewBattleTurn();
				*/
				client.packetSender.sendBattleMovementPacket(0);
				frame.validate();
				frame.repaint();
			}
			if (e.getActionCommand().equals(GamePanel.AUTO_ROUND)){
				/*
				int currentRound = Game.currentBattle.round;
				Game.autoRound = true;
				while(Game.currentBattle != null && currentRound == Game.currentBattle.round){
					Game.currentBattle.playAIBattleTurn(Game.currentBattle.getCurrentBattleGladiator());
				}
				*/
				client.packetSender.sendBattleMovementPacket(1);
				frame.validate();
				frame.repaint();
			}
			if (e.getActionCommand().equals(GamePanel.BATTLE_DIR_BUTTON_NW)){
				/*
				if(Game.currentBattle.moveGladiator("NW")){
					Game.autoRound = false;
					if(Game.currentBattle.showBattleButtons)
						Game.currentBattle.startNewBattleTurn();
					frame.validate();
					frame.repaint();
				}
				*/
				client.packetSender.sendBattleMovementPacket(2);
				frame.validate();
				frame.repaint();
			}
			if (e.getActionCommand().equals(GamePanel.BATTLE_DIR_BUTTON_N)){
				/*
				if(Game.currentBattle.moveGladiator("N")){
					Game.autoRound = false;
					if(Game.currentBattle.showBattleButtons)
						Game.currentBattle.startNewBattleTurn();
					frame.validate();
					frame.repaint();
				}
				*/
				client.packetSender.sendBattleMovementPacket(3);
				frame.validate();
				frame.repaint();
			}
			if (e.getActionCommand().equals(GamePanel.BATTLE_DIR_BUTTON_NE)){
				/*
				if(Game.currentBattle.moveGladiator("NE")){
					Game.autoRound = false;
					if(Game.currentBattle.showBattleButtons)
						Game.currentBattle.startNewBattleTurn();
					frame.validate();
					frame.repaint();
				}
				*/
				client.packetSender.sendBattleMovementPacket(4);
				frame.validate();
				frame.repaint();
			}
			if (e.getActionCommand().equals(GamePanel.BATTLE_DIR_BUTTON_E)){
				/*
				if(Game.currentBattle.moveGladiator("E")){
					Game.autoRound = false;
					if(Game.currentBattle.showBattleButtons)
						Game.currentBattle.startNewBattleTurn();
					frame.validate();
					frame.repaint();
				}
				*/
				client.packetSender.sendBattleMovementPacket(5);
				frame.validate();
				frame.repaint();
			}
			if (e.getActionCommand().equals(GamePanel.BATTLE_DIR_BUTTON_SE)){
				/*
				if(Game.currentBattle.moveGladiator("SE")){
					Game.autoRound = false;
					if(Game.currentBattle.showBattleButtons)
						Game.currentBattle.startNewBattleTurn();
					frame.validate();
					frame.repaint();
				}
				*/
				client.packetSender.sendBattleMovementPacket(6);
				frame.validate();
				frame.repaint();
			}
			if (e.getActionCommand().equals(GamePanel.BATTLE_DIR_BUTTON_S)){
				/*
				if(Game.currentBattle.moveGladiator("S")){
					Game.autoRound = false;
					if(Game.currentBattle.showBattleButtons)
						Game.currentBattle.startNewBattleTurn();
					frame.validate();
					frame.repaint();
				}
				*/
				client.packetSender.sendBattleMovementPacket(7);
				frame.validate();
				frame.repaint();
			}
			if (e.getActionCommand().equals(GamePanel.BATTLE_DIR_BUTTON_SW)){
				/*
				if(Game.currentBattle.moveGladiator("SW")){
					Game.autoRound = false;
					if(Game.currentBattle.showBattleButtons)
						Game.currentBattle.startNewBattleTurn();
					frame.validate();
					frame.repaint();
				}
				*/
				client.packetSender.sendBattleMovementPacket(8);
				frame.validate();
				frame.repaint();
			}
			if (e.getActionCommand().equals(GamePanel.BATTLE_DIR_BUTTON_W)){
				/*
				if(Game.currentBattle.moveGladiator("W")){
					Game.autoRound = false;
					if(Game.currentBattle.showBattleButtons)
						Game.currentBattle.startNewBattleTurn();
					frame.validate();
					frame.repaint();
				}
				*/
				client.packetSender.sendBattleMovementPacket(9);
				frame.validate();
				frame.repaint();
			}	
			if (e.getActionCommand().equals(GamePanel.SHOOT_BUTTON_NAME)){
				Gladiator gladiator = Game.currentBattle.getCurrentBattleGladiator();
				if(gladiator.ammo <= 0){
					return;
				}
				if(gladiator.needsReload){
					Game.currentBattle.reloadCrossbow();
					frame.validate();
					frame.repaint();
					return;
				}
				setShootMode(Constants.SHOOT_MODE_RANGED_WEAPON);
			}
			if (e.getActionCommand().equals(GamePanel.BATTLE_SPELL_BUTTON_NAME_1)){
				if(Game.currentBattle.doSpellCast(0)){
					setShootMode(Constants.SHOOT_MODE_SPELL);
				} else {
					frame.validate();
					frame.repaint();
				}
			}
			if (e.getActionCommand().equals(GamePanel.BATTLE_SPELL_BUTTON_NAME_2)){
				if(Game.currentBattle.doSpellCast(1)){
					setShootMode(Constants.SHOOT_MODE_SPELL);
				} else {
					frame.validate();
					frame.repaint();
				}
			}
			if (e.getActionCommand().equals(GamePanel.BATTLE_SPELL_BUTTON_NAME_3)){
				if(Game.currentBattle.doSpellCast(2)){
					setShootMode(Constants.SHOOT_MODE_SPELL);
				} else {
					frame.validate();
					frame.repaint();
				}
			}
			if (e.getActionCommand().equals(GamePanel.BATTLE_SPELL_BUTTON_NAME_4)){
				if(Game.currentBattle.doSpellCast(3)){
					setShootMode(Constants.SHOOT_MODE_SPELL);
				} else {
					frame.validate();
					frame.repaint();
				}
			}
		}
		if (e.getActionCommand().equals(GamePanel.EXIT_GAME_TXT)){
			System.exit(0);
		}
		frame.requestFocus();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println("Mouse clicked: "+e.getX()+","+e.getY());
		if(gamePanel.GAME_WINDOW_CURRENT != GamePanel.GAME_WINDOW_TITLESCREEN){
			gamePanel.checkIfClickedHero(new Position(e.getX(), e.getY()));
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		//System.out.println("Mouse clicked: "+e.getX()+","+e.getY());
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		//System.out.println("Mouse clicked: "+e.getX()+","+e.getY());
	}

	@Override
	public void mousePressed(MouseEvent e) {
		System.out.println("Mouse pressed: "+e.getX()+","+e.getY());
		if(shootMode == 0){
			if(gamePanel.GAME_WINDOW_CURRENT != GamePanel.GAME_WINDOW_TITLESCREEN){
				Gladiator gladiator = gamePanel.checkIfPressedHero(new Position(e.getX(), e.getY()));
				if(gladiator != null){
					if(gladiator.hurt == 0)
						setDraggedGladiator(gladiator, true);
				}
			}
			if(gamePanel.GAME_WINDOW_CURRENT == GamePanel.GAME_WINDOW_BATTLE_SETUP){
				Gladiator gladiator = gamePanel.checkIfPressedHeroInBattleFormationSlot(new Position(e.getX(), e.getY()));
				if(gladiator != null){
					setDraggedGladiator(gladiator, false);
				}
			}
			if(gamePanel.GAME_WINDOW_CURRENT == GamePanel.GAME_WINDOW_BATTLE){
				Gladiator gladiator = gamePanel.checkIfPressedBattleHero(new Position(e.getX(), e.getY()));
				if(gladiator != null){
					if(gladiator.owner == Game.currentTeam){
						Game.currentGladiator = gladiator;
					} else {
						Game.currentGladiator = gladiator;
					}
					frame.validate();
					frame.repaint();
				}
			}
		} else if(shootMode == Constants.SHOOT_MODE_RANGED_WEAPON){
			if(gamePanel.GAME_WINDOW_CURRENT == GamePanel.GAME_WINDOW_BATTLE){
				Gladiator gladiator = gamePanel.checkIfPressedBattleHero(new Position(e.getX(), e.getY()));
				if(gladiator != null){
					if(gladiator.owner == Game.currentBattle.getCurrentBattleGladiator().owner){
						GamePanel.setMessage(Game.currentBattle.getCurrentBattleGladiator().name+" kielt�ytyy jyrk�sti ampumasta joukkuetoveriaan.");
						setShootMode(0);
						return;
					}
					if(gladiator.hp <= 0){
						GamePanel.setMessage("Gladiaattorien Liiton s��nn�t kielt�v�t tajuttomien ampumisen.");
						setShootMode(0);
						return;
					}
					Game.autoRound = false;
					Game.rangedAtk(Game.currentBattle.getCurrentBattleGladiator(), gladiator);
					if(Game.currentBattle.showBattleButtons)
						Game.currentBattle.startNewBattleTurn();
					setShootMode(0);
				}
			}
		} else if(shootMode == Constants.SHOOT_MODE_SPELL){
			if(gamePanel.GAME_WINDOW_CURRENT == GamePanel.GAME_WINDOW_BATTLE){
				Gladiator gladiator = gamePanel.checkIfPressedBattleHero(new Position(e.getX(), e.getY()));
				if(gladiator != null){
					if(gladiator.owner == Game.currentBattle.getCurrentBattleGladiator().owner && !Game.currentShootSpell.isFriendlySpell()){
						String txt = getAsOneString("Mutta seh�n on joukkuetoveri.");
						JOptionPane.showMessageDialog(frame, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE);
						return;
					}
					if(gladiator.owner != Game.currentBattle.getCurrentBattleGladiator().owner && Game.currentShootSpell.isFriendlySpell()){
						String txt = getAsOneString("Mutta seh�n on vastustaja.");
						JOptionPane.showMessageDialog(frame, txt, Constants.GAME_NAME, JOptionPane.PLAIN_MESSAGE);
						return;
					}
					if(gladiator.hp <= 0){
						GamePanel.setMessage("Gladiaattorien Liiton s��nn�t kielt�v�t tajuttomien ampumisen.");
						setShootMode(0);
						return;
					}
					Game.autoRound = false;
					Game.spellCast(Game.currentBattle.getCurrentBattleGladiator(), gladiator, Game.currentShootSpell);
					if(Game.currentBattle.showBattleButtons)
						Game.currentBattle.startNewBattleTurn();
					setShootMode(0);
				}
			}
		}
	}
	
	public Gladiator draggedGladiator = null;
	public boolean draggedToTeam = false;
	
	public void setDraggedGladiator(Gladiator gladiator, boolean draggedToTeam_){
		draggedGladiator = gladiator;
		draggedToTeam = draggedToTeam_;
		Cursor a = gamePanel.toolkit.createCustomCursor(ImageHandler.gladiatorImages[gladiator.race.getIdx()] , new Point(16, 16), "");
		frame.setCursor(a);
	}
	
	public void resetDraggedGladiator(){
		draggedGladiator = null;
		draggedToTeam = false;
		frame.setCursor(Cursor.getDefaultCursor());
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		System.out.println("Mouse released: "+e.getX()+","+e.getY());
		if(shootMode == 0){
			if(draggedGladiator != null){
				if(gamePanel.GAME_WINDOW_CURRENT == GamePanel.GAME_WINDOW_BATTLE_SETUP){
					int slot = gamePanel.checkIfMouseInBattleFormationSlot(new Position(e.getX(), e.getY()));
					if(slot != -1){
						if(!Game.battleTeamHasGladiator(draggedGladiator) || !draggedToTeam){
							if(!draggedToTeam){
								Game.currentTeam.battleGladiators[Game.battleTeamGetGladiatorSlot(draggedGladiator)] = Game.currentTeam.battleGladiators[slot];
							}
							Game.putGladiatorToBattleSlot(draggedGladiator, slot);
							frame.validate();
							frame.repaint();
						}
						resetDraggedGladiator();
						return;
					}
					Gladiator gladiator = gamePanel.checkIfPressedHero(new Position(e.getX(), e.getY()));
					if(gladiator != null && !draggedToTeam){
						Game.currentTeam.battleGladiators[Game.battleTeamGetGladiatorSlot(draggedGladiator)] = null;
						frame.validate();
						frame.repaint();
						resetDraggedGladiator();
						return;
					}
				}
			}
			resetDraggedGladiator();
		}
	}
	
	public void setShootMode(int type){
		shootMode = type;
		if(shootMode == Constants.SHOOT_MODE_RANGED_WEAPON){
			Cursor a = gamePanel.toolkit.createCustomCursor(ImageHandler.shootCursorImage , new Point(13, 13), "");
			frame.setCursor(a);
			GamePanel.setMessage("Valitse kohde hiirell� tai paina ESC peruaksesi.");
		}
		else if(shootMode == Constants.SHOOT_MODE_SPELL){
			Cursor a = gamePanel.toolkit.createCustomCursor(ImageHandler.shootCursorImage , new Point(13, 13), "");
			frame.setCursor(a);
			Gladiator gladiator = Game.currentBattle.getCurrentBattleGladiator();
			if(Game.currentShootSpell.isFriendlySpell()){
				int atkVal = gladiator.getAtkSkillValueForSpell(Game.currentShootSpell);
				GamePanel.setMessage(gladiator.name+" heitt�� loitsua nimelt� "+Game.currentShootSpell.name+".",
									"Loitsu ("+atkVal+") onnistui.",
									"Valitse kohde hiirell� tai paina ESC peruaksesi.");
			} else {
				GamePanel.setMessage(gladiator.name+" heitt�� loitsua nimelt� "+Game.currentShootSpell.name+".",
									"Valitse kohde hiirell� tai paina ESC peruaksesi.");
			}
		}
		else {
			frame.setCursor(Cursor.getDefaultCursor());
			Game.currentShootSpell = null;
		}
		frame.validate();
		frame.repaint();
	}

	int shootMode = 0;
	
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            if(shootMode != 0){
            	setShootMode(0);
            	GamePanel.setMessage("No ei sitten, jos kerran muutit mielesi.");
            }
        }
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	private static void setFontsForPopups() {      
		UIManager.put("OptionPane.font", MENU_FONT);
	    UIManager.put("OptionPane.messageFont", MENU_FONT);
	    UIManager.put("OptionPane.buttonFont", MENU_FONT);
	    UIManager.put("TextField.font", MENU_FONT);
	    UIManager.put("InternalFrame.titleFont", MENU_FONT);
	}
	
}
