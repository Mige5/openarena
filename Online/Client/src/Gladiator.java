import java.util.ArrayList;
import java.util.HashMap;

public class Gladiator {

	String name;
	Race race;
	int salary;
	int takeOuts_1;
	int takeOuts_2;
	int hp;
	int maxHp;
	int mana;
	int maxMana;
	int manaBonus;
	int age;
	int hurt = 0;
	int matches;
	int str;
	int strBonus;
	int speed;
	int speedBonus;
	int skillEffectCount;
	int moral;
	int condition;
	int spells;
	int melee_weapon_atk_1;
	int melee_weapon_atk_2;
	int melee_weapon_atk_3;
	int melee_weapon_atk_4;
	int melee_weapon_atk_5;
	int melee_weapon_def_1;
	int melee_weapon_def_2;
	int melee_weapon_def_3;
	int melee_weapon_def_4;
	int melee_weapon_def_5;
	int ranged_weapon_atk_1;
	int ranged_weapon_atk_2;
	int ability1;
	int ability2;
	boolean specialGladiator = false;
	Item[] items = new Item[3];
	Spell[] knownSpells = new Spell[4];
	int[] spellAtk = new int[4];
	
	Team owner;
	
	int askPrice;
	int offerPrice;
	Team offerTeam;
	
	Position pos;
	
	int receivedDmg = -1;
	int ammo = Constants.AMMO_COUNT;
	boolean needsReload = false;
	
	int global_idx;
	
	public static int GLADIATOR_IDX = 0;
	
	public static HashMap<Integer, Gladiator> gladiatorMap = new HashMap<Integer, Gladiator>();
	
	public Gladiator(){
	}
	
	public Gladiator(String name, int race, int idx){
		/*
		this.global_idx = GLADIATOR_IDX;
		GLADIATOR_IDX++;
		*/
		this.name = name;
		if(race == -1){
			this.race = Race.races.get(Util.random_(Race.races.size()));
		} else {
			this.race = Race.races.get(race);
		}
		/*
		this.str = Util.roll(this.race.str, "max");
		this.speed = Util.roll(this.race.speed, "max");
		this.maxMana = Util.roll(this.race.mana, "max");
		this.maxHp = Util.roll(this.race.health, "max");
		*/
		this.str = Util.roll(this.race.str);
		this.speed = Util.roll(this.race.speed);
		this.maxMana = Util.roll(this.race.mana);
		this.maxHp = Util.roll(this.race.health);
		this.mana = this.maxMana;
		this.hp = this.maxHp;
		this.age = Constants.GLADIATOR_MIN_AGE + Util.random_(Constants.GLADIATOR_MAX_AGE - Constants.GLADIATOR_MIN_AGE + 1);
		//TODO: Figure out how the values below should be generated
		this.askPrice = 50;
		this.salary = Util.random_(6, 65);
		this.melee_weapon_atk_1 = Util.random_(3, 52);
		this.melee_weapon_atk_2 = Util.random_(3, 52);
		this.melee_weapon_atk_3 = Util.random_(3, 52);
		this.melee_weapon_atk_4 = Util.random_(3, 52);
		this.melee_weapon_atk_5 = Util.random_(3, 52);
		this.melee_weapon_def_1 = 10;
		this.melee_weapon_def_2 = Util.random_(3, 52);
		this.melee_weapon_def_3 = Util.random_(3, 52);
		this.melee_weapon_def_4 = Util.random_(3, 52);
		this.melee_weapon_def_5 = Util.random_(3, 52);
		this.ranged_weapon_atk_1 = Util.random_(3, 52);
		this.ranged_weapon_atk_2 = Util.random_(3, 52);
		this.ability1 = Util.random_(3, 52);
		this.ability2 = Util.random_(3, 52);
		for(int i = 0; i < this.spellAtk.length; i++){
			this.spellAtk[i] = this.ability2;
		}
		this.speedBonus = Util.random_(-9, 9);
		this.strBonus = Util.random_(-3, 8);
		this.manaBonus = Util.random_(-9, 11);
		gladiatorMap.put(idx, this);
	}
	
	/*
	public Gladiator(String name){
		this(name, -1);
	}
	*/
	
	public int getAtkSkillValueForWeapon(Item weapon){
		if(weapon == null){
			return melee_weapon_atk_1;
		}
		if(weapon.subtype == Item.SUBTYPE_MELEE_WEAPON_SPEAR){
			return melee_weapon_atk_2;
		}
		if(weapon.subtype == Item.SUBTYPE_MELEE_WEAPON_SWORD){
			return melee_weapon_atk_3;
		}
		if(weapon.subtype == Item.SUBTYPE_MELEE_WEAPON_AXE){
			return melee_weapon_atk_4;
		}
		if(weapon.subtype == Item.SUBTYPE_MELEE_WEAPON_MACE){
			return melee_weapon_atk_5;
		}
		if(weapon.subtype == Item.SUBTYPE_RANGED_WEAPON_BOW){
			return ranged_weapon_atk_1;
		}
		if(weapon.subtype == Item.SUBTYPE_RANGED_WEAPON_CROSSBOW){
			return ranged_weapon_atk_2;
		}
		return 0;
	}
	
	public int getDefSkillValueForWeapon(Item weapon){
		if(weapon == null){
			return melee_weapon_def_1;
		}
		if(weapon.subtype == Item.SUBTYPE_MELEE_WEAPON_SPEAR){
			return melee_weapon_def_2;
		}
		if(weapon.subtype == Item.SUBTYPE_MELEE_WEAPON_SWORD){
			return melee_weapon_def_3;
		}
		if(weapon.subtype == Item.SUBTYPE_MELEE_WEAPON_AXE){
			return melee_weapon_def_4;
		}
		if(weapon.subtype == Item.SUBTYPE_MELEE_WEAPON_MACE){
			return melee_weapon_def_5;
		}
		return 0;
	}
	
	public int getAtkSkillValueForSpell(Spell spell){
		for(int i = 0; i < this.knownSpells.length; i++){
			Spell spell_ = this.knownSpells[i];
			if(spell_ == null)
				continue;
			if(spell == spell_){
				return this.spellAtk[i];
			}
		}
		return 0;
	}
	
	public int getNextAvailableSpellSlot(){
		for(int i = 0; i < this.knownSpells.length; i++){
			Spell spell = this.knownSpells[i];
			if(spell == null)
				return i;
		}
		return -1;
	}
	
	public ArrayList<Spell> getKnownSpells(){
		ArrayList<Spell> spells = new ArrayList<Spell>();
		for(int i = 0; i < this.knownSpells.length; i++){
			Spell spell = this.knownSpells[i];
			if(spell != null)
				spells.add(spell);
		}
		return spells;
	}
	
	public int getArmorValue(){
		int value = 0;
		if(getArmour() != null){
			value += getArmour().pp_val;
		}
		return value;
	}
	
	public String getRangedDmgValue(){
		String s = "";
		if(getRangedWeapon() != null){
			s = getRangedWeapon().dmg;
		}
		return s;
	}
	
	public String getMeleeDmgValue(){
		String s = "1d2";
		if(strBonus > 0){
			s += "+"+strBonus;
		}
		else if(strBonus < 0){
			s += ""+strBonus;
		}
		if(getMeleeWeapon() != null){
			String weaponDmg = getMeleeWeapon().dmg;
			int[] dmgParts = Util.getDmgValueParts(weaponDmg);
			s = dmgParts[0]+"d"+dmgParts[1];
			int off = dmgParts[2] + strBonus;
			if(off > 0){
				s += "+"+off;
			}
			else if(off < 0){
				s += ""+off;
			}
		}
		return s;
	}
	
	public Item getMeleeWeapon(){
		return this.items[Constants.MELEE_WEAPON_SLOT];
	}
	
	public Item getRangedWeapon(){
		return this.items[Constants.RANGED_WEAPON_SLOT];
	}
	
	public Item getArmour(){
		return this.items[Constants.ARMOUR_SLOT];
	}
	
	public void setMeleeWeapon(Item item){
		this.items[Constants.MELEE_WEAPON_SLOT] = item;
	}
	
	public void setRangedWeapon(Item item){
		this.items[Constants.RANGED_WEAPON_SLOT] = item;
	}
	
	public void setArmour(Item item){
		this.items[Constants.ARMOUR_SLOT] = item;
	}
	
	public void giveItem(Item item){
		if(item.type == Item.TYPE_MELEE_WEAPON){
			setMeleeWeapon(item);
		} else if(item.type == Item.TYPE_RANGED_WEAPON){
			setRangedWeapon(item);
		} else if(item.type == Item.TYPE_ARMOUR){
			setArmour(item);
		} else {
			System.out.println("Item type not found! item: "+item.name+" type: "+item.type);
		}
	}
	
}
