

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;

public class Client implements Runnable{
	
	private String host;
	private int port;
	
	public static GamePanel gamePanel;
	
	private Socket socket;
	public DataInputStream in;
	public DataOutputStream out;
	public PacketReader packetReader;
	public PacketSender packetSender;
	
	public Client(String host, int port) {
		this.host = host;
		this.port = port;
	}
	
	public void connect(String name) {
		try {
			socket = new Socket(host, port);
			out = new DataOutputStream(socket.getOutputStream());
			in = new DataInputStream(socket.getInputStream());
			packetReader = new PacketReader(this);
			packetSender = new PacketSender(this);
			out.writeUTF(name);
			new Thread(this).start();
		}catch (IOException e){
			closeClient(socket, in, out);
		}
	}
	
	public void closeClient(Socket socket, DataInputStream in, DataOutputStream out){
		try{
			if(in != null){
				in.close();
			}
			if(out != null){
				out.close();
			}
			if(socket != null){
				socket.close();
			}
		}catch (IOException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		while(socket.isConnected()){
			try{
				int packetId = in.readByte();
				packetReader.readPacket(packetId);
			}catch (IOException e){
				closeClient(socket, in, out);
			}
		}
	}

}
