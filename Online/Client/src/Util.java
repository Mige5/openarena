import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.font.TextAttribute;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JTable;

public class Util {

	private static Random random = new Random();
	
	public static int random_(int range) {
		int number = random.nextInt(range);
		return number;
	}
	
	public static int random_(int min, int max) {
		int number = min + random.nextInt(max-(min-1));
		return number;
	}
	
	public static int hitChance(int atkSkill, int blockSkill){
		return (atkSkill-blockSkill+50);
	}
	
	public static boolean successfulHit(int atkSkill, int blockSkill){
		if(random_(100) + 1 <= hitChance(atkSkill, blockSkill)){
			return true;
		}
		return false;
	}
	
	public static boolean successfulHit(int hitChance){
		if(random_(100) + 1 <= hitChance){
			return true;
		}
		return false;
	}
	
	public static boolean criticalHit(){
		if(random_(100) + 1 <= Constants.CRITICAL_HIT_CHANCE_PERCENTAGE){
			return true;
		}
		return false;
	}
	
	public static int getDistance(Position a, Position b) {
		int deltaX = b.getX() - a.getX();
		int deltaY = b.getY() - a.getY();
		return (int) Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
	}
	
	public static String getBonusTxt(int val){
		String s = "";
		if(val != 0){
	    	if(val > 0){
	    		s = "(+"+val+")";
	    	} else {
	    		s = "("+val+")";
	    	}
	    } else {
	    	s = "(0)";
	    }
		return s;
	}
	
	public static int roll(String rolls){
		return roll(rolls, "normal");
	}
	
	public static int roll(String rolls, String rollType){
		String[] values_1 = rolls.split("d");
		int times = Integer.parseInt(values_1[0]);
		int dice = 0;
		int add = 0;
		if(values_1[1].contains("+")){
			String[] values_2 = values_1[1].split("\\+");
			dice = Integer.parseInt(values_2[0]);
			add = Integer.parseInt(values_2[1]);
		} else if(values_1[1].contains("-")){
			String[] values_2 = values_1[1].split("\\-");
			dice = Integer.parseInt(values_2[0]);
			add = -Integer.parseInt(values_2[1]);
		} else {
			dice = Integer.parseInt(values_1[1]);
		}
		int total = 0;
		for(int i = 0; i < times; i ++){
			if(rollType.equals("min")){
				total += 1;
			} else if(rollType.equals("max")){
				total += dice;
			} else {
				total += random_(dice)+1;
			}
		}
		total += add;
		return total;
	}
	
	public static int[] getDmgValueParts(String dmg){
		String[] values_1 = dmg.split("d");
		int times = Integer.parseInt(values_1[0]);
		int dice = 0;
		int add = 0;
		if(values_1[1].contains("+")){
			String[] values_2 = values_1[1].split("\\+");
			dice = Integer.parseInt(values_2[0]);
			add = Integer.parseInt(values_2[1]);
		} else if(values_1[1].contains("-")){
			String[] values_2 = values_1[1].split("\\-");
			dice = Integer.parseInt(values_2[0]);
			add = -Integer.parseInt(values_2[1]);
		} else {
			dice = Integer.parseInt(values_1[1]);
		}
		int[] dmgParts = new int[]{times, dice, add};
		return dmgParts;
	}

	public static void drawStringMultiLine(Graphics2D g, String text, int lineWidth, int x, int y, int heightOffset) {
	    FontMetrics m = g.getFontMetrics();
	    if(m.stringWidth(text) < lineWidth) {
	        g.drawString(text, x, y);
	    } else {
	        String[] words = text.split(" ");
	        String currentLine = words[0];
	        for(int i = 1; i < words.length; i++) {
	            if(m.stringWidth(currentLine+words[i]) < lineWidth) {
	                currentLine += " "+words[i];
	            } else {
	                g.drawString(currentLine, x, y);
	                if(heightOffset == -1)
	                	y += m.getHeight();
	                else
	                	y += heightOffset;
	                currentLine = words[i];
	            }
	        }
	        if(currentLine.trim().length() > 0) {
	            g.drawString(currentLine, x, y);
	        }
	    }
	}
	
	public static Object GetData(JTable table, int row_index, int col_index){
		  return table.getModel().getValueAt(row_index, col_index);
	}

	public static void setButtonStatus(JButton button, String disabledTxt){
		if(button == null)
			return;
		if(button.getText().toLowerCase().equals(disabledTxt.toLowerCase())){
			button.setEnabled(false);
		} else {
			button.setEnabled(true);
		}
	}

	public static AttributedCharacterIterator underlinedString(String s, Font f){
		AttributedString as1 = new AttributedString(s);
		as1.addAttribute(TextAttribute.FONT, f);
	    as1.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
	    return as1.getIterator();
	}

	public static void drawCenteredString(String s, int x, int y, Graphics2D g2d) {
	    FontMetrics fm = g2d.getFontMetrics();
	    x = x - (fm.stringWidth(s) / 2);
	    g2d.drawString(s, x, y);
	 }

	public static void drawCenteredUnderlinedString(String s, Font f, int x, int y, Graphics2D g2d) {
		g2d.setFont(f);
	    FontMetrics fm = g2d.getFontMetrics();
	    x = x - (fm.stringWidth(s) / 2);
	    g2d.drawString(underlinedString(s, f), x, y);
	 }

	public static void organizeTeams(ArrayList<Team> teams){
		Collections.sort(teams, new Comparator<Team>() {
		    public int compare(Team t1, Team t2) {
		    	return Integer.compare(t2.wins, t1.wins);
		    }
		});
	}

	public static int getTeamIndex(Division division, Team team){
		for(int i = 0; i < division.teams.size(); i++){
			Team team_ = division.teams.get(i);
			if(team == team_){
				return i;
			}
		}
		return -1;
	}
	
}
