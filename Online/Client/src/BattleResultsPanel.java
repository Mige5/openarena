import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JPanel;

public class BattleResultsPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	//COLORS
	public static Color BACKGROUND_COLOR = new Color(224, 224, 224);
	
	//FONTS
	public static Font PANEL_FONT = new Font("courier new", Font.PLAIN, 12);
	public static Font BUTTON_FONT = new Font("Segoe UI", Font.BOLD, 12);
	
	public static int GAME_WINDOW_STANDINGS_VIEW = 11;
	
	public BattleResultsPanel(Jframe frame){
		this.mainFrame = frame;
		this.setLayout(null);
		this.add(button(GAME_WINDOW_STANDINGS_VIEW, "close battle results view", "OK", BUTTON_FONT, 8, 432, 777, 105));
	}
	
	public void paintComponent (Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
	    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    g2d.setColor(BACKGROUND_COLOR);
	    g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
	    g2d.setColor(Color.BLACK);
	    g2d.setFont(PANEL_FONT);
	    int divisionOffY = 102;
	    int teamOffY = 17;
	    
	    ArrayList<Battle> div1Battles = new ArrayList<Battle>();
	    ArrayList<Battle> div2Battles = new ArrayList<Battle>();
	    ArrayList<Battle> div3Battles = new ArrayList<Battle>();
	    ArrayList<Battle> div4Battles = new ArrayList<Battle>();
	    for(Battle battle : Game.battleList){
	    	if(battle.winningTeam.division == 0){
	    		div1Battles.add(battle);
	    	}
	    	else if(battle.winningTeam.division == 1){
	    		div2Battles.add(battle);
	    	}
	    	else if(battle.winningTeam.division == 2){
	    		div3Battles.add(battle);
	    	}
	    	else if(battle.winningTeam.division == 3){
	    		div4Battles.add(battle);
	    	}
	    }
	    ArrayList<Battle> battleList = null;
	    for(int i = 0; i < Division.divisions.size(); i++){
	    	g2d.drawString((i+1)+". DIVISIOONA", 172, 29+(i*divisionOffY));
	    	if(i == 0){
	    		battleList = div1Battles;
	    	}
	    	else if(i == 1){
	    		battleList = div2Battles;
	    	}
	    	if(i == 2){
	    		battleList = div3Battles;
	    	}
	    	if(i == 3){
	    		battleList = div4Battles;
	    	}
	    	for(int j = 0; j < battleList.size(); j++){
	    		Battle battle = battleList.get(j);
	    		g2d.drawString(battle.topTeam.name, 172, 63+(i*divisionOffY)+(j*teamOffY));
	    		g2d.drawString("-", 343, 63+(i*divisionOffY)+(j*teamOffY));
	    		g2d.drawString(battle.bottomTeam.name, 379, 63+(i*divisionOffY)+(j*teamOffY));
	    		g2d.drawString(battle.topTeam == battle.winningTeam ? "1" : "0", 560, 63+(i*divisionOffY)+(j*teamOffY));
	    		g2d.drawString("-", 577, 63+(i*divisionOffY)+(j*teamOffY));
	    		g2d.drawString(battle.bottomTeam == battle.winningTeam ? "1" : "0", 595, 63+(i*divisionOffY)+(j*teamOffY));
	    	}
	    }
	}
	
	public void refresh(){
		this.revalidate();
		this.repaint();
	}
	
	public ArrayList<Button> buttons = new ArrayList<Button>();
	
	public JButton button(int windowName, String s, Font f, int x, int y, int width, int height){
		return button(windowName, s, s, f, x, y, width, height);
	}
	
	public JButton button(int windowName, String buttonName, String s, Font f, int x, int y, int width, int height){
		JButton jButton = new JButton(s);
		jButton.setForeground(Color.BLACK);
		jButton.setFont(f);
		jButton.setBounds(x, y, width, height);
		jButton.setMargin(new Insets(0, 0, 0, 0));
		jButton.setVisible(true);
		jButton.setActionCommand(buttonName);
		jButton.addActionListener(this.mainFrame);
		for(Button button : buttons){
			if(button.button_name.equals(buttonName)){
				System.out.println("[ERROR]: Button with same name already exists! "+buttonName);
			}
		}
		buttons.add(new Button(new int[]{windowName}, buttonName, jButton));
		return jButton;
	}
	
	public JButton button(int[] windowName, String s, Font f, int x, int y, int width, int height){
		return button(windowName, s, s, f, x, y, width, height);
	}
	
	public JButton button(int[] windowName, String buttonName, String s, Font f, int x, int y, int width, int height){
		JButton jButton = new JButton(s);
		jButton.setForeground(Color.BLACK);
		jButton.setFont(f);
		jButton.setBounds(x, y, width, height);
		jButton.setMargin(new Insets(0, 0, 0, 0));
		jButton.setVisible(true);
		jButton.setActionCommand(buttonName);
		jButton.addActionListener(this.mainFrame);
		for(Button button : buttons){
			if(button.button_name.equals(buttonName)){
				System.out.println("[ERROR]: Button with same name already exists! "+buttonName);
			}
		}
		buttons.add(new Button(windowName, buttonName, jButton));
		return jButton;
	}
	
	public JButton findButton(String s){
		for(Button button : buttons){
			if(button.button_name.toLowerCase().equals(s.toLowerCase())){
				return button.jButton;
			}
		}
		return null;
	}
	
	Jframe mainFrame;
	
}
