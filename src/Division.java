import java.util.ArrayList;

public class Division {

	public static ArrayList<Division> divisions = new ArrayList<Division>();
	public ArrayList<Team> teams = new ArrayList<Team>();

	int startMoney;
	
	public Division(int startMoney){
		this.startMoney = startMoney;
		divisions.add(this);
	}
	
}
