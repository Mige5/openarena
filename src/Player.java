import java.util.ArrayList;

public class Player {

	public static ArrayList<Player> players = new ArrayList<Player>();
	
	Team controlledTeam;
	
	public Player(Team team){
		this.controlledTeam = team;
		players.add(this);
	}
	
}
