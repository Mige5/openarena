

public class Main {
	
	public static void main(String[] args) {
		Race.initRaces();
		ImageHandler.loadImages();
		Server server = new Server(5000);
		server.start();
	}

}
