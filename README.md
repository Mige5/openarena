# OpenArena
Open source gladiator management game

## Installation
Built with Java8 (so works best with that version, but may work with other versions)

## Roadmap
Hopefully in v1.0 this would be fully reimagined Areena 5.

## Contributing
The project is open to contributions. Theres also todo list included.

## Authors and acknowledgment
Mige

## License
OpenArena is free software, licenced under the GPL3 license.

## Project status
First public release v0.1 now available.