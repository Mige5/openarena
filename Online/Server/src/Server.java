

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Runnable{
	
	private int port;
	private ServerSocket serverSocket;
	
	public Server(int port) {
		this.port = port;
		try {
			serverSocket = new ServerSocket(port);
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void start() {
		new Thread(this).start();
	}

	@Override
	public void run() {
		System.out.println("Server started on port: "+port);
		try{
			while(!serverSocket.isClosed() && Client.clients.size() < Constants.MAX_PLAYERS){
				Socket socket = serverSocket.accept();
				initSocket(socket);
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	private void initSocket(Socket socket) {
		Client client = new Client(socket);
		Client.clients.add(client);
		Client.clients2.put(client.clientId, client);
		new Thread(client).start();
		Client.sendPlayersToAllClients();
	}
	
	public void closeServer(){
		try{
			if(serverSocket != null){
				serverSocket.close();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}

}
